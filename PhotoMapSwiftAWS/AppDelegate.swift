//
//  AppDelegate.swift
//  PhotoMapSwiftAWS
//
//  Created by Roman Andreyenko on 4/20/17.
//  Copyright © 2017 iTechArt. All rights reserved.
//

import UIKit
import Swinject
import SwinjectAutoregistration
import AWSS3
import ReachabilitySwift

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate, SessionCallback {

    let reachability = Reachability()!
    var window: UIWindow?
    private var isAnimatedAppearance = false
    
    private var appComponent: ApplicationComponent!
    
    //Inject
    private var sessionManager: SessionManager!

    public class func sharedDelegate() -> AppDelegate {
        return UIApplication.shared.delegate as! AppDelegate
    }
    
    public func getAppComponent() -> ApplicationComponent {
        return appComponent
    }
    
    private func setupInitScreenViewController() -> UIViewController {
        let navigationController = UINavigationController(rootViewController: BaseViewController.newInstance(of: WelcomeViewController.self))
        return navigationController
    }
    
    private func setupMainScreenViewController() -> UIViewController {
        return BaseViewController.newInstance(of: MainTabBarController.self)
    }
    
    private func setupWindow() {
        self.window = UIWindow(frame: UIScreen.main.bounds)
        self.window?.backgroundColor = UIColor.white
        self.window?.rootViewController = UIViewController()
        self.window?.makeKeyAndVisible()
    }
    
    private func setup(rootViewController viewController: UIViewController, animated: Bool) {
        self.window?.rootViewController = viewController
        self.window?.makeKeyAndVisible()
        
        if (animated) {
            self.window?.rootViewController?.view.alpha = 0.0;
            
            UIView.animate(withDuration: 1.0, animations: {
                self.window?.rootViewController?.view.alpha = 1.0
            })
        }
    }
    
    private func startReachability() {
        try! reachability.startNotifier()
    }
    
    private func stopReachability() {
        reachability.stopNotifier()
    }
    
    func setupRootViewController(animated: Bool) {
        self.isAnimatedAppearance = animated
        sessionManager.getSession(with: self)
    }
    
    func isUserAuthorized() {
        setup(rootViewController: setupMainScreenViewController(), animated: self.isAnimatedAppearance)
    }
    
    func isUserAnonymous() {
        setup(rootViewController: setupInitScreenViewController(), animated: self.isAnimatedAppearance)
    }
    
    func setupAppComponent() {
        appComponent = ApplicationComponent(applicationAssembly: ApplicationAssembly(appDelegate: self),
                                            applicationDataAssembly: ApplicationDataAssembly(),
                                            interactorsAssembly: InteractorsAssembly(),
                                            sessionDataAssembly: SessionDataAssembly(),
                                            photoDataAssembly: PhotoDataAssembly(),
                                            cognitoAssembly: AWSCognitoAssembly(),
                                            dynamoDBAssembly: AWSDynamoDBAssembly(),
                                            s3Assembly: AWSS3Assembly())
    }
    
    func resolveDependencies(withResolver r: Resolver) {
        sessionManager = r ~> SessionManager.self
    }
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        startReachability()
        setupAppComponent()
        resolveDependencies(withResolver: appComponent.getResolver())
        setupWindow()
        setupRootViewController(animated: false)

        return true
    }

    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(_ application: UIApplication) {
        sessionManager.onCleanUp()
        stopReachability()
    }

    func application(_ application: UIApplication, handleEventsForBackgroundURLSession identifier: String, completionHandler: @escaping () -> Void) {
        // Store the completion handler.
        AWSS3TransferUtility.interceptApplication(application, handleEventsForBackgroundURLSession: identifier, completionHandler: completionHandler)
    }
}

