//
//  CategoryPickerViewController.swift
//  PhotoMapSwiftAWS
//
//  Created by Roman Andreyenko on 6/9/17.
//  Copyright © 2017 iTechArt. All rights reserved.
//

import UIKit
import Swinject
import SwinjectAutoregistration

/**
 Class implementation of view controller to choose photo category
 */
class CategoryPickerViewController: BaseViewController, UIPickerViewDelegate, UIPickerViewDataSource, CategoryPickerView {

    private static let PickerViewCellHeight: CGFloat = 56.0
    
    weak var delegate: CategoryPickerViewControllerDelegate?
    
    @IBOutlet private weak var categoryPickerView: UIPickerView!
    @IBOutlet private weak var categoryPickerDoneView: UIButton!
    
    //Inject
    private var categoryPickerPresenter: CategoryPickerPresenter!
    
    // MARK: - init & deinit
    
    deinit {
        categoryPickerPresenter.destroy()
    }
    
    public static func newInstance(withCategory category: PhotoModel.Category) -> CategoryPickerViewController {
        let viewController = BaseViewController.newInstance(of: CategoryPickerViewController.self)
        viewController.categoryPickerPresenter.initialize(withCategory: category)
        
        return viewController
    }
    
    // MARK: - UIViewController lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setUpViews()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        categoryPickerPresenter.willAppear()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        categoryPickerPresenter.willDisappear()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

    // MARK: - Swinject
    
    public override func setupSwinjectMembers() -> BaseComponent? {
        return CategoryPickerComponent(applicationComponent: AppDelegate.sharedDelegate().getAppComponent(),
                                    categoryPickerAssembly: CategoryPickerAssembly(view: self))
    }
    
    public override func resolveDependencies(withResolver r: Resolver) {
        categoryPickerPresenter = r ~> CategoryPickerPresenter.self
    }
    
    // MARK: - CategoryPickerView
    
    func pick(newCategory category: PhotoModel.Category) {
        if let delegate = delegate {
            delegate.picked(newCategory: category)
        }
    }
    
    func closeCategoryPickerScreen() {
        willMove(toParentViewController: nil)
        view.removeFromSuperview()
        removeFromParentViewController()
    }
    
    // MARK: - UI logic methods
    
    private func setUpViews() {
        categoryPickerView.delegate = self
        categoryPickerView.dataSource = self
        categoryPickerView.selectRow(categoryPickerPresenter.categories.index(of: categoryPickerPresenter.getCurrentCategory())!, inComponent: 0, animated: false)
        categoryPickerDoneView.setTitle(categoryPickerDoneView.title(for: .normal)!.uppercased(), for: .normal)
    }
    
    @IBAction func onDoneClicked(_ sender: UIButton) {
        let selectedRow = categoryPickerView.selectedRow(inComponent: 0)
        let newCategory = categoryPickerPresenter.categories[selectedRow]
        categoryPickerPresenter.pick(newCategory: newCategory)
    }
    
    // MARK: - UIPickerViewDataSource
    
    public func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    public func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return categoryPickerPresenter.categories.count
    }
    
    // MARK: - UIPickerViewDelegate
    
    public func pickerView(_ pickerView: UIPickerView, widthForComponent component: Int) -> CGFloat {
        return pickerView.bounds.size.width
    }
    
    public func pickerView(_ pickerView: UIPickerView, rowHeightForComponent component: Int) -> CGFloat {
        return CategoryPickerViewController.PickerViewCellHeight
    }
    
    public func pickerView(_ pickerView: UIPickerView, viewForRow row: Int, forComponent component: Int, reusing view: UIView?) -> UIView {
        let categoryView = view as! CategoryView? ?? CategoryView(frame: CGRect(x: 0.0, y: 0.0, width: pickerView.bounds.size.width, height: CategoryPickerViewController.PickerViewCellHeight))
        let category = categoryPickerPresenter.categories[row]
        categoryView.categoryTextView.text = category.rawValue.uppercased()
        categoryView.categoryIconView.image = PhotoPinAnnotationView.imageIcon(for: category)
        
        return categoryView;
    }
}

/**
 Delegate interface for CategoryPickerViewController
 */
protocol CategoryPickerViewControllerDelegate: class {
    
    /**
     Callback method that notifies new photo category is picked
     
     - parameters:
        - newCategory: new category
     */
    func picked(newCategory: PhotoModel.Category)
}
