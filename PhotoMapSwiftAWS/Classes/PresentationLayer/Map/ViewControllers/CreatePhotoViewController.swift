//
//  CreatePhotoViewController.swift
//  PhotoMapSwiftAWS
//
//  Created by Roman Andreyenko on 5/29/17.
//  Copyright © 2017 iTechArt. All rights reserved.
//

import UIKit
import Swinject
import SwinjectAutoregistration
import CoreLocation

/**
 Class implementation of view controller for photo creation
 */
class CreatePhotoViewController: BaseLoadingViewController, CreatePhotoView, CategoryPickerViewControllerDelegate {
    
    @IBOutlet private weak var photoView: UIImageView!
    @IBOutlet private weak var createdDateView: UILabel!
    @IBOutlet private weak var categoryView: UIButton!
    @IBOutlet private weak var descriptionTextView: UITextView!
    @IBOutlet private weak var cancelView: UIButton!
    @IBOutlet private weak var doneView: UIButton!
    @IBOutlet private weak var categoryPickerContainerView: UIView!
    
    //Inject
    private var createPhotoPresenter: CreatePhotoPresenter!
    
    // MARK: - init & deinit
    
    deinit {
        createPhotoPresenter.destroy()
    }
    
    public static func newInstance(withCoordinates coordinates: CLLocationCoordinate2D,
                                   photoPictureData pictureData: Data,
                                   photoThumbnailPictureData thumbnailPictureData: Data,
                                   creationDate: Date) -> CreatePhotoViewController {
        let viewController = BaseViewController.newInstance(of: CreatePhotoViewController.self)
        viewController.createPhotoPresenter.initialize(withCoordinates: coordinates, photoPictureData: pictureData, photoThumbnailPictureData: thumbnailPictureData, creationDate: creationDate)
        
        return viewController
    }
    
    // MARK: - UIViewController lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()

        setUpViews()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        createPhotoPresenter.willAppear()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        createPhotoPresenter.willDisappear()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    // MARK: - Swinject
    
    public override func setupSwinjectMembers() -> BaseComponent? {
        return CreatePhotoComponent(applicationComponent: AppDelegate.sharedDelegate().getAppComponent(),
                            createPhotoAssembly: CreatePhotoAssembly(view: self))
    }
    
    public override func resolveDependencies(withResolver r: Resolver) {
        createPhotoPresenter = r ~> CreatePhotoPresenter.self
    }
    
    // MARK: - CreatePhotoView
    
    func closeCreatePhotoScreen() {
        willMove(toParentViewController: nil)
        view.removeFromSuperview()
        removeFromParentViewController()
    }
    
    func setUpCategoryView() {
        categoryView.setTitle(createPhotoPresenter.getPhotoCategoryFormattedText(), for: .normal)
        categoryView.setImage(PhotoPinAnnotationView.imageIcon(for: createPhotoPresenter.category), for: .normal)
    }
    
    func showCategoryPickerContainerView() {
        categoryPickerContainerView.isHidden = false
        photoView.isUserInteractionEnabled = false
        categoryView.isUserInteractionEnabled = false
        descriptionTextView.isUserInteractionEnabled = false
        cancelView.isUserInteractionEnabled = false
        doneView.isUserInteractionEnabled = false
    }
    
    func hideCategoryPickerContainerView() {
        categoryPickerContainerView.isHidden = true
        photoView.isUserInteractionEnabled = true
        categoryView.isUserInteractionEnabled = true
        descriptionTextView.isUserInteractionEnabled = true
        cancelView.isUserInteractionEnabled = true
        doneView.isUserInteractionEnabled = true
    }
    
    func openChooseCategoryScreen(withCategory category: PhotoModel.Category) {
        let categoryPickerViewController = CategoryPickerViewController.newInstance(withCategory: category)
        categoryPickerViewController.delegate = self
        addChildViewController(categoryPickerViewController)
        categoryPickerContainerView.addSubview(categoryPickerViewController.view)
        categoryPickerViewController.view.frame = categoryPickerContainerView.bounds
        categoryPickerViewController.didMove(toParentViewController: self)
    }
    
    func openFullPhotoScreen(for photo: PhotoModel, withPicture picture: PhotoPictureModel) {
        let fullPhotoViewController = FullPhotoViewController.newInstance(withPhoto: photo, withPicture: picture)
        presentHorizontally(viewController: fullPhotoViewController, animated: true)
    }
    
    func enablePhotoCreation() {
        doneView.isEnabled = true
    }
    
    func disablePhotoCreation() {
        doneView.isEnabled = false
    }
    
    // MARK: - UI logic methods
    
    private func setUpViews() {
        photoView.image = createPhotoPresenter.photoImage
        createdDateView.text = createPhotoPresenter.getPhotoCreatedDateFormattedText()
        setUpCategoryView()
        cancelView.setTitle(cancelView.title(for: .normal)!.uppercased(), for: .normal)
        descriptionTextView.layer.borderColor = UIColor.lightGray.cgColor
        descriptionTextView.layer.borderWidth = 2.0
        view.layer.cornerRadius = 5.0
        setupShadow(forView: view, withOffsetSize: CGSize(width: 3.0, height: 3.0))
        setupShadow(forView: photoView, withOffsetSize: CGSize(width: -3.0, height: 3.0))
    }
    
    private func setupShadow(forView view: UIView, withOffsetSize offsetSize: CGSize) {
        view.layer.masksToBounds = false
        view.layer.shadowOffset = offsetSize;
        view.layer.shadowRadius = 3.0
        view.layer.shadowOpacity = 0.5
        view.layer.shadowColor = UIColor.black.cgColor
    }
    
    @IBAction func onCategoryFilterClicked(_ sender: UIButton) {
        createPhotoPresenter.chooseCategory()
    }
    
    @IBAction func onCancelClicked(_ sender: UIButton) {
        createPhotoPresenter.cancelPhotoCreation()
    }
    
    @IBAction func onDoneClicked(_ sender: UIButton) {
        createPhotoPresenter.createPhoto(withDescriptionText: descriptionTextView.text!)
    }
    
    @IBAction func onPickedPhotoTapGestureRecognized(_ sender: UITapGestureRecognizer) {
        createPhotoPresenter.openFullPhotoScreen(withPhotoDescriptionText: descriptionTextView.text!)
    }
    
    // MARK: - CategoryPickerViewControllerDelegate
    
    func picked(newCategory category: PhotoModel.Category) {
        createPhotoPresenter.picked(newCategory: category)
    }
}
