//
//  MapViewController.swift
//  PhotoMapSwiftAWS
//
//  Created by Roman Andreyenko on 5/4/17.
//  Copyright © 2017 iTechArt. All rights reserved.
//

import UIKit
import Swinject
import SwinjectAutoregistration
import CoreLocation
import MapKit

/**
 Class implementation of view controller for map tab
 */
class MapViewController: BaseLoadingViewController, MapView, MKMapViewDelegate, UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    @IBOutlet private weak var filterCategoriesView: UIButton!
    @IBOutlet private weak var createPictureView: UIButton!
    @IBOutlet private weak var changeNavigationModeView: UIButton!
    @IBOutlet private weak var mapView: MKMapView!
    @IBOutlet private weak var createPhotoContainerView: UIView!
    
    @IBOutlet private var selectedPinTapGestureRecognizer: UITapGestureRecognizer!
    
    private lazy var imagePicker = UIImagePickerController()
    private var selectedPinAnnotationView: PhotoPinAnnotationView?
    
    //Inject
    private var mapPresenter: MapPresenter!
    
    // MARK: - init & deinit
    
    deinit {
        mapPresenter.destroy()
    }
    
    // MARK: - UIViewController lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()

        setUpViews()
        mapPresenter.fetchPhotos()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        mapPresenter.willAppear()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        mapPresenter.willDisappear()
    }
    
    override var prefersStatusBarHidden: Bool {
        return true
    }
    
    // MARK: - Swinject
    
    override func setupSwinjectMembers() -> BaseComponent? {
        return MapComponent(applicationComponent: AppDelegate.sharedDelegate().getAppComponent(),
                               mapAssembly: MapAssembly(view: self))
    }
    
    override func resolveDependencies(withResolver r: Resolver) {
        mapPresenter = r ~> MapPresenter.self
    }
    
    // MARK: - MapView
    
    func openCategoryFilterScreen() {
        let viewController = BaseViewController.newInstance(of: PhotoCategoryFilterViewController.self)
        present(viewController, animated: true)
    }

    func openFullPhotoScreen(for photoAnnotation: PhotoAnnotationModel) {
        let fullPhotoViewController = FullPhotoViewController.newInstance(withPhoto: photoAnnotation.photoModel, withPicture: nil)
        presentHorizontally(viewController: fullPhotoViewController, animated: true)
    }
    
    func closeCategoryFilterScreen() {
        dismiss(animated: true)
    }
    
    func changeMapNavigationMode() {
        mapView.userTrackingMode = changeNavigationModeView.isSelected ? .none : .follow
    }
    
    func showPictureCreationChooserView() {
        let alertController = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        alertController.addAction(getTakePictureAction())
        alertController.addAction(getChooseFromLibraryAction())
        alertController.addAction(getCancelAction())
        present(alertController, animated: true)
    }
    
    func openPhotoLibrary() {
        openImagePicker(withType: .photoLibrary)
    }

    func openPhotoCamera() {
        openImagePicker(withType: .camera)
    }

    func openPhotoDetailsScreen(withCoordinates coordinates: CLLocationCoordinate2D,
                                photoPictureData pictureData: Data,
                                photoThumbnailPictureData thumbnailPictureData: Data,
                                creationDate: Date) {
        let createPhotoViewController = CreatePhotoViewController.newInstance(withCoordinates: coordinates, photoPictureData: pictureData, photoThumbnailPictureData: thumbnailPictureData, creationDate: creationDate)
        addChildViewController(createPhotoViewController)
        createPhotoContainerView.addSubview(createPhotoViewController.view)
        createPhotoViewController.view.frame = createPhotoContainerView.bounds
        createPhotoViewController.didMove(toParentViewController: self)
    }
    
    func showPhotoCreationContainerView() {
        createPhotoContainerView.isHidden = false
        changeNavigationModeView.isUserInteractionEnabled = false
        createPictureView.isUserInteractionEnabled = false
        filterCategoriesView.isUserInteractionEnabled = false
        mapView.isUserInteractionEnabled = false
    }
    
    func hidePhotoCreationContainerView() {
        createPhotoContainerView.isHidden = true
        changeNavigationModeView.isUserInteractionEnabled = true
        createPictureView.isUserInteractionEnabled = true
        filterCategoriesView.isUserInteractionEnabled = true
        mapView.isUserInteractionEnabled = true
    }
    
    func showMapPins(withPhotoAnnotationModels photoAnnotationModels: [PhotoAnnotationModel]) {
        let toRemoveCount = mapView.annotations.count
        var toRemove = [MKAnnotation]()
        toRemove.reserveCapacity(toRemoveCount)
        for annotation in mapView.annotations {
            if (!annotation.isEqual(mapView.userLocation)) {
                toRemove.append(annotation)
            }
        }
        self.mapView.removeAnnotations(toRemove)
        self.mapView.addAnnotations(photoAnnotationModels)
    }
    
    func showSelectedPhotoAnnotationPicture(with pictureData: Data) {
        setupLeftImage(forPinAnnotationView: selectedPinAnnotationView, with: UIImage(data: pictureData))
    }
    
    // MARK: - UI logic methods
    
    private func setUpViews() {
        mapView.delegate = self
        changeNavigationModeView.isSelected = false
        changeMapNavigationMode()
    }
    
    @IBAction func onNavigationModeButtonClicked(_ sender: UIButton) {
        sender.isSelected = !sender.isSelected
        mapPresenter.changeMapNavigationMode()
    }
    
    @IBAction func onCreatePictureButtonClicked(_ sender: UIButton) {
        mapPresenter.createPhoto(withCoordinates: getUserLocationCoordinate())
    }
    
    @IBAction func onCategoriesFilterClicked(_ sender: UIButton) {
        mapPresenter.openCategoryFilterScreen()
    }
    
    @IBAction func onLongPressRecognized(_ sender: UILongPressGestureRecognizer) {
        if sender.state == .ended {
            mapPresenter.createPhoto(withCoordinates: getTouchLocationCoordinate(withGestureRecognizer: sender))
        }
    }
    
    @IBAction func onTapPinAnnotationViewRecognized(_ sender: UITapGestureRecognizer) {
        mapPresenter.openFullPhotoScreen(for: selectedPinAnnotationView!.annotation as! PhotoAnnotationModel)
    }
    
    private func getUserLocationCoordinate() -> CLLocationCoordinate2D {
        return mapView.userLocation.coordinate
    }
    
    private func getTouchLocationCoordinate(withGestureRecognizer gestureRecognizer: UIGestureRecognizer) -> CLLocationCoordinate2D {
        let touchPoint = gestureRecognizer.location(in: mapView)
        return mapView.convert(touchPoint, toCoordinateFrom: mapView)
    }
    
    private func openImagePicker(withType type: UIImagePickerControllerSourceType) {
        if UIImagePickerController.isSourceTypeAvailable(type) {
            imagePicker.sourceType = type
            imagePicker.delegate = self
            present(imagePicker, animated: true)
        }
    }
    
    private func getTakePictureAction() -> UIAlertAction {
        return UIAlertAction(title: NSLocalizedString("TakePicture", comment: ""), style: .default) { [unowned self] alertAction in
            #if !((arch(i386) || arch(x86_64)) && os(iOS))
                self.mapPresenter.takePhotoPictureWithCamera()
            #else
                self.presentAlert(withMessage: "This feature isn't supported for iOS simulator, please, launch the app with real device")
            #endif
        }
    }
    
    private func getChooseFromLibraryAction() -> UIAlertAction {
        return UIAlertAction(title: NSLocalizedString("ChooseFromLibrary", comment: ""), style: .default) { [unowned self] alertAction in
            self.mapPresenter.choosePhotoPictureFromPhotoLibrary()
        }
    }
    
    private func getCancelAction() -> UIAlertAction {
        return UIAlertAction(title: NSLocalizedString("Cancel", comment: ""), style: .cancel) { [unowned self] alertAction in
            self.mapPresenter.cancelPhotoCreation()
        }
    }
    
    private func setupLeftImage(forPinAnnotationView pinAnnotationView: PhotoPinAnnotationView?, with image: UIImage?) {
        let leftPinView = pinAnnotationView?.leftCalloutAccessoryView as! UIImageView
        leftPinView.image = image
    }
    
    private func moveMapToCenter(of pinAnnotationView: PhotoPinAnnotationView?) {
        let photoAnnotation = pinAnnotationView!.annotation as! PhotoAnnotationModel
        var region = self.mapView.region
        region.center.latitude = photoAnnotation.photoModel.latitude
        region.center.longitude = photoAnnotation.photoModel.longitude
        self.mapView.setRegion(region, animated: true)
    }
    
    // MARK: - UIImagePickerControllerDelegate
    
    public func imagePickerController(_ picker: UIImagePickerController,
                                      didFinishPickingMediaWithInfo info: [String : Any]) {
        picker.dismiss(animated: true)
        let image = info[UIImagePickerControllerOriginalImage] as! UIImage
        mapPresenter.photoPictureCreated(withImage: image)
    }
    
    public func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        picker.dismiss(animated: true)
        mapPresenter.cancelPhotoCreation()
    }
    
    // MARK: - MKMapViewDelegate
    
    public func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        if annotation is MKUserLocation {
            return nil
        } else if annotation is PhotoAnnotationModel {
            let reuseId = PhotoPinAnnotationView.className
            var pinAnnotationView = self.mapView.dequeueReusableAnnotationView(withIdentifier: reuseId) as! PhotoPinAnnotationView?
            if let annotationView = pinAnnotationView {
                annotationView.updateViewWithAnnotation(annotation)
            } else {
                pinAnnotationView = PhotoPinAnnotationView(annotation: annotation, reuseIdentifier: reuseId)
            }
            return pinAnnotationView;
        }
        return nil
    }
    
    public func mapView(_ mapView: MKMapView, didSelect view: MKAnnotationView) {
        if view.annotation is PhotoAnnotationModel {
            selectedPinAnnotationView = view as? PhotoPinAnnotationView
            selectedPinAnnotationView!.addGestureRecognizer(selectedPinTapGestureRecognizer)
            setupLeftImage(forPinAnnotationView: selectedPinAnnotationView, with: UIImage(named:"PlaceholderIcon"))
            moveMapToCenter(of: selectedPinAnnotationView)
            mapPresenter.downloadPicture(for: selectedPinAnnotationView!.annotation as! PhotoAnnotationModel)
        }
    }
    
    public func mapView(_ mapView: MKMapView, didDeselect view: MKAnnotationView) {
        if view.annotation is PhotoAnnotationModel {
            selectedPinAnnotationView = nil
            view.removeGestureRecognizer(selectedPinTapGestureRecognizer)
            mapPresenter.cancelPictureDownloading(for: (view.annotation as? PhotoAnnotationModel)!)
        }
    }
}
