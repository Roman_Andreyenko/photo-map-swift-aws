//
//  PhotoAnnotationModel.swift
//  PhotoMapSwiftAWS
//
//  Created by Roman Andreyenko on 6/5/17.
//  Copyright © 2017 iTechArt. All rights reserved.
//

import MapKit

/**
 Class representation of a map photo annotation
 */
class PhotoAnnotationModel: NSObject, MKAnnotation {
    
    let photoModel: PhotoModel
    let dateManager: DateManager
    
    init(photoModel: PhotoModel, dateManager: DateManager) {
        self.photoModel = photoModel
        self.dateManager = dateManager
    }
    
    var coordinate: CLLocationCoordinate2D {
        return CLLocationCoordinate2D(latitude: photoModel.latitude, longitude: photoModel.longitude)
    }
    
    var title: String? {
        return photoModel.descriptionText.characters.count > 0 ? photoModel.descriptionText : " "
    }
    
    var subtitle: String? {
        return dateManager.MM_dd_yyyy.string(from: photoModel.createdTime)
    }
}
