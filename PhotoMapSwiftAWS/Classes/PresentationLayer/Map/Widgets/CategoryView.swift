//
//  CategoryView.swift
//  PhotoMapSwiftAWS
//
//  Created by Roman Andreyenko on 6/9/17.
//  Copyright © 2017 iTechArt. All rights reserved.
//

import UIKit

/**
 Custom view for photo category
 */
class CategoryView: UIView {
    
    @IBOutlet weak var categoryIconView: UIImageView!
    @IBOutlet weak var categoryTextView: UILabel!
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        self.initialize()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        self.initialize()
    }
    
    private func initialize() {
        let view = UINib(nibName: CategoryView.className, bundle: nil).instantiate(withOwner: self, options: nil).first as! UIView
        view.frame = self.frame
        self.addSubview(view)
    }
    
}
