//
//  PhotoPinAnnotationView.swift
//  PhotoMapSwiftAWS
//
//  Created by Roman Andreyenko on 6/5/17.
//  Copyright © 2017 iTechArt. All rights reserved.
//

import MapKit

/**
 Custom view for map pin photo annotation
 */
class PhotoPinAnnotationView: MKAnnotationView {
    
    private static let CalloutAccessoryViewWidth = 55.0
    private static let CalloutAccessoryViewHeight = 55.0
    
    public class func imageIcon(for category: PhotoModel.Category) -> UIImage? {
        switch category {
        case .default:
            return UIImage(named: "MarkerDefaultIcon")
        case .friends:
            return UIImage(named: "MarkerFriendsIcon")
        case .nature:
            return UIImage(named: "MarkerNatureIcon")
        }
    }
    
    override init(annotation: MKAnnotation?, reuseIdentifier: String?) {
        super.init(annotation: annotation, reuseIdentifier: reuseIdentifier)
        
        self.canShowCallout = true
        let photoView = UIImageView(frame: CGRect(x: 0.0, y: 0.0, width: PhotoPinAnnotationView.CalloutAccessoryViewWidth, height: PhotoPinAnnotationView.CalloutAccessoryViewHeight))
        photoView.contentMode = .scaleAspectFit
        self.leftCalloutAccessoryView = photoView
        let rightButton = UIButton(type: .custom)
        rightButton.setImage(UIImage(named: "ArrowRightIcon"), for: .normal)
        rightButton.sizeToFit()
        rightButton.isUserInteractionEnabled = false
        self.rightCalloutAccessoryView = rightButton
        self.updateViewWithAnnotation(annotation)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    public func updateViewWithAnnotation(_ annotation: MKAnnotation?) {
        self.annotation = annotation;
        let photoAnnotation = annotation as! PhotoAnnotationModel
        self.image = PhotoPinAnnotationView.imageIcon(for: photoAnnotation.photoModel.category)
        if let imageHeight = self.image?.size.height {
            self.calloutOffset = CGPoint(x: 0.0, y: imageHeight / 2.0)
        }
    }
}
