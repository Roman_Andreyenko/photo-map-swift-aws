//
//  MapPresenter.swift
//  PhotoMapSwiftAWS
//
//  Created by Roman Andreyenko on 5/5/17.
//  Copyright © 2017 iTechArt. All rights reserved.
//

import Foundation
import CoreLocation
import UIKit

/**
 Presenter interface(MVP) for MapViewController
 */
protocol MapPresenter: BasePresenter {
    
    /**
     View should call this method to open category filter screen
     */
    func openCategoryFilterScreen()
    
    /**
     View should call this method to open fullscreen photo screen for map photo annotation
     
     - parameters:
        - for: map photo annotation
     */
    func openFullPhotoScreen(for: PhotoAnnotationModel)
    
    /**
     Callback that notifies that categories filter is changed
     
     - parameters:
        - categoriesFilter: new categories filter
     */
    func changed(categoriesFilter: Set<PhotoModel.Category>)

    /**
     View should call this method to change map navigation mode
     */
    func changeMapNavigationMode()
    
    /**
     View should call this method to start process of photo creation
     
     - parameters:
        - withCoordinates: represents a geographical coordinate of potential photo
     */
    func createPhoto(withCoordinates: CLLocationCoordinate2D)
    
    /**
     View should call this method to get photo picture from photo library
     */
    func choosePhotoPictureFromPhotoLibrary()
    
    /**
     View should call this method to take picture with camera
     */
    func takePhotoPictureWithCamera()
    
    /**
     View should call this method to cancel photo creation process and clean up assosiated data
     */
    func cancelPhotoCreation()
    
    /**
     View should call this method when photo picture was created
     
     - parameters:
        - withImage: new created photo picture
     */
    func photoPictureCreated(withImage: UIImage)
    
    /**
     Callback that notifies that photo is created
     
     - parameters:
        - photoModel: new created photo
     */
    func created(photoModel: PhotoModel)
    
    /**
     Callback that notifies that photo creation is cancelled
     */
    func cancelledPhotoCreation()
    
    /**
     View should call this method to fetch all user photos
     */
    func fetchPhotos()
    
    /**
     Callback that notifies that all user photos are fetched
     
     - parameters:
        - photoModels: list of user photos
     */
    func fetched(photoModels: [PhotoModel])
    
    /**
     Callback that notifies that photo fetching is failed
     
     - parameters:
        - error: reason of failure
     */
    func failedPhotoFetching(error: Error)
    
    /**
     View should call this method to download picture for map photo annotation
     
     - parameters:
        - for: map photo annotation
     */
    func downloadPicture(for: PhotoAnnotationModel)
    
    /**
     Callback that notifies that picture for map photo annotation is downloaded
     
     - parameters:
        - pictureData: PhotoPictureModel, which represents downloaded picture
     */
    func downloaded(pictureData: PhotoPictureModel)
    
    /**
     Callback that notifies that picture downloading for map photo annotation is failed
     
     - parameters:
        - error: reason of failure
     */
    func failedPictureDownloading(error: Error)

    /**
     View should call this method to cancel picture downloading for map photo annotation
     
     - parameters:
        - for: map photo annotation
     */
    func cancelPictureDownloading(for: PhotoAnnotationModel)
}
