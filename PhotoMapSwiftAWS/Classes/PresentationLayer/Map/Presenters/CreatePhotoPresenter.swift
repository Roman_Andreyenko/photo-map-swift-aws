//
//  CreatePhotoPresenter.swift
//  PhotoMapSwiftAWS
//
//  Created by Roman Andreyenko on 5/30/17.
//  Copyright © 2017 iTechArt. All rights reserved.
//

import UIKit
import CoreLocation

/**
 Presenter interface(MVP) for CreatePhotoViewController
 */
protocol CreatePhotoPresenter: BasePresenter {

    /**
     View should use this property to get or set photo category
     */
    var category: PhotoModel.Category { get set }

    /**
     View should use this property to get photo picture or nil, if there is no photo
     */
    var photoImage: UIImage? { get }
    
    /**
     View should call this method to initialize presenter
     
     - parameters:
        - withCoordinates: represents a geographical coordinate of potential photo
        - photoPictureData: data representation of fullsize photo picture
        - photoThumbnailPictureData: data representation of thumbnail photo picture
        - creationDate: date of created photo picture
     */
    func initialize(withCoordinates: CLLocationCoordinate2D,
                    photoPictureData: Data,
                    photoThumbnailPictureData: Data,
                    creationDate: Date)
    
    /**
     View should call this method to open fullscreen photo screen for picked photo
     
     - parameters:
        - withPhotoDescriptionText: photo description text
     */
    func openFullPhotoScreen(withPhotoDescriptionText: String)
    
    /**
     View should call this method to cancel photo creation
     */
    func cancelPhotoCreation()
    
    /**
     View should call this method to create photo
     
     - parameters:
        - withDescrpiionText: photo description text
     */
    func createPhoto(withDescriptionText: String)
    
    /**
     Callback that notifies that photo is created
     
     - parameters:
        - photoModel: new created photo
     */
    func created(photoModel: PhotoModel)
    
    /**
     Callback that notifies that photo creation is failed
     
     - parameters:
        - error: reason of failure
     */
    func failedPhotoCreation(error: Error)
    
    /**
     View should call this method to choose photo category
     */
    func chooseCategory()

    /**
     Callback that notifies that new photo category is choosen
    
     - parameters:
        - newCategory: new category
     */
    func picked(newCategory: PhotoModel.Category)
    
    /**
     View should call this method to get formatted text for photo created date
     
     - returns:
        formatted text for photo created date
     */
    func getPhotoCreatedDateFormattedText() -> String
    
    /**
     View should call this method to get formatted text for photo category
     
     - returns:
        formatted text for photo category
     */
    func getPhotoCategoryFormattedText() -> String
}
