//
//  MapPresenterImpl.swift
//  PhotoMapSwiftAWS
//
//  Created by Roman Andreyenko on 5/5/17.
//  Copyright © 2017 iTechArt. All rights reserved.
//

import CoreLocation
import UIKit
import RxCocoa
import RxSwift
import ReachabilitySwift

/*
 Class implementation of MapPresenter
 */
class MapPresenterImpl: MapPresenter {

    private static let maxThumbnailSizeLength: CGFloat = 100.0    
    
    private var coordinates: CLLocationCoordinate2D?
    private var photos: [PhotoModel]!
    
    //Inject
    private unowned let view: MapView
    //Inject
    private let notificationCenter: NotificationCenter
    //Inject
    private let sessionManager: SessionManager
    //Inject
    private let photoInteractor: PhotoInteractor
    //Inject
    private let mapper: PhotoModelDataMapper
    //Inject
    private let dateManager: DateManager
    //Inject
    private let filterManager: PhotoCategoryFilterManager
    
    init(view: MapView, notificationCenter: NotificationCenter, photoInteractor: PhotoInteractor,  mapper: PhotoModelDataMapper, sessionManager: SessionManager, dateManager: DateManager, filterManager: PhotoCategoryFilterManager) {
        self.view = view
        self.notificationCenter = notificationCenter
        self.photoInteractor = photoInteractor
        self.mapper = mapper
        self.sessionManager = sessionManager
        self.dateManager = dateManager
        self.filterManager = filterManager
        
        notificationCenter.addObserver(self, selector: #selector(MapPresenterImpl.created(photoNotification:)), name: .photoCreated, object: nil)
        notificationCenter.addObserver(self, selector: #selector(MapPresenterImpl.cancelled(photoNotification:)), name: .photoCancelled, object: nil)
        notificationCenter.addObserver(self, selector: #selector(MapPresenterImpl.categoryFilterChanged(categoriesNotification:)), name: .categoriesFilterChanged, object: nil)
    }
    
    func willAppear() {
        notificationCenter.addObserver(self, selector: #selector(MapPresenterImpl.reachabilityChanged(note:)), name: ReachabilityChangedNotification, object: AppDelegate.sharedDelegate().reachability)
    }
    
    func willDisappear() {
        notificationCenter.removeObserver(self, name: ReachabilityChangedNotification, object: AppDelegate.sharedDelegate().reachability)
    }
    
    func destroy() {
        photoInteractor.onCleanUp()
        
        notificationCenter.removeObserver(self, name: .photoCreated, object: nil)
        notificationCenter.removeObserver(self, name: .photoCancelled, object: nil)
        notificationCenter.removeObserver(self, name: .categoriesFilterChanged, object: nil)
    }
    
    func openCategoryFilterScreen() {
        view.openCategoryFilterScreen()
    }
    
    func openFullPhotoScreen(for photoAnnotation: PhotoAnnotationModel) {
        view.openFullPhotoScreen(for: photoAnnotation)
    }
    
    func changed(categoriesFilter: Set<PhotoModel.Category>) {
        view.closeCategoryFilterScreen()
        view.showMapPins(withPhotoAnnotationModels: createPhotoAnnotationWarp(of: photos))
    }
    
    func changeMapNavigationMode() {
        view.changeMapNavigationMode()
    }
    
    func createPhoto(withCoordinates coordinates: CLLocationCoordinate2D) {
        self.coordinates = coordinates
        view.showPictureCreationChooserView()
    }
    
    func choosePhotoPictureFromPhotoLibrary() {
        view.openPhotoLibrary()
    }
    
    func takePhotoPictureWithCamera() {
        view.openPhotoCamera()
    }
    
    func cancelPhotoCreation() {
        self.coordinates = nil
    }
    
    func photoPictureCreated(withImage image: UIImage) {
        let thumbnailImage = image.resizeImageWithMaxSizeLength(MapPresenterImpl.maxThumbnailSizeLength)!
        let pictureData = UIImageJPEGRepresentation(image, 1.0)!
        let thumbnailPictureData = UIImageJPEGRepresentation(thumbnailImage, 0.5)!
        view.showPhotoCreationContainerView()
        view.openPhotoDetailsScreen(withCoordinates: self.coordinates!, photoPictureData: pictureData, photoThumbnailPictureData: thumbnailPictureData, creationDate: Date())
    }
    
    func created(photoModel: PhotoModel) {
        view.hidePhotoCreationContainerView()
        fetchPhotos()
    }

    func cancelledPhotoCreation() {
        cancelPhotoCreation()
        view.hidePhotoCreationContainerView()
    }
    
    func fetchPhotos() {
        guard let user = sessionManager.getCurrentUser() else {
            return
        }
        let photosObserver = AnyObserver<[Photo]> { [unowned self] event in
            switch event {
            case .next(let value):
                self.view.hideActivityIndicator()
                self.fetched(photoModels: self.mapper.transform(photoList: value))
            case .error(let error):
                self.view.hideActivityIndicator()
                self.failedPhotoFetching(error: error)
            case .completed:
                return
            }
        }
        
        view.showActivityIndicator()
        photoInteractor.fetchPhotos(forUserId: user.username, withSearchText: "", observer: photosObserver)
    }
    
    func fetched(photoModels: [PhotoModel]) {
        photos = photoModels
        view.showMapPins(withPhotoAnnotationModels: createPhotoAnnotationWarp(of: photoModels))
    }

    func failedPhotoFetching(error: Error) {
        view.show(error: error)
    }
    
    func downloadPicture(for photoAnnotationModel: PhotoAnnotationModel) {
        let pictureObserver = AnyObserver<PhotoPicture> { [unowned self] event in
            switch event {
            case .next(let value):
                self.downloaded(pictureData: self.mapper.transform(photoPicture: value))
            case .error(let error):
                self.failedPictureDownloading(error: error)
            case .completed:
                return
            }
        }
        
        photoInteractor.downloadThumbnailPicture(of: photoAnnotationModel.photoModel, observer: pictureObserver)
    }
    
    func downloaded(pictureData: PhotoPictureModel) {
        view.showSelectedPhotoAnnotationPicture(with: pictureData.data)
    }
    
    func failedPictureDownloading(error: Error) {
        view.show(error: error)
    }
    
    func cancelPictureDownloading(for photoAnnotationModel: PhotoAnnotationModel) {
        photoInteractor.cancelThumbnailPictureDownloading(of: photoAnnotationModel.photoModel)
    }
    
    @objc
    private func created(photoNotification: Notification) {
        created(photoModel: photoNotification.object! as! PhotoModel)
    }
    
    @objc
    private func cancelled(photoNotification: Notification) {
        cancelledPhotoCreation()
    }
    
    @objc
    private func categoryFilterChanged(categoriesNotification: Notification) {
        changed(categoriesFilter: categoriesNotification.object! as! Set<PhotoModel.Category>)
    }
    
    private func createPhotoAnnotationWarp(of photoModels: [PhotoModel]) -> [PhotoAnnotationModel] {
        var photoAnnotations = [PhotoAnnotationModel]()
        photoAnnotations.reserveCapacity(photoModels.count)
        for photo in photoModels {
            if filterManager.isFiltered(category: photo.category) {
                photoAnnotations.append(PhotoAnnotationModel(photoModel: photo, dateManager: dateManager))
            }
        }
        
        return photoAnnotations
    }
    
    @objc private func reachabilityChanged(note: Notification) {
        let reachability = note.object as! Reachability
        if reachability.isReachable {
            fetchPhotos()
        } else {
            view.showSettingsAlert()
        }
    }
}
