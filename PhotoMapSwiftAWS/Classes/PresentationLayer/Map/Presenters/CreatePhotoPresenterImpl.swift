//
//  CreatePhotoPresenterImpl.swift
//  PhotoMapSwiftAWS
//
//  Created by Roman Andreyenko on 5/30/17.
//  Copyright © 2017 iTechArt. All rights reserved.
//

import CoreLocation
import RxCocoa
import RxSwift
import ReachabilitySwift

/*
 Class implementation of CreatePhotoPresenter
 */
class CreatePhotoPresenterImpl: CreatePhotoPresenter {

    var category: PhotoModel.Category
    
    var photoImage: UIImage? {
        return UIImage(data: photoPictureData)
    }

    private var coordinate: CLLocationCoordinate2D!
    private var photoPictureData: Data!
    private var photoThumbnailPictureData: Data!
    private var creationDate: Date!
    
    //Inject
    private unowned let view: CreatePhotoView
    //Inject
    private let interactor: PhotoInteractor
    //Inject
    private let mapper: PhotoModelDataMapper
    //Inject
    private let notificationCenter: NotificationCenter
    //Inject
    private let sessionManager: SessionManager
    //Inject
    private let dateManager: DateManager
    
    init(view: CreatePhotoView, interactor: PhotoInteractor, mapper: PhotoModelDataMapper, notificationCenter: NotificationCenter, sessionManager: SessionManager, dateManager: DateManager) {
        self.view = view
        self.interactor = interactor
        self.mapper = mapper
        self.category = .default
        self.notificationCenter = notificationCenter
        self.sessionManager = sessionManager
        self.dateManager = dateManager
    }
    
    func initialize(withCoordinates coordinate: CLLocationCoordinate2D,
                    photoPictureData: Data,
                    photoThumbnailPictureData: Data,
                    creationDate: Date) {
        self.coordinate = coordinate
        self.photoPictureData = photoPictureData
        self.photoThumbnailPictureData = photoThumbnailPictureData
        self.creationDate = creationDate
    }
    
    func openFullPhotoScreen(withPhotoDescriptionText photoDescriptionText: String) {
        let photo = PhotoModel(photoId: "", ownerId: "", createdTime: creationDate, category: category, descriptionText: photoDescriptionText, latitude: coordinate.latitude, longitude: coordinate.longitude)
        let picture = PhotoPictureModel(photoId: "", type: .fullQuality, data: photoPictureData)
        view.openFullPhotoScreen(for: photo, withPicture: picture)
    }
    
    func cancelPhotoCreation() {
        view.closeCreatePhotoScreen()
        notificationCenter.post(name: .photoCancelled, object: nil)
    }

    func createPhoto(withDescriptionText descriptionText: String) {
        guard let user = sessionManager.getCurrentUser() else {
            return
        }
        let photoObserver = AnyObserver<Photo> { [unowned self] event in
            switch event {
            case .next(let value):
                self.view.hideActivityIndicator()
                self.created(photoModel: self.mapper.transform(photo: value))
            case .error(let error):
                self.view.hideActivityIndicator()
                self.failedPhotoCreation(error: error)
            case .completed:
                return
            }
        }
        view.showActivityIndicator()
        interactor.createPhoto(withPictureData: photoPictureData, withThumbnailPictureData: photoThumbnailPictureData, withOwnerId: user.username, withCategory: mapper.transform(categoryModel: category), withDescriptionText: descriptionText, withLatitude: coordinate.latitude, withLongitude: coordinate.longitude, withCreatedTime: creationDate, observer: photoObserver)
    }
    
    func created(photoModel: PhotoModel) {
        view.closeCreatePhotoScreen()
        notificationCenter.post(name: .photoCreated, object: photoModel)
    }

    func failedPhotoCreation(error: Error) {
        view.show(error: error)
    }
    
    func chooseCategory() {
        view.showCategoryPickerContainerView()
        view.openChooseCategoryScreen(withCategory: category)
    }
    
    func picked(newCategory category: PhotoModel.Category) {
        self.category = category
        view.hideCategoryPickerContainerView()
        view.setUpCategoryView()
    }
    
    func willAppear() {
        setupPhotoCreationAvailability(reachability: AppDelegate.sharedDelegate().reachability)
        notificationCenter.addObserver(self, selector: #selector(CreatePhotoPresenterImpl.reachabilityChanged(note:)), name: ReachabilityChangedNotification, object: AppDelegate.sharedDelegate().reachability)
    }

    func willDisappear() {
        notificationCenter.removeObserver(self, name: ReachabilityChangedNotification, object: AppDelegate.sharedDelegate().reachability)
    }

    func destroy() {
        interactor.onCleanUp()
    }
    
    func getPhotoCreatedDateFormattedText() -> String {
        return String(format: dateManager.MMMM_d_suffix_comma_yyyy_hyphen_h_mm_a.string(from: creationDate), type(of: dateManager).daySuffix(for: creationDate))
    }
    
    func getPhotoCategoryFormattedText() -> String {
        return NSLocalizedString(category.rawValue, comment: "").uppercased()
    }
    
    private func setupPhotoCreationAvailability(reachability: Reachability) {
        if reachability.isReachable {
            view.enablePhotoCreation()
        } else {
            view.disablePhotoCreation()
        }
    }
    
    @objc private func reachabilityChanged(note: Notification) {
        let reachability = note.object as! Reachability
        setupPhotoCreationAvailability(reachability: reachability)
        if !reachability.isReachable {
            view.showSettingsAlert()
        }
    }
}
