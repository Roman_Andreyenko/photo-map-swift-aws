//
//  CategoryPickerPresenter.swift
//  PhotoMapSwiftAWS
//
//  Created by Roman Andreyenko on 6/9/17.
//  Copyright © 2017 iTechArt. All rights reserved.
//

import Foundation

/**
 Presenter interface(MVP) for CategoryPickerViewController
 */
protocol CategoryPickerPresenter: BasePresenter {

    /**
     View should call this method to initialize presenter
     
     - parameters:
        - withCategory: photo category
     */
    func initialize(withCategory: PhotoModel.Category)
    
    /**
     View should use this property to get list of categories
     */
    var categories: [PhotoModel.Category] { get }
    
    /**
     View should call this method to get current photo category
     
     - returns:
        current category
     */
    func getCurrentCategory() -> PhotoModel.Category
    
    /**
     View should call this method to pick new photo category
     
     - parameters:
        - newCategory: new category
     */
    func pick(newCategory: PhotoModel.Category)
}
