//
//  CategoryPickerPresenterImpl.swift
//  PhotoMapSwiftAWS
//
//  Created by Roman Andreyenko on 6/9/17.
//  Copyright © 2017 iTechArt. All rights reserved.
//

import UIKit

/*
 Class implementation of CategoryPickerPresenter
 */
class CategoryPickerPresenterImpl: CategoryPickerPresenter {

    lazy var categories: [PhotoModel.Category] = [.default, .friends, .nature]
    private var category: PhotoModel.Category!

    //Inject
    private unowned let view: CategoryPickerView
    
    init(view: CategoryPickerView) {
        self.view = view
    }
    
    func initialize(withCategory category: PhotoModel.Category) {
        self.category = category
    }
    
    func getCurrentCategory() -> PhotoModel.Category {
        return category
    }
    
    func pick(newCategory category: PhotoModel.Category) {
        self.category = category
        view.pick(newCategory: category)
        view.closeCategoryPickerScreen()
    }
    
    func willAppear() {
    }

    func willDisappear() {
    }
    
    func destroy() {   
    }
}
