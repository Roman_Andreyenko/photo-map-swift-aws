//
//  CreatePhotoAssembly.swift
//  PhotoMapSwiftAWS
//
//  Created by Roman Andreyenko on 5/31/17.
//  Copyright © 2017 iTechArt. All rights reserved.
//

import Swinject

/**
 Swinject assembly which provides dependency bindings for CreatePhotoComponent
 */
class CreatePhotoAssembly: Assembly {

    private unowned let view: CreatePhotoView
    
    init(view: CreatePhotoView) {
        self.view = view
    }
    
    func assemble(container: Container) {
        assembleCreatePhotoView(container: container)
        assemleCreatePhotoPresenter(container: container)
    }
    
    private func assembleCreatePhotoView(container: Container) {
        container.register(CreatePhotoView.self) { r in
            return self.view
        }
    }
    
    private func assemleCreatePhotoPresenter(container: Container) {
        container.autoregister(CreatePhotoPresenter.self, initializer: CreatePhotoPresenterImpl.init)
    }
}
