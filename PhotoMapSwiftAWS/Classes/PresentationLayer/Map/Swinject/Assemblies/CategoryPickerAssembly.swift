//
//  CategoryPickerAssembly.swift
//  PhotoMapSwiftAWS
//
//  Created by Roman Andreyenko on 6/9/17.
//  Copyright © 2017 iTechArt. All rights reserved.
//

import Swinject

/**
 Swinject assembly which provides dependency bindings for CategoryPickerComponent
 */
class CategoryPickerAssembly: Assembly {

    private unowned let view: CategoryPickerView
    
    init(view: CategoryPickerView) {
        self.view = view
    }
    
    func assemble(container: Container) {
        assembleCategoryPickerView(container: container)
        assemleCategoryPickerPresenter(container: container)
    }
    
    private func assembleCategoryPickerView(container: Container) {
        container.register(CategoryPickerView.self) { r in
            return self.view
        }
    }
    
    private func assemleCategoryPickerPresenter(container: Container) {
        container.autoregister(CategoryPickerPresenter.self, initializer: CategoryPickerPresenterImpl.init)
    }
}
