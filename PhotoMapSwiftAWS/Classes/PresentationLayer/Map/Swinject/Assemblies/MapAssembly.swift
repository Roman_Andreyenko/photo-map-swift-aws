//
//  MapAssembly.swift
//  PhotoMapSwiftAWS
//
//  Created by Roman Andreyenko on 5/5/17.
//  Copyright © 2017 iTechArt. All rights reserved.
//

import Swinject

/**
 Swinject assembly which provides dependency bindings for MapComponent
 */
class MapAssembly: Assembly {

    private unowned let view: MapView
    
    init(view: MapView) {
        self.view = view
    }
    
    func assemble(container: Container) {
        assembleMapView(container: container)
        assemleMapPresenter(container: container)
    }
    
    private func assembleMapView(container: Container) {
        container.register(MapView.self) { r in
            return self.view
        }
    }
    
    private func assemleMapPresenter(container: Container) {
        container.autoregister(MapPresenter.self, initializer: MapPresenterImpl.init)
    }
}
