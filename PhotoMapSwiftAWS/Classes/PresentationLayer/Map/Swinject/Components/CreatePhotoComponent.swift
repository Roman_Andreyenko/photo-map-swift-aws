//
//  CreatePhotoComponent.swift
//  PhotoMapSwiftAWS
//
//  Created by Roman Andreyenko on 5/31/17.
//  Copyright © 2017 iTechArt. All rights reserved.
//

import Swinject

/**
 Swinject component for CreatePhotoViewController
 */
class CreatePhotoComponent: BaseComponent {

    init(applicationComponent: ApplicationComponent, createPhotoAssembly: CreatePhotoAssembly) {
        super.init()
        self.assembler = try! Assembler(assemblies: [createPhotoAssembly], parentAssembler: applicationComponent.assembler)
    }
}
