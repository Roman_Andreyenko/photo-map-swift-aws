//
//  MapComponent.swift
//  PhotoMapSwiftAWS
//
//  Created by Roman Andreyenko on 5/5/17.
//  Copyright © 2017 iTechArt. All rights reserved.
//

import Swinject

/**
 Swinject component for MapViewController
 */
class MapComponent: BaseComponent {

    init(applicationComponent: ApplicationComponent, mapAssembly: MapAssembly) {
        super.init()
        self.assembler = try! Assembler(assemblies: [mapAssembly], parentAssembler: applicationComponent.assembler)
    }
}
