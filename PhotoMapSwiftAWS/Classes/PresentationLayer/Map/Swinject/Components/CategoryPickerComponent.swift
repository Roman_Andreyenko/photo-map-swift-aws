//
//  CategoryPickerComponent.swift
//  PhotoMapSwiftAWS
//
//  Created by Roman Andreyenko on 6/9/17.
//  Copyright © 2017 iTechArt. All rights reserved.
//

import Swinject

/**
 Swinject component for CategoryPickerViewController
 */
class CategoryPickerComponent: BaseComponent {

    init(applicationComponent: ApplicationComponent, categoryPickerAssembly: CategoryPickerAssembly) {
        super.init()
        self.assembler = try! Assembler(assemblies: [categoryPickerAssembly], parentAssembler: applicationComponent.assembler)
    }
}
