//
//  MapView.swift
//  PhotoMapSwiftAWS
//
//  Created by Roman Andreyenko on 5/5/17.
//  Copyright © 2017 iTechArt. All rights reserved.
//

import Foundation
import CoreLocation

/**
 View interface(MVP) for MapViewController
 */
protocol MapView: BaseLoadingView {
    
    /**
     Opens category filter screen
     */
    func openCategoryFilterScreen()

    /**
     Opens fullscreen photo screen for map photo annotation
     
     - parameters:
        - for: map photo annotation
     */
    func openFullPhotoScreen(for: PhotoAnnotationModel)
    
    /**
     Closes category filter screen
     */
    func closeCategoryFilterScreen()
    
    /**
     Changes map navigation mode
     */
    func changeMapNavigationMode()
    
    /**
     Shows view that offers the ways user can creates photo's picture
     */
    func showPictureCreationChooserView()
    
    /**
     Opens photo library
     */
    func openPhotoLibrary()

    /**
     Opens photo camera
     */
    func openPhotoCamera()
    
    /**
     Opens photo details screen
     
     - parameters:
        - withCoordinates: represents a geographical coordinate of potential photo
        - photoPictureData: data representation of fullsize photo picture
        - photoThumbnailPictureData: data representation of thumbnail photo picture
        - creationDate: date of created photo picture
     */
    func openPhotoDetailsScreen(withCoordinates: CLLocationCoordinate2D, photoPictureData: Data, photoThumbnailPictureData: Data, creationDate: Date)
    
    /**
     Shows photo creation container view
     */
    func showPhotoCreationContainerView()
    
    /**
     Hides photo creation container view
     */
    func hidePhotoCreationContainerView()
    
    /**
     Shows map pins of photos
     
     - parameters:
        - withPhotoAnnotationModels: photo annotation list
     */
    func showMapPins(withPhotoAnnotationModels: [PhotoAnnotationModel])
    
    /**
     Shows photo annotation picture for selected map pin
     
     - parameters:
        - with: data that represents picture
     */
    func showSelectedPhotoAnnotationPicture(with: Data)
}
