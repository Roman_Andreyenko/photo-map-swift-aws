//
//  CategoryPickerView.swift
//  PhotoMapSwiftAWS
//
//  Created by Roman Andreyenko on 6/9/17.
//  Copyright © 2017 iTechArt. All rights reserved.
//

import UIKit

/**
 View interface(MVP) for CategoryPickerViewController
 */
protocol CategoryPickerView: class {
    
    /**
     Picks new photo category
     
     - parameters:
        - newCategory: new category
     */
    func pick(newCategory: PhotoModel.Category)
    
    /**
     Closes category picker screen
     */
    func closeCategoryPickerScreen()
}
