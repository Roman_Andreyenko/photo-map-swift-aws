//
//  CreatePhotoView.swift
//  PhotoMapSwiftAWS
//
//  Created by Roman Andreyenko on 5/30/17.
//  Copyright © 2017 iTechArt. All rights reserved.
//

import UIKit

/**
 View interface(MVP) for CreatePhotoViewController
 */
protocol CreatePhotoView: BaseLoadingView {

    /**
     Closes create photo screen
     */
    func closeCreatePhotoScreen()
    
    /**
     Opens choose category screen
     
     - parameters:
        - withCategory: current photo category
     */
    func openChooseCategoryScreen(withCategory: PhotoModel.Category)
    
    /**
     Opens fullscreen photo screen for picked photo
     
     - parameters:
        - for: photo model
        - withPicture: picture model
     */
    func openFullPhotoScreen(for: PhotoModel, withPicture: PhotoPictureModel)
    
    /**
     Setups category view appearence
     */
    func setUpCategoryView()
    
    /**
     Shows photo category picker container view
     */
    func showCategoryPickerContainerView()
    
    /**
     Hides photo category picker container view
     */
    func hideCategoryPickerContainerView()
    
    /**
     Enabled photo creation functionality
     */
    func enablePhotoCreation()
    
    /**
     Disabled photo creation functionality
     */
    func disablePhotoCreation()
}
