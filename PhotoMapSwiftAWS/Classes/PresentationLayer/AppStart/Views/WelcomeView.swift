//
//  WelcomeView.swift
//  PhotoMapSwiftAWS
//
//  Created by Roman Andreyenko on 4/26/17.
//  Copyright © 2017 iTechArt. All rights reserved.
//

import Foundation

/**
 View interface(MVP) for WelcomeViewController
 */
protocol WelcomeView: class {
    
    /**
     Opens log in screen
     */
    func openLogInScreen()
    
    /**
     Opens sign up screen
     */
    func openSignUpScreen()
}
