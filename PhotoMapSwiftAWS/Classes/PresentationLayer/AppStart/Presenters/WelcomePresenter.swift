//
//  WelcomePresenter.swift
//  PhotoMapSwiftAWS
//
//  Created by Roman Andreyenko on 4/26/17.
//  Copyright © 2017 iTechArt. All rights reserved.
//

import Foundation

/**
 Presenter interface(MVP) for WelcomeViewController
 */
protocol WelcomePresenter: BasePresenter {
    
    /**
     View should call this method to open log in screen
     */
    func openLoginScreen()
    
    /**
     View should call this method to open sign up screen
     */
    func openSignupScreen()
}
