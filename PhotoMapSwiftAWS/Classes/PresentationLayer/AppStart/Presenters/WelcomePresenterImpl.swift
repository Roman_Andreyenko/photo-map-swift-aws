//
//  WelcomePresenterImpl.swift
//  PhotoMapSwiftAWS
//
//  Created by Roman Andreyenko on 4/26/17.
//  Copyright © 2017 iTechArt. All rights reserved.
//

import Swinject

/**
 Class implementation of WelcomePresenter
 */
class WelcomePresenterImpl: WelcomePresenter {
    
    //Inject
    private unowned let view: WelcomeView
    
    init(view: WelcomeView) {
        self.view = view
    }
    
    func openLoginScreen() {
        view.openLogInScreen()
    }
    
    func openSignupScreen() {
        view.openSignUpScreen()
    }
    
    func willAppear() {
    }
    
    func willDisappear() {
    }
    
    func destroy() {
    }
}
