//
//  WelcomeAssembly.swift
//  PhotoMapSwiftAWS
//
//  Created by Roman Andreyenko on 4/26/17.
//  Copyright © 2017 iTechArt. All rights reserved.
//

import Swinject
import SwinjectAutoregistration

/**
 Swinject assembly which provides dependency bindings for WelcomeComponent
 */
class WelcomeAssembly: Assembly {

    private unowned let view: WelcomeView
    
    init(view: WelcomeView) {
        self.view = view
    }
    
    func assemble(container: Container) {
        assembleWelcomeView(container: container)
        assemleWelcomePresenter(container: container)
    }
    
    private func assembleWelcomeView(container: Container) {
        container.register(WelcomeView.self) { r in
            return self.view
            }
    }
    
    private func assemleWelcomePresenter(container: Container) {
        container.autoregister(WelcomePresenter.self, initializer: WelcomePresenterImpl.init)
    }
}
