//
//  WelcomeComponent.swift
//  PhotoMapSwiftAWS
//
//  Created by Roman Andreyenko on 4/26/17.
//  Copyright © 2017 iTechArt. All rights reserved.
//

import Swinject

/**
 Swinject component for WelcomeViewController
 */
class WelcomeComponent: BaseComponent {
    
    init(applicationComponent: ApplicationComponent, welcomeAssembly: WelcomeAssembly) {
        super.init()
        self.assembler = try! Assembler(assemblies: [welcomeAssembly], parentAssembler: applicationComponent.assembler)
    }
}
