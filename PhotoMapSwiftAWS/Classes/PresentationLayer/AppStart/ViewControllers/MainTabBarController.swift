//
//  MainTabBarController.swift
//  PhotoMapSwiftAWS
//
//  Created by Roman Andreyenko on 4/20/17.
//  Copyright © 2017 iTechArt. All rights reserved.
//

import UIKit

/**
 Class implementation of main view controller for the app
 */
class MainTabBarController: UITabBarController {
    
    // MARK: - UIViewController lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()

        let mapViewController = BaseViewController.newInstance(of: MapViewController.self)
        mapViewController.title = NSLocalizedString("Map", comment: "")
        mapViewController.tabBarItem.image = UIImage(named: "MapIcon.png")
        
        let timelineViewController = BaseViewController.newInstance(of: TimelineViewController.self)
        let timelineTabNavigationController = UINavigationController(rootViewController: timelineViewController)
        timelineTabNavigationController.title = NSLocalizedString("Timeline", comment: "")
        timelineTabNavigationController.tabBarItem.image = UIImage(named: "TimelineIcon.png")
        
        let moreViewController = BaseViewController.newInstance(of: LogoutViewController.self)
        moreViewController.title = NSLocalizedString("More", comment: "")
        moreViewController.tabBarItem.image = UIImage(named: "MoreIcon.png")
        
        viewControllers = [mapViewController, timelineTabNavigationController, moreViewController]
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}
