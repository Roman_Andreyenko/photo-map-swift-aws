//
//  WelcomeViewController.swift
//  PhotoMapSwiftAWS
//
//  Created by Roman Andreyenko on 4/21/17.
//  Copyright © 2017 iTechArt. All rights reserved.
//

import UIKit
import Swinject
import SwinjectAutoregistration

/**
 Class implementation of view controller to welcome unauthorized user
 */
class WelcomeViewController: BaseViewController, WelcomeView {
    
    //Inject
    private var welcomePresenter: WelcomePresenter!
    
    // MARK: - UIViewController lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - Swinject
    
    override func setupSwinjectMembers() -> BaseComponent? {
        return WelcomeComponent(applicationComponent: AppDelegate.sharedDelegate().getAppComponent(), welcomeAssembly: WelcomeAssembly(view: self))
    }
    
    override func resolveDependencies(withResolver r: Resolver) {
        welcomePresenter = r ~> WelcomePresenter.self
    }
    
    // MARK: - WelcomeView
    
    func openLogInScreen() {
        self.navigationController?.pushViewController(BaseViewController.newInstance(of: LoginViewController.self), animated: true)
    }
    
    func openSignUpScreen() {
        self.navigationController?.pushViewController(BaseViewController.newInstance(of: SignupViewController.self), animated: true)
    }

    // MARK: - UI logic methods
    
    @IBAction func signUpClick(_ sender: UIButton) {
        welcomePresenter.openSignupScreen()
    }
    
    @IBAction func logInClick(_ sender: UIButton) {
        welcomePresenter.openLoginScreen()
    }
}
