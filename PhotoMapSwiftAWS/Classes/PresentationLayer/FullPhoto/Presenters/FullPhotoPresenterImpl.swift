//
//  FullPhotoPresenterImpl.swift
//  PhotoMapSwiftAWS
//
//  Created by Roman Andreyenko on 7/17/17.
//  Copyright © 2017 iTechArt. All rights reserved.
//

import RxCocoa
import RxSwift
import ReachabilitySwift

/*
 Class implementation of FullPhotoPresenter
 */
class FullPhotoPresenterImpl: FullPhotoPresenter {

    private static let PictureZoomFactor: Float = 2.0
    
    private var photo: PhotoModel!
    private var photoPicture: PhotoPictureModel?
    
    //Inject
    private unowned let view: FullPhotoView
    //Inject
    private let interactor: PhotoInteractor
    //Inject
    private let mapper: PhotoModelDataMapper
    //Inject
    private let dateManager: DateManager
    //Inject
    private let notificationCenter: NotificationCenter
    
    init(view: FullPhotoView, interactor: PhotoInteractor, mapper: PhotoModelDataMapper, dateManager: DateManager, notificationCenter: NotificationCenter) {
        self.view = view
        self.interactor = interactor
        self.mapper = mapper
        self.dateManager = dateManager
        self.notificationCenter = notificationCenter
    }
    
    func initialize(withPhoto photo: PhotoModel, withPicture photoPicture: PhotoPictureModel?) {
        self.photo = photo
        self.photoPicture = photoPicture
    }
    
    func willAppear() {
        notificationCenter.addObserver(self, selector: #selector(FullPhotoPresenterImpl.reachabilityChanged(note:)), name: ReachabilityChangedNotification, object: AppDelegate.sharedDelegate().reachability)
    }
    
    func willDisappear() {
        notificationCenter.removeObserver(self, name: ReachabilityChangedNotification, object: AppDelegate.sharedDelegate().reachability)
    }
    
    func destroy() {
        interactor.onCleanUp()
    }
    
    func loadPhoto() {
        if let photoPicture = photoPicture {
            loaded(photoPicture: photoPicture)
        } else {
            if AppDelegate.sharedDelegate().reachability.isReachable {
                let pictureObserver = AnyObserver<PhotoPicture> { [unowned self] event in
                    switch event {
                    case .next(let value):
                        self.view.hideActivityIndicator()
                        self.loaded(photoPicture: self.mapper.transform(photoPicture: value))
                    case .error(let error):
                        self.view.hideActivityIndicator()
                        self.failedToLoadPhotoPicture(error: error)
                    case .completed:
                        return
                    }
                }
                
                view.showActivityIndicator()
                interactor.downloadPicture(of: photo, observer: pictureObserver)
            } else {
                view.showSettingsAlert()
            }
        }
    }
    
    func loaded(photoPicture picture: PhotoPictureModel) {
        view.show(photoImage: UIImage(data: picture.data)!)
    }
    
    func failedToLoadPhotoPicture(error: Error) {
        view.show(error: error)
    }
    
    func hideFullPhotoScreen() {
        view.hideFullPhotoScreen()
    }
    
    func getDescriptionText() -> String {
        return photo.descriptionText
    }
    
    func getCreatedDateFormattedText() -> String {
        return String(format: dateManager.MMMM_d_suffix_comma_yyyy_at_h_mm_a.string(from: photo.createdTime), type(of: dateManager).daySuffix(for: photo.createdTime))
    }
    
    func zoomPicture() {
        view.zoomPicture(withScale: FullPhotoPresenterImpl.PictureZoomFactor)
    }
    
    func changeBarsVisibility() {
        view.changeBarsVisibility()
    }
    
    @objc private func reachabilityChanged(note: Notification) {
        let reachability = note.object as! Reachability
        if reachability.isReachable {
            loadPhoto()
        } else {
            view.showSettingsAlert()
        }
    }
}
