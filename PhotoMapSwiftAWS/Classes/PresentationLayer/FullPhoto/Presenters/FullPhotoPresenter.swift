//
//  FullPhotoPresenter.swift
//  PhotoMapSwiftAWS
//
//  Created by Roman Andreyenko on 7/17/17.
//  Copyright © 2017 iTechArt. All rights reserved.
//

import Foundation

/**
 Presenter interface(MVP) for FullPhotoViewController
 */
protocol FullPhotoPresenter: BasePresenter {
    
    /**
     View should call this method to initialize presenter
     
     - parameters:
        - withPhoto: photo model
        - withPicture: downloaded picture representation, or nil - if it hasn't been downloaded yet
     */
    func initialize(withPhoto: PhotoModel, withPicture: PhotoPictureModel?)
    
    /**
     View should call this method to load photo picture
     */
    func loadPhoto()
    
    /**
     Callback that notifies that photo picture is loaded
     
     - parameters:
        - photoPicture: loaded picture
     */
    func loaded(photoPicture: PhotoPictureModel)
    
    /**
     Callback that notifies that photo loading is failed
     
     - parameters:
        - error: reason of failure
     */
    func failedToLoadPhotoPicture(error: Error)
    
    /**
     View should call this method to hide full photo screen
     */
    func hideFullPhotoScreen()
    
    /**
     View should call this method to get formatted text for photo description
     
     - returns:
        formatted text for photo description
     */
    func getDescriptionText() -> String
    
    /**
     View should call this method to get formatted text for photo created date
     
     - returns:
        formatted text for photo created date
     */
    func getCreatedDateFormattedText() -> String
    
    /**
     View should call this method to zoom picture
     */
    func zoomPicture()
    
    /**
     View should call this method to show/hide top and bottom bars
     */
    func changeBarsVisibility()
}
