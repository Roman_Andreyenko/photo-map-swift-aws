//
//  FullPhotoAssembly.swift
//  PhotoMapSwiftAWS
//
//  Created by Roman Andreyenko on 7/19/17.
//  Copyright © 2017 iTechArt. All rights reserved.
//

import Swinject

/**
 Swinject assembly which provides dependency bindings for FullPhotoComponent
 */
class FullPhotoAssembly: Assembly {

    private unowned let view: FullPhotoView
    
    init(view: FullPhotoView) {
        self.view = view
    }
    
    func assemble(container: Container) {
        assembleFullPhotoView(container: container)
        assemleFullPhotoPresenter(container: container)
    }
    
    private func assembleFullPhotoView(container: Container) {
        container.register(FullPhotoView.self) { r in
            return self.view
        }
    }
    
    private func assemleFullPhotoPresenter(container: Container) {
        container.autoregister(FullPhotoPresenter.self, initializer: FullPhotoPresenterImpl.init)
    }
}
