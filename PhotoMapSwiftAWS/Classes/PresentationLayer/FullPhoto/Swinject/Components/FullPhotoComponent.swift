//
//  FullPhotoComponent.swift
//  PhotoMapSwiftAWS
//
//  Created by Roman Andreyenko on 7/19/17.
//  Copyright © 2017 iTechArt. All rights reserved.
//

import Swinject

/**
 Swinject component for FullPhotoViewController
 */
class FullPhotoComponent: BaseComponent {

    init(applicationComponent: ApplicationComponent, fullPhotoAssembly: FullPhotoAssembly) {
        super.init()
        self.assembler = try! Assembler(assemblies: [fullPhotoAssembly], parentAssembler: applicationComponent.assembler)
    }
}
