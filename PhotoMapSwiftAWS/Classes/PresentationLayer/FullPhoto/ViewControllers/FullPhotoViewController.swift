//
//  FullPhotoViewController.swift
//  PhotoMapSwiftAWS
//
//  Created by Roman Andreyenko on 7/17/17.
//  Copyright © 2017 iTechArt. All rights reserved.
//

import UIKit

import Swinject
import SwinjectAutoregistration

/**
 Class implementation of view controller for fullscreen photo picture
 */
class FullPhotoViewController: BaseLoadingViewController, FullPhotoView, UIScrollViewDelegate {

    @IBOutlet private weak var scrollView: UIScrollView!
    @IBOutlet private weak var topBarView: UIView!
    @IBOutlet private weak var bottomBarView: UIView!
    @IBOutlet private weak var photoDescriptionView: UILabel!
    @IBOutlet private weak var photoCreationDateView: UITextField!
    @IBOutlet private weak var photoView: UIImageView!
    
    @IBOutlet private weak var hideTopBottomBarTapGestureRecognizer: UITapGestureRecognizer!
    @IBOutlet private weak var zoomPhotoTapGestureRecognizer: UITapGestureRecognizer!
    
    //Inject
    private var fullPhotoPresenter: FullPhotoPresenter!
    
    // MARK: - init & deinit
    
    deinit {
        fullPhotoPresenter.destroy()
    }
    
    public static func newInstance(withPhoto photo: PhotoModel, withPicture picture: PhotoPictureModel?) -> FullPhotoViewController {
        let viewController = BaseViewController.newInstance(of: FullPhotoViewController.self)
        viewController.fullPhotoPresenter.initialize(withPhoto: photo, withPicture: picture)
        
        return viewController
    }
    
    // MARK: - UIViewController lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupViews()
        fullPhotoPresenter.loadPhoto()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        fullPhotoPresenter.willAppear()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        fullPhotoPresenter.willDisappear()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override var prefersStatusBarHidden: Bool {
        return true
    }
    
    // MARK: - Swinject
    
    public override func setupSwinjectMembers() -> BaseComponent? {
        return FullPhotoComponent(applicationComponent: AppDelegate.sharedDelegate().getAppComponent(),
                                 fullPhotoAssembly: FullPhotoAssembly(view: self))
    }
    
    public override func resolveDependencies(withResolver r: Resolver) {
        fullPhotoPresenter = r ~> FullPhotoPresenter.self
    }
    
    // MARK: - FullPhotoView
    
    override func showActivityIndicator() {
        activityIndicator.isHidden = false
    }
    
    override func hideActivityIndicator() {
        activityIndicator.isHidden = true
    }
    
    func hideFullPhotoScreen() {
        dismissHorizontallyViewController(animated: true)
    }
    
    func show(photoImage: UIImage) {
        photoView.image = photoImage
    }
    
    func zoomPicture(withScale scale: Float) {
        scrollView.setZoomScale(min(scrollView.maximumZoomScale, scrollView.zoomScale * CGFloat(scale)), animated: true)
    }
    
    func changeBarsVisibility() {
        let sourceTopBarHidden = topBarView.isHidden
        let sourceBottomBarHidden = bottomBarView.isHidden
        var sourceTopBarFrame = topBarView.frame
        var destinationTopBarFrame = topBarView.frame
        var sourceBottomBarFrame = bottomBarView.frame
        var destinationBottomBarFrame = bottomBarView.frame
        if (sourceTopBarHidden) {
            sourceTopBarFrame.origin.y -= sourceTopBarFrame.size.height
        } else {
            destinationTopBarFrame.origin.y -= destinationTopBarFrame.size.height
        }
        if (sourceBottomBarHidden) {
            sourceBottomBarFrame.origin.y += sourceBottomBarFrame.size.height
        } else {
            destinationBottomBarFrame.origin.y += destinationBottomBarFrame.size.height
        }
        topBarView.isHidden = false
        topBarView.frame = sourceTopBarFrame;
        bottomBarView.isHidden = false
        bottomBarView.frame = sourceBottomBarFrame;
        hideTopBottomBarTapGestureRecognizer.isEnabled = false
        UIView.animate(withDuration: 0.3, animations: { [weak self] in
            self?.topBarView.frame = destinationTopBarFrame
            self?.bottomBarView.frame = destinationBottomBarFrame
        }) { [weak self] isFinished in
            self?.hideTopBottomBarTapGestureRecognizer.isEnabled = true
            self?.topBarView.isHidden = !sourceTopBarHidden
            self?.topBarView.frame = (sourceTopBarHidden) ? destinationTopBarFrame : sourceTopBarFrame
            self?.bottomBarView.isHidden = !sourceBottomBarHidden
            self?.bottomBarView.frame = (sourceBottomBarHidden) ? destinationBottomBarFrame : sourceBottomBarFrame
        }
    }
    
    // MARK: - UI logic methods
    
    private func setupViews() {
        photoDescriptionView.text = fullPhotoPresenter.getDescriptionText()
        photoCreationDateView.text = fullPhotoPresenter.getCreatedDateFormattedText()
        setupShadowFor(view: topBarView, withOffsetSize: CGSize(width: 0.0, height: 4.0))
        setupShadowFor(view: bottomBarView, withOffsetSize: CGSize(width: 0.0, height: -4.0))
        hideTopBottomBarTapGestureRecognizer.require(toFail: zoomPhotoTapGestureRecognizer)
        scrollView.delegate = self
    }
    
    private func setupShadowFor(view: UIView, withOffsetSize offsetSize: CGSize) {
        view.layer.masksToBounds = false
        view.layer.shadowOffset = offsetSize
        view.layer.shadowRadius = 2.0
        view.layer.shadowOpacity = 0.9
        view.layer.shadowColor = UIColor.black.cgColor
    }
    
    @IBAction func onBackButonClicked(_ sender: UIButton) {
        fullPhotoPresenter.hideFullPhotoScreen()
    }
    
    @IBAction func onChangeBarStateGestureRecognized(_ sender: UITapGestureRecognizer) {
        fullPhotoPresenter.changeBarsVisibility()
    }
    
    @IBAction func onZoomPhotoGestureRecognized(_ sender: UITapGestureRecognizer) {
        fullPhotoPresenter.zoomPicture()
    }
    
    // MARK: - UIScrollViewDelegate
    
    public func viewForZooming(in scrollView: UIScrollView) -> UIView? {
        return photoView
    }
}
