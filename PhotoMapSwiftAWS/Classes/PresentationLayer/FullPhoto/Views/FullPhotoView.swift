//
//  FullPhotoView.swift
//  PhotoMapSwiftAWS
//
//  Created by Roman Andreyenko on 7/17/17.
//  Copyright © 2017 iTechArt. All rights reserved.
//

import UIKit

/**
 View interface(MVP) for FullPhotoViewController
 */
protocol FullPhotoView: BaseLoadingView {
    
    /**
     Hides fullscreen photo screen
     */
    func hideFullPhotoScreen()
    
    /**
     Shows loaded photo picture
     
     - parameters:
        - photoImage: loaded picture image
     */
    func show(photoImage: UIImage)
    
    /**
     Zooms photo picture with provided scale factor
     
     - parameters:
        - withScale: scale factor for zooming
     */
    func zoomPicture(withScale: Float)
    
    /**
     Changes top and bottom bars visibility
     */
    func changeBarsVisibility()
}
