//
//  TimelineComponent.swift
//  PhotoMapSwiftAWS
//
//  Created by Roman Andreyenko on 6/15/17.
//  Copyright © 2017 iTechArt. All rights reserved.
//

import Swinject

/**
 Swinject component for TimelineViewController
 */
class TimelineComponent: BaseComponent {

    init(applicationComponent: ApplicationComponent, timelineAssembly: TimelineAssembly) {
        super.init()
        self.assembler = try! Assembler(assemblies: [timelineAssembly], parentAssembler: applicationComponent.assembler)
    }
}
