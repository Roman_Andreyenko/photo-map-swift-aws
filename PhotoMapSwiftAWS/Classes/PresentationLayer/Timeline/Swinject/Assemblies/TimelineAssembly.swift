//
//  TimelineAssembly.swift
//  PhotoMapSwiftAWS
//
//  Created by Roman Andreyenko on 6/15/17.
//  Copyright © 2017 iTechArt. All rights reserved.
//

import Swinject

/**
 Swinject assembly which provides dependency bindings for TimelineComponent
 */
class TimelineAssembly: Assembly {

    private unowned let view: TimelineView
    
    init(view: TimelineView) {
        self.view = view
    }
    
    func assemble(container: Container) {
        assembleTimelineView(container: container)
        assemleTimelinePresenter(container: container)
    }
    
    private func assembleTimelineView(container: Container) {
        container.register(TimelineView.self) { r in
            return self.view
        }
    }
    
    private func assemleTimelinePresenter(container: Container) {
        container.autoregister(TimelinePresenter.self, initializer: TimelinePresenterImpl.init)
    }
}
