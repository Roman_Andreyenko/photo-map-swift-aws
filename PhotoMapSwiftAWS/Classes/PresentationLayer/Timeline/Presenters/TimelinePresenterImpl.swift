//
//  TimelinePresenterImpl.swift
//  PhotoMapSwiftAWS
//
//  Created by Roman Andreyenko on 6/14/17.
//  Copyright © 2017 iTechArt. All rights reserved.
//

import RxCocoa
import RxSwift
import ReachabilitySwift

/*
 Class implementation of TimelinePresenter
 */
class TimelinePresenterImpl: TimelinePresenter {

    lazy var sections = [TableSectionWrapper]()
    private lazy var photos = [PhotoModel]()
    private var lastSearchText = ""

    //Inject
    private unowned let view: TimelineView
    //Inject
    private let interactor: PhotoInteractor
    //Inject
    private let mapper: PhotoModelDataMapper
    //Inject
    private let sessionManager: SessionManager
    //Inject
    private let notificationCenter: NotificationCenter
    //Inject
    private let dateManager: DateManager
    //Inject
    private let filterManager: PhotoCategoryFilterManager
    
    init(view: TimelineView, interactor: PhotoInteractor, mapper: PhotoModelDataMapper, sessionManager: SessionManager, notificationCenter: NotificationCenter, dateManager: DateManager, filterManager: PhotoCategoryFilterManager) {
        self.view = view
        self.interactor = interactor
        self.mapper = mapper
        self.sessionManager = sessionManager
        self.notificationCenter = notificationCenter
        self.dateManager = dateManager
        self.filterManager = filterManager
        
        notificationCenter.addObserver(self, selector: #selector(TimelinePresenterImpl.categoryFilterChanged(categoriesNotification:)), name: .categoriesFilterChanged, object: nil)
        notificationCenter.addObserver(self, selector: #selector(TimelinePresenterImpl.created(photoNotification:)), name: .photoCreated, object: nil)
    }
    
    func willAppear() {
        setupTimelineAvailability(reachability: AppDelegate.sharedDelegate().reachability)
        notificationCenter.addObserver(self, selector: #selector(TimelinePresenterImpl.reachabilityChanged(note:)), name: ReachabilityChangedNotification, object: AppDelegate.sharedDelegate().reachability)
    }

    func willDisappear() {
        notificationCenter.removeObserver(self, name: ReachabilityChangedNotification, object: AppDelegate.sharedDelegate().reachability)
    }

    func destroy() {
        interactor.onCleanUp()
        
        notificationCenter.removeObserver(self, name: .categoriesFilterChanged, object: nil)
        notificationCenter.removeObserver(self, name: .photoCreated, object: nil)
    }

    func created(photoModel: PhotoModel) {
        fetchPhotos(withSearchText: lastSearchText)
    }
    
    func fetchPhotos(withSearchText searchText: String) {
        guard let user = sessionManager.getCurrentUser() else {
            return
        }
        let photosObserver = AnyObserver<[Photo]> { [unowned self] event in
            switch event {
            case .next(let value):
                self.view.hideActivityIndicator()
                self.fetched(photoModels: self.mapper.transform(photoList: value))
            case .error(let error):
                self.view.hideActivityIndicator()
                self.failedPhotoFetching(error: error)
            case .completed:
                return
            }
        }
        
        lastSearchText = searchText
        view.showActivityIndicator()
        interactor.onCleanUp()
        interactor.initialize()
        interactor.fetchPhotos(forUserId: user.username, withSearchText: searchText, observer: photosObserver)
    }
    
    func fetched(photoModels: [PhotoModel]) {
        photos = photoModels
        calculateSections()
        view.updatePhotoList(withTableSections: sections)
    }

    func failedPhotoFetching(error: Error) {
        view.show(error: error)
    }

    func openCategoryFilterScreen() {
        view.openCategoryFilterScreen()
    }

    func openFullPhotoScreen(forIndexPath indexPath: IndexPath) {
        let photo = getPhoto(forIndexPath: indexPath)
        view.openFullPhotoScreen(for: photo)
    }
    
    func changed(categoriesFilter: Set<PhotoModel.Category>) {
        view.closeCategoryFilterScreen()
        calculateSections()
        view.updatePhotoList(withTableSections: sections)
    }
    
    func getPhoto(forIndexPath indexPath: IndexPath) -> PhotoModel {
        let section = sections[indexPath.section]
        return section.photos[indexPath.row]
    }

    func getSectionHeaderText(forIndexPath indexPath: IndexPath) -> String {
        let photo = getPhoto(forIndexPath: indexPath)
        return dateManager.MMMM_yyyy.string(from: photo.createdTime)
    }

    func getCategoryDescriptionText(for photo: PhotoModel) -> String {
        return String(format: "%@ / %@", dateManager.MM_dd_yyyy.string(from: photo.createdTime), photo.category.rawValue.uppercased())
    }
    
    func downloadPicture(for photoModel: PhotoModel) {
        let pictureObserver = AnyObserver<PhotoPicture> { [unowned self] event in
            switch event {
            case .next(let value):
                self.downloaded(pictureData: self.mapper.transform(photoPicture: value))
            case .error(let error):
                self.failedPictureDownloading(error: error)
            case .completed:
                return
            }
        }
        
        interactor.downloadThumbnailPicture(of: photoModel, observer: pictureObserver)
    }
    
    func downloaded(pictureData: PhotoPictureModel) {
        view.showPhotoListItemPicture(with: pictureData)
    }
    
    func failedPictureDownloading(error: Error) {
    }
    
    func cancelPictureDownloading(for photoModel: PhotoModel) {
        interactor.cancelThumbnailPictureDownloading(of: photoModel)
    }
    
    @objc
    private func created(photoNotification: Notification) {
        created(photoModel: photoNotification.object! as! PhotoModel)
    }
    
    @objc
    private func categoryFilterChanged(categoriesNotification: Notification) {
        changed(categoriesFilter: categoriesNotification.object! as! Set<PhotoModel.Category>)
    }
    
    @objc private func reachabilityChanged(note: Notification) {
        let reachability = note.object as! Reachability
        setupTimelineAvailability(reachability: reachability)
        if reachability.isReachable {
            fetchPhotos(withSearchText: lastSearchText)
        } else {
            view.showSettingsAlert()
        }
    }
    
    private func setupTimelineAvailability(reachability: Reachability) {
        if reachability.isReachable {
            view.enableTimeline()
        } else {
            view.disableTimeline()
        }
    }
    
    private func calculateSections() {
        var currentMonth = 0
        var currentSectionWrapper = TableSectionWrapper()
        sections.removeAll(keepingCapacity: true)
        for i in 0..<photos.count {
            let photo = photos[i]
            if filterManager.isFiltered(category: photo.category) {
                let month = Calendar.current.component(.month, from: photo.createdTime)
                if currentMonth != month {
                    currentMonth = month
                    currentSectionWrapper = TableSectionWrapper()
                    sections.append(currentSectionWrapper)
                }
                currentSectionWrapper.photos.append(photo)
            }
        }
    }
}
