//
//  TimelinePresenter.swift
//  PhotoMapSwiftAWS
//
//  Created by Roman Andreyenko on 6/14/17.
//  Copyright © 2017 iTechArt. All rights reserved.
//

import Foundation

/**
 Presenter interface(MVP) for TimelineViewController
 */
protocol TimelinePresenter: BasePresenter {

    /**
     View should use this property to get list of table sections
     */
    var sections: [TableSectionWrapper] { get }
    
    /**
     View should call this method to open fullscreen photo screen for photo item
     
     - parameters:
        - forIndexPath: index path of photo item in list
     */
    func openFullPhotoScreen(forIndexPath: IndexPath)
    
    /**
     Callback that notifies that new photo is created by user
     
     - parameters:
        - photoModel: new created photo
     */
    func created(photoModel: PhotoModel)
    
    /**
     Callback that notifies that categories filter is changed
     
     - parameters:
        - categoriesFilter: new categories filter
     */
    func changed(categoriesFilter: Set<PhotoModel.Category>)
    
    /**
     View should call this method to fetch user photos using search text
     
     - parameters:
        - withSearchText: search text with photo tags
     */
    func fetchPhotos(withSearchText: String)
    
    /**
     Callback that notifies that all user photos are fetched
     
     - parameters:
        - photoModels: list of user photos
     */
    func fetched(photoModels: [PhotoModel])
    
    /**
     Callback that notifies that photo fetching is failed
     
     - parameters:
        - error: reason of failure
     */
    func failedPhotoFetching(error: Error)
    
    /**
     View should call this method to open category filter screen
     */
    func openCategoryFilterScreen()
    
    /**
     View should call this method to get photo for index path
     
     - parameters:
        - forIndexPath: index path of the table
     
     - returns:
        photo model for provided index path
     */
    func getPhoto(forIndexPath: IndexPath) -> PhotoModel
    
    /**
     View should call this method to get section header text
     
     - paramaters:
        - forIndexPath: index path of the table
     
     - returns:
        header text
     */
    func getSectionHeaderText(forIndexPath: IndexPath) -> String
    
    /**
     View should call this method to get photo category and created date formatted text
     
     - parameters: 
        - for: photo model
     
     - returns:
        formatted text for photo category and created date
     */
    func getCategoryDescriptionText(for: PhotoModel) -> String
    
    /**
     View should call this method to download picture for timeline list
     
     - parameters:
        - for: photo of timeline list
     */
    func downloadPicture(for: PhotoModel)
    
    /**
     Callback that notifies that picture for timeline list is downloaded
     
     - parameters:
        - pictureData: PhotoPictureModel, which represents downloaded picture
     */
    func downloaded(pictureData: PhotoPictureModel)
    
    /**
     Callback that notifies that picture downloading for timeline list is failed
     
     - parameters:
        - error: reason of failure
     */
    func failedPictureDownloading(error: Error)
    
    /**
     View should call this method to cancel picture downloading for timeline list
     
     - parameters:
        - for: photo of timeline list
     */
    func cancelPictureDownloading(for: PhotoModel)
}
