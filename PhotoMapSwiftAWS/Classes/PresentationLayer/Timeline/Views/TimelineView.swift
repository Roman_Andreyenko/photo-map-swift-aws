//
//  TimelineView.swift
//  PhotoMapSwiftAWS
//
//  Created by Roman Andreyenko on 6/14/17.
//  Copyright © 2017 iTechArt. All rights reserved.
//

import UIKit

/**
 View interface(MVP) for TimelineViewController
 */
protocol TimelineView: BaseLoadingView {
    
    /**
     Opens category filter screen
     */
    func openCategoryFilterScreen()
    
    /**
     Opens fullscreen photo screen for photo model
     
     - parameters:
        - for: photo model
     */
    func openFullPhotoScreen(for: PhotoModel)
    
    /**
     Closes category filter screen
     */
    func closeCategoryFilterScreen()
    
    /**
     Updates photo list
     
     - parameters:
        - withTableSections: table sections
     */
    func updatePhotoList(withTableSections: [TableSectionWrapper])
    
    /**
     Shows photo picture for item in timeline list
     
     - parameters:
        - with: PhotoPictureModel that represents picture
     */
    func showPhotoListItemPicture(with: PhotoPictureModel)
    
    /**
     Enabled timeline functionality
     */
    func enableTimeline()
    
    /**
     Disabled timeline functionality
     */
    func disableTimeline()
}
