//
//  TimelineViewController.swift
//  PhotoMapSwiftAWS
//
//  Created by Roman Andreyenko on 6/13/17.
//  Copyright © 2017 iTechArt. All rights reserved.
//

import UIKit
import Swinject
import SwinjectAutoregistration

/**
 Class implementation of view controller for timeline tab
 */
class TimelineViewController: BaseLoadingViewController, UITableViewDataSource, UITableViewDelegate, UISearchBarDelegate, TimelineTableViewCellDelegate, TimelineView {

    private static let TimelineTableContentTopInset: CGFloat = 20.0
    
    private var searchBar: UISearchBar!
    private var rightBarButtonItem: UIBarButtonItem!
    @IBOutlet private weak var timelineTableView: UITableView!
    
    //Inject
    private var timelinePresenter: TimelinePresenter!
    
    // MARK: - init & deinit
    
    deinit {
        timelinePresenter.destroy()
    }
    
    // MARK: - UIViewController lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setUpViews()
        timelinePresenter.fetchPhotos(withSearchText: "")
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        timelinePresenter.willAppear()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        timelinePresenter.willDisappear()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    // MARK: - Swinject
    
    override func setupSwinjectMembers() -> BaseComponent? {
        return TimelineComponent(applicationComponent: AppDelegate.sharedDelegate().getAppComponent(),
                            timelineAssembly: TimelineAssembly(view: self))
    }
    
    override func resolveDependencies(withResolver r: Resolver) {
        timelinePresenter = r ~> TimelinePresenter.self
    }
    
    // MARK: - TimelineView
    
    func openCategoryFilterScreen() {
        let viewController = BaseViewController.newInstance(of: PhotoCategoryFilterViewController.self)
        present(viewController, animated: true)
    }
    
    func openFullPhotoScreen(for photoModel: PhotoModel) {
        let fullPhotoViewController = FullPhotoViewController.newInstance(withPhoto: photoModel, withPicture: nil)
        presentHorizontally(viewController: fullPhotoViewController, animated: true)
    }
    
    func closeCategoryFilterScreen() {
        dismiss(animated: true)
    }

    func updatePhotoList(withTableSections: [TableSectionWrapper]) {
        timelineTableView.reloadData()
    }
    
    func showPhotoListItemPicture(with photoPicture: PhotoPictureModel) {
        if let visibleIndexPaths = timelineTableView.indexPathsForVisibleRows {
            for indexPath in visibleIndexPaths {
                if timelinePresenter.getPhoto(forIndexPath: indexPath).photoId == photoPicture.photoId {
                    let cell = timelineTableView.cellForRow(at: indexPath) as! TimelineTableViewCell
                    cell.update(with: photoPicture)
                }
            }
        }
    }
    
    func enableTimeline() {
        searchBar.isUserInteractionEnabled = true
    }
    
    func disableTimeline() {
        searchBar.isUserInteractionEnabled = false
        searchBar.resignFirstResponder()
    }
    
    // MARK: - UI logic methods
    
    private func setUpViews() {
        setUpSearchBar()
        setUpTableView()
    }
    
    private func setUpSearchBar() {
        searchBar = UISearchBar(frame: CGRect.zero)
        searchBar.delegate = self
        searchBar.sizeToFit()
        searchBar.enablesReturnKeyAutomatically = false
        navigationItem.titleView = searchBar
        rightBarButtonItem = UIBarButtonItem(title: NSLocalizedString("Category", comment: ""), style: .plain, target: self, action: #selector(onCategoryFilterClicked(sender:)))
        navigationItem.rightBarButtonItem = rightBarButtonItem
    }
    
    private func setUpTableView() {
        edgesForExtendedLayout = []
        timelineTableView.dataSource = self
        timelineTableView.delegate = self
        timelineTableView.contentInset = UIEdgeInsets(top: TimelineViewController.TimelineTableContentTopInset, left: 0.0, bottom: 0.0, right: 0.0)
        timelineTableView.backgroundView = nil;
        timelineTableView.backgroundColor = UIColor.white
        timelineTableView.register(UINib(nibName: TimelineTableViewCell.className, bundle: Bundle.main), forCellReuseIdentifier: TimelineTableViewCell.className)
        timelineTableView.register(UINib(nibName: TimelineTableSectionHeaderView.className, bundle: Bundle.main), forHeaderFooterViewReuseIdentifier: TimelineTableSectionHeaderView.className)
    }
    
    @objc
    private func onCategoryFilterClicked(sender: UIBarButtonItem) {
        timelinePresenter.openCategoryFilterScreen()
    }
    
    // MARK: - TimelineTableViewCellDelegate
    
    func cancelPictureDownloading(of photo: PhotoModel) {
        timelinePresenter.cancelPictureDownloading(for: photo)
    }
    
    func downloadPicture(for photo: PhotoModel) {
        timelinePresenter.downloadPicture(for: photo)
    }
    
    // MARK: - UISearchBarDelegate
    
    public func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        timelinePresenter.fetchPhotos(withSearchText: searchText)
    }
    
    public func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        timelinePresenter.fetchPhotos(withSearchText: searchBar.text ?? "")
        searchBar.resignFirstResponder()
    }
    
    // MARK: - UITableViewDataSource
    
    public func numberOfSections(in tableView: UITableView) -> Int {
        return timelinePresenter.sections.count
    }
    
    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return timelinePresenter.sections[section].photos.count
    }
    
    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: TimelineTableViewCell.className) as! TimelineTableViewCell
        let photo = timelinePresenter.getPhoto(forIndexPath: indexPath)
        cell.delegate = self
        cell.bind(photo: photo)
        cell.dateAndCategoryView.text = timelinePresenter.getCategoryDescriptionText(for: photo)
        
        return cell
    }
    
    // MARK: - UITableViewDelegate
    
    public func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if cell.responds(to: #selector(getter: UITableView.separatorInset)) {
            cell.separatorInset = UIEdgeInsets.zero
        }
        if cell.responds(to: #selector(getter: UITableView.preservesSuperviewLayoutMargins)) {
            cell.preservesSuperviewLayoutMargins = false
        }
        if cell.responds(to: #selector(getter: UITableView.layoutMargins)) {
            cell.layoutMargins = UIEdgeInsets.zero
        }
    }
    
    public func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        timelinePresenter.openFullPhotoScreen(forIndexPath: indexPath)
    }
    
    public func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        let cell = tableView.dequeueReusableCell(withIdentifier: TimelineTableViewCell.className) as! TimelineTableViewCell
        
        return cell.frame.size.height
    }
    
    public func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        let view = tableView.dequeueReusableHeaderFooterView(withIdentifier:TimelineTableSectionHeaderView.className)
        return view!.bounds.size.height
    }
    
    public func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let view = tableView.dequeueReusableHeaderFooterView(withIdentifier:TimelineTableSectionHeaderView.className) as! TimelineTableSectionHeaderView
        view.headerView.text = timelinePresenter.getSectionHeaderText(forIndexPath: IndexPath(row: 0, section: section))
        
        return view
    }
}
