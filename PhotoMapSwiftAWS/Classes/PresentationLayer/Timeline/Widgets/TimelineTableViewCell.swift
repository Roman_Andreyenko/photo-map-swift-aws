//
//  TimelineTableViewCell.swift
//  PhotoMapSwiftAWS
//
//  Created by Roman Andreyenko on 6/13/17.
//  Copyright © 2017 iTechArt. All rights reserved.
//

import UIKit

/**
  Custom table view cell for photo
 */
class TimelineTableViewCell: UITableViewCell {

    @IBOutlet private weak var descriptionTextView: UILabel!
    @IBOutlet weak var dateAndCategoryView: UILabel!
    @IBOutlet private weak var photoView: UIImageView!
    @IBOutlet private weak var activityIndicatorView: UIActivityIndicatorView!
    
    weak var delegate: TimelineTableViewCellDelegate?
    private var photo: PhotoModel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func prepareForReuse() {
        super.prepareForReuse()
        
        photoView.image = nil
        activityIndicatorView.isHidden = false
        activityIndicatorView.startAnimating()
        delegate?.cancelPictureDownloading(of: photo)
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    public func bind(photo: PhotoModel) {
        self.photo = photo
        descriptionTextView.text = photo.descriptionText
        if AppDelegate.sharedDelegate().reachability.isReachable {
            delegate?.downloadPicture(for: photo)
        } else {
            photoView.image = UIImage(named: "PlaceholderIcon")!
            activityIndicatorView.isHidden = true
        }
    }
    
    public func update(with picture: PhotoPictureModel) {
        photoView.image = UIImage(data: picture.data)
        activityIndicatorView.isHidden = true
    }
}

/**
 Delegate interface for TimelineTableViewCell
 */
protocol TimelineTableViewCellDelegate: class {
    
    /**
     Cancels picture downloading
     
     - parameters:
        - of: photo
     */
    func cancelPictureDownloading(of: PhotoModel)
    
    /**
     Downloads picture
     
     - parameters:
        - for: photo
     */
    func downloadPicture(for: PhotoModel)
}
