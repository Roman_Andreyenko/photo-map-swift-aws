//
//  TimelineTableSectionHeaderViewll.swift
//  PhotoMapSwiftAWS
//
//  Created by Roman Andreyenko on 6/13/17.
//  Copyright © 2017 iTechArt. All rights reserved.
//

import UIKit

/**
 Custom table section header view for photo's created date
 */
class TimelineTableSectionHeaderView: UITableViewHeaderFooterView {

    @IBOutlet weak var headerView: UILabel!
}
