//
//  TableSectionWrapper.swift
//  PhotoMapSwiftAWS
//
//  Created by Roman Andreyenko on 6/14/17.
//  Copyright © 2017 iTechArt. All rights reserved.
//

import Foundation

/**
 Class representation of a timeline table section
 */
class TableSectionWrapper {
    
    lazy var photos = [PhotoModel]()
}
