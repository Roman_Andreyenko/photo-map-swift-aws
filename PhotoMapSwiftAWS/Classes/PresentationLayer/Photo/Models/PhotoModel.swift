//
//  PhotoModel.swift
//  PhotoMapSwiftAWS
//
//  Created by Roman Andreyenko on 5/29/17.
//  Copyright © 2017 iTechArt. All rights reserved.
//

import Foundation

/**
 Class representation of a photo model
 */
class PhotoModel: NSObject {

    /**
     Enum representation of PhotoModel category
     */
    enum Category: String {
        case `default`
        case friends
        case nature
    }
    
    var photoId: String
    var ownerId: String
    var createdTime: Date
    var category: Category
    var descriptionText: String
    var latitude: Double
    var longitude: Double
    
    init(photoId: String, ownerId: String, createdTime: Date, category: Category, descriptionText: String, latitude: Double, longitude: Double) {
        self.photoId = photoId
        self.ownerId = ownerId
        self.createdTime = createdTime
        self.category = category
        self.descriptionText = descriptionText
        self.latitude = latitude
        self.longitude = longitude
    }
}
