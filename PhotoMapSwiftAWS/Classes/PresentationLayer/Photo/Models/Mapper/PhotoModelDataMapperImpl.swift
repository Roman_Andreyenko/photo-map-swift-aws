//
//  PhotoModelDataMapperImpl.swift
//  PhotoMapSwiftAWS
//
//  Created by Roman Andreyenko on 5/29/17.
//  Copyright © 2017 iTechArt. All rights reserved.
//

import Foundation

/**
 Class implementation of PhotoModelDataMapper
 */
class PhotoModelDataMapperImpl: PhotoModelDataMapper {

    func transform(photo: Photo) -> PhotoModel {
        return PhotoModel(photoId: photo.photoId, ownerId: photo.ownerId, createdTime: photo.createdTime, category: PhotoModel.Category(rawValue: photo.category.rawValue)!, descriptionText: photo.descriptionText, latitude: photo.latitude, longitude: photo.longitude)
    }
    
    func transform(photoModel: PhotoModel) -> Photo {
        return Photo(photoId: photoModel.photoId, ownerId: photoModel.ownerId, createdTime: photoModel.createdTime, category: Photo.Category(rawValue: photoModel.category.rawValue)!, descriptionText: photoModel.descriptionText, latitude: photoModel.latitude, longitude: photoModel.longitude)
    }
    
    func transform(photoPicture: PhotoPicture) -> PhotoPictureModel {
        return PhotoPictureModel(photoId: photoPicture.photoId,
                                 type: PhotoPictureModel.PictureType(rawValue: photoPicture.type.rawValue)!,
                                 data: photoPicture.data)
    }

    func transform(photoPictureModel: PhotoPictureModel) -> PhotoPicture {
        return PhotoPicture(photoId: photoPictureModel.photoId,
                            type: PhotoPicture.PictureType(rawValue: photoPictureModel.type.rawValue)!,
                            data: photoPictureModel.data)
    }
    
    func transform(category: Photo.Category) -> PhotoModel.Category {
        return PhotoModel.Category(rawValue: category.rawValue)!
    }
    
    func transform(categoryModel: PhotoModel.Category) -> Photo.Category {
        return Photo.Category(rawValue: categoryModel.rawValue)!
    }
    
    func transform(photoList: [Photo]) -> [PhotoModel] {
        var photoModelList = [PhotoModel]()
        photoModelList.reserveCapacity(photoList.count)
        for item in photoList {
            photoModelList.append(transform(photo: item))
        }
        
        return photoModelList
    }
    
    func transform(photoModelList: [PhotoModel]) -> [Photo] {
        var photoList = [Photo]()
        photoList.reserveCapacity(photoModelList.count)
        for item in photoModelList {
            photoList.append(transform(photoModel: item))
        }
        
        return photoList
    }
}
