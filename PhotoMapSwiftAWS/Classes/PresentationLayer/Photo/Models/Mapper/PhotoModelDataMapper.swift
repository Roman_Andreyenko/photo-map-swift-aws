//
//  PhotoModelDataMapper.swift
//  PhotoMapSwiftAWS
//
//  Created by Roman Andreyenko on 5/29/17.
//  Copyright © 2017 iTechArt. All rights reserved.
//

import Foundation

/**
 Contract to map Photo related data from presentation layer to domain layer and vice versa.
 */
protocol PhotoModelDataMapper {

    /**
     Converts Photo to a presentation layer PhotoModel
     
     - parameters:
        - photo: the model to convert
     
     - returns:
        the converted PhotoModel
     */
    func transform(photo: Photo) -> PhotoModel
    
    /**
     Converts PhotoModel to a domain layer Photo
     
     - parameters:
        - photoModel: the model to convert
     
     - returns:
        the converted Photo
     */
    func transform(photoModel: PhotoModel) -> Photo
    
    /**
     Converts PhotoPicture to a presentation layer PhotoPictureModel
     
     - parameters:
        - photoPicture: the model to convert
     
     - returns:
        the converted PhotoPictureModel
     */
    func transform(photoPicture: PhotoPicture) -> PhotoPictureModel
    
    /**
     Converts PhotoPictureModel to a domain layer PhotoPicture
     
     - parameters:
        - photoPictureModel: the model to convert
     
     - returns:
        the converted PhotoPicture
     */
    func transform(photoPictureModel: PhotoPictureModel) -> PhotoPicture
    
    /**
     Converts Photo.Category to a presentation layer PhotoModel.Category
     
     - parameters:
        - category: the model to convert
     
     - returns:
        the converted PhotoModel.Category
     */
    func transform(category: Photo.Category) -> PhotoModel.Category
    
    /**
     Converts PhotoModel.Category to a domain layer Photo.Category
     
     - parameters:
        - categoryModel: the model to convert
     
     - returns:
        the converted Photo.Category
     */
    func transform(categoryModel: PhotoModel.Category) -> Photo.Category
    
    /**
     Converts Photo list to a presentation layer PhotoModel list
     
     - parameters:
        - photoList: the model list to convert
     
     - returns:
        the converted PhotoModel list
     */
    func transform(photoList: [Photo]) -> [PhotoModel]
    
    /**
     Converts PhotoModel list to a domain layer Photo list
     
     - parameters:
        - photoModelList: the model list to convert
     
     - returns:
        the converted Photo list
     */
    func transform(photoModelList: [PhotoModel]) -> [Photo]
}
