//
//  PhotoPictureModel.swift
//  PhotoMapSwiftAWS
//
//  Created by Roman Andreyenko on 6/28/17.
//  Copyright © 2017 iTechArt. All rights reserved.
//

import Foundation

/**
 Class representation of a PhotoPictureModel
 */
class PhotoPictureModel: NSObject {
    
    /**
     Enum representation of Picture quality type
     */
    enum PictureType: String {
        case fullQuality
        case thumbnailQuality
    }
    
    let photoId: String
    let type: PictureType
    let data: Data
    
    init(photoId: String, type: PictureType, data: Data) {
        self.photoId = photoId
        self.type = type
        self.data = data
    }
}
