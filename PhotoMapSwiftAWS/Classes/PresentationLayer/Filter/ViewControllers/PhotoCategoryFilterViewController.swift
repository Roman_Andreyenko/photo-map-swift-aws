//
//  PhotoCategoryFilterViewController.swift
//  PhotoMapSwiftAWS
//
//  Created by Roman Andreyenko on 6/12/17.
//  Copyright © 2017 iTechArt. All rights reserved.
//

import Swinject
import SwinjectAutoregistration

/**
 Class implementation of view controller for photo category filter
 */
class PhotoCategoryFilterViewController: BaseViewController {

    @IBOutlet private weak var natureCategoryView: UIButton!
    @IBOutlet private weak var friendsCategoryView: UIButton!
    @IBOutlet private weak var defaultCategoryView: UIButton!
    
    //Inject
    private var categoryFilterPresenter: PhotoCategoryFilterPresenter!
    
    // MARK: - init & deinit
    
    deinit {
        categoryFilterPresenter.destroy()
    }
    
    // MARK: - UIViewController lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setUpViews()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        categoryFilterPresenter.willAppear()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        categoryFilterPresenter.willDisappear()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    // MARK: - Swinject
    
    public override func setupSwinjectMembers() -> BaseComponent? {
        return PhotoCategoryFilterComponent(applicationComponent: AppDelegate.sharedDelegate().getAppComponent(),
                            filterAssembly: PhotoCategoryFilterAssembly())
    }
    
    public override func resolveDependencies(withResolver r: Resolver) {
        categoryFilterPresenter = r ~> PhotoCategoryFilterPresenter.self
    }
    
    // MARK: - UI logic methods
    
    @IBAction func onDoneClicked(_ sender: UIBarButtonItem) {
        categoryFilterPresenter.changePhotoCategoryFilter()
    }
    
    @IBAction func onNatureCategoryClicked(_ sender: UIButton) {
        sender.isSelected = !sender.isSelected
        configure(categoryView: natureCategoryView, isSelected: sender.isSelected, withBorderColor: UIColor.natureCategoryColor)
        categoryFilterPresenter.filterNatureCategory(isSelected: sender.isSelected)
    }
    
    @IBAction func onFriendsCategoryClicked(_ sender: UIButton) {
        sender.isSelected = !sender.isSelected
        configure(categoryView: friendsCategoryView, isSelected: sender.isSelected, withBorderColor: UIColor.friendsCategoryColor)
        categoryFilterPresenter.filterFriendsCategory(isSelected: sender.isSelected)
    }
    
    @IBAction func onDefaultCategoryClicked(_ sender: UIButton) {
        sender.isSelected = !sender.isSelected
        configure(categoryView: defaultCategoryView, isSelected: sender.isSelected, withBorderColor: UIColor.defaultCategoryColor)
        categoryFilterPresenter.filterDefaultCategory(isSelected: sender.isSelected)
    }
    
    private func setUpViews() {
        configure(categoryView: natureCategoryView, isSelected: categoryFilterPresenter.isFilteredNatureCategory, withBorderColor: UIColor.natureCategoryColor)
        configure(categoryView: friendsCategoryView, isSelected: categoryFilterPresenter.isFilteredFriendsCategory, withBorderColor: UIColor.friendsCategoryColor)
        configure(categoryView: defaultCategoryView, isSelected: categoryFilterPresenter.isFilteredDefaultCategory, withBorderColor: UIColor.defaultCategoryColor)
        
    }
    
    private func configure(categoryView: UIButton, isSelected: Bool, withBorderColor borderColor: UIColor) {
        categoryView.isSelected = isSelected
        categoryView.layer.cornerRadius = categoryView.bounds.size.height / 2.0
        categoryView.layer.borderWidth = 1.0
        categoryView.layer.borderColor = borderColor.cgColor
        categoryView.backgroundColor = isSelected ? borderColor : UIColor.white
    }
}
