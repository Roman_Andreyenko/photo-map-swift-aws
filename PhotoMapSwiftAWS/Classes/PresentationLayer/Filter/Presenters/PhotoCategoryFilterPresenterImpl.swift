//
//  PhotoCategoryFilterPresenterImpl.swift
//  PhotoMapSwiftAWS
//
//  Created by Roman Andreyenko on 6/12/17.
//  Copyright © 2017 iTechArt. All rights reserved.
//

import UIKit

/*
 Class implementation of PhotoCategoryFilterPresenter
 */
class PhotoCategoryFilterPresenterImpl: PhotoCategoryFilterPresenter {

    var isFilteredDefaultCategory: Bool
    var isFilteredFriendsCategory: Bool
    var isFilteredNatureCategory: Bool
    
    //Inject
    private let filterManager: PhotoCategoryFilterManager
    
    init(filterManager: PhotoCategoryFilterManager) {
        self.filterManager = filterManager
        isFilteredDefaultCategory = filterManager.isFiltered(category: .default)
        isFilteredFriendsCategory = filterManager.isFiltered(category: .friends)
        isFilteredNatureCategory = filterManager.isFiltered(category: .nature)
    }
    
    func willAppear() {
    }

    func willDisappear() {
    }

    func destroy() {
    }

    func filterDefaultCategory(isSelected: Bool) {
        isFilteredDefaultCategory = isSelected
    }

    func filterFriendsCategory(isSelected: Bool) {
        isFilteredFriendsCategory = isSelected
    }

    func filterNatureCategory(isSelected: Bool) {
        isFilteredNatureCategory = isSelected
    }

    func changePhotoCategoryFilter() {
        var newFilteredCategories = Set<PhotoModel.Category>()
        if isFilteredDefaultCategory {
            newFilteredCategories.insert(.default)
        }
        if isFilteredFriendsCategory {
            newFilteredCategories.insert(.friends)
        }
        if isFilteredNatureCategory {
            newFilteredCategories.insert(.nature)
        }
        filterManager.change(filteredCategories: newFilteredCategories)
    }
}
