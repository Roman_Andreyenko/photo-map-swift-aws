//
//  PhotoCategoryFilterPresenter.swift
//  PhotoMapSwiftAWS
//
//  Created by Roman Andreyenko on 6/12/17.
//  Copyright © 2017 iTechArt. All rights reserved.
//

import Foundation

/**
 Presenter interface(MVP) for PhotoCategoryFilterViewController
 */
protocol PhotoCategoryFilterPresenter: BasePresenter {
    
    /**
     View should use this property to determine if DEFAULT photo category is filtered
     */
    var isFilteredDefaultCategory: Bool { get }
    
    /**
     View should use this property to determine if FRIENDS photo category is filtered
     */
    var isFilteredFriendsCategory: Bool { get }
    
    /**
     View should use this property to determine if NATURE photo category is filtered
     */
    var isFilteredNatureCategory: Bool { get }
    
    /**
     View should call this method to turn on/off DEFAULT photo category
     
     - parameters:
        - isSelected: true, if DEFAULT category is selected, otherwise - false
     */
    func filterDefaultCategory(isSelected: Bool)
    
    /**
     View should call this method to turn on/off FRIENDS photo category
     
     - parameters:
        - isSelected: true, if FRIENDS category is selected, otherwise - false
     */
    func filterFriendsCategory(isSelected: Bool)
    
    /**
     View should call this method to turn on/off NATURE photo category
     
     - parameters:
        - isSelected: true, if NATURE category is selected, otherwise - false
     */
    func filterNatureCategory(isSelected: Bool)
    
    /**
     View should call this method to change photo category filter settings
     */
    func changePhotoCategoryFilter()
}
