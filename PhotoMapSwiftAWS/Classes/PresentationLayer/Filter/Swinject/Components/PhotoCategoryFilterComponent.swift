//
//  PhotoCategoryFilterComponent.swift
//  PhotoMapSwiftAWS
//
//  Created by Roman Andreyenko on 6/12/17.
//  Copyright © 2017 iTechArt. All rights reserved.
//

import Swinject

/**
 Swinject component for PhotoCategoryFilterViewController
 */
class PhotoCategoryFilterComponent: BaseComponent {

    init(applicationComponent: ApplicationComponent, filterAssembly: PhotoCategoryFilterAssembly) {
        super.init()
        self.assembler = try! Assembler(assemblies: [filterAssembly], parentAssembler: applicationComponent.assembler)
    }
}
