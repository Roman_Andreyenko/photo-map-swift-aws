//
//  PhotoCategoryFilterAssembly.swift
//  PhotoMapSwiftAWS
//
//  Created by Roman Andreyenko on 6/12/17.
//  Copyright © 2017 iTechArt. All rights reserved.
//

import Swinject

/**
 Swinject assembly which provides dependency bindings for PhotoCategoryFilterComponent
 */
class PhotoCategoryFilterAssembly: Assembly {
    
    func assemble(container: Container) {
        assemlePhotoCategoryFilterPresenter(container: container)
    }
    
    private func assemlePhotoCategoryFilterPresenter(container: Container) {
        container.autoregister(PhotoCategoryFilterPresenter.self, initializer: PhotoCategoryFilterPresenterImpl.init)
    }
}
