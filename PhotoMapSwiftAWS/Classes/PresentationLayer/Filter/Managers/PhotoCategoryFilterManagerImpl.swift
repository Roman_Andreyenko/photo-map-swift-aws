//
//  PhotoCategoryFilterManagerImpl.swift
//  PhotoMapSwiftAWS
//
//  Created by Roman Andreyenko on 6/12/17.
//  Copyright © 2017 iTechArt. All rights reserved.
//

import Foundation

/**
 Class implementation of PhotoCategoryFilterManager
 */
class PhotoCategoryFilterManagerImpl: PhotoCategoryFilterManager {

    //Inject
    private let notificationCenter: NotificationCenter
    
    init(notificationCenter: NotificationCenter) {
        self.notificationCenter = notificationCenter
    }
    
    private lazy var filteredCategories = Set<PhotoModel.Category>(arrayLiteral: .default, .friends, .nature)
    
    func isFiltered(category: PhotoModel.Category) -> Bool {
        return filteredCategories.contains(category)
    }
    
    func change(filteredCategories newCategories: Set<PhotoModel.Category>) {
        filteredCategories = newCategories
        notificationCenter.post(name: .categoriesFilterChanged, object: filteredCategories)
    }
}
