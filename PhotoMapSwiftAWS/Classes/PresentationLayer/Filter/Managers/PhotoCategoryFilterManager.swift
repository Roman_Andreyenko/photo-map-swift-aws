//
//  PhotoCategoryFilterManager.swift
//  PhotoMapSwiftAWS
//
//  Created by Roman Andreyenko on 6/12/17.
//  Copyright © 2017 iTechArt. All rights reserved.
//

import Foundation

/**
 Interface of manager which stores settings for filtered photo categories
 */
protocol PhotoCategoryFilterManager {
    
    /**
     Checks if provided photo category is filtered
     
     - parameters:
        - category: photo category
     */
    func isFiltered(category: PhotoModel.Category) -> Bool
    
    /**
     Changes filtered photo categories and sends notification
     
     - parameters:
        - filteredCategories: new filtered categories
     */
    func change(filteredCategories: Set<PhotoModel.Category>)
}
