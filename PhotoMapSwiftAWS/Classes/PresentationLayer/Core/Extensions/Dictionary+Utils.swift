//
//  Dictionary+Utils.swift
//  PhotoMapSwiftAWS
//
//  Created by Roman Andreyenko on 5/19/17.
//  Copyright © 2017 iTechArt. All rights reserved.
//

import Foundation

extension Dictionary {
    
    static func + (left : Dictionary, right: Dictionary) -> Dictionary {
        var result = Dictionary(minimumCapacity: left.count + right.count)
        result += left
        result += right
        return result
    }
    
    static func += (left :inout Dictionary, right: Dictionary) {
        for (k, v) in right {
            left[k] = v
        } 
    }
}
