//
//  UIButton+UI.swift
//  PhotoMapSwiftAWS
//
//  Created by Roman Andreyenko on 5/30/17.
//  Copyright © 2017 iTechArt. All rights reserved.
//

import UIKit

extension UIButton {
    
    @IBInspectable var localizedTitleForNormal: String {
        set (key) {
            setTitle(NSLocalizedString(key, comment: ""), for: .normal)
        }
        get {
            return title(for: .normal)!
        }
    }
}
