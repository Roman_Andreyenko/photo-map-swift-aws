//
//  NSObject+ClassName.swift
//  PhotoMapSwiftAWS
//
//  Created by Roman Andreyenko on 4/20/17.
//  Copyright © 2017 iTechArt. All rights reserved.
//

import Foundation

extension NSObject {
    
    var className: String {
        return String(describing: type(of: self))
    }
    
    class var className: String {
        return String(describing: self)
    }
}
