//
//  UILabel+UI.swift
//  PhotoMapSwiftAWS
//
//  Created by Roman Andreyenko on 6/5/17.
//  Copyright © 2017 iTechArt. All rights reserved.
//

import UIKit

extension UILabel {
    
    @IBInspectable var localizedText: String {
        set (key) {
            text = NSLocalizedString(key, comment: "")
        }
        get {
            return text!
        }
    }
}
