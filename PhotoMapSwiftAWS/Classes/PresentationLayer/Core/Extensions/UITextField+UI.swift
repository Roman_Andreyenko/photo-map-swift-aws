//
//  UITextField+UI.swift
//  PhotoMapSwiftAWS
//
//  Created by Roman Andreyenko on 6/5/17.
//  Copyright © 2017 iTechArt. All rights reserved.
//

import UIKit

extension UITextField {
    
    @IBInspectable var localizedPlaceHolder: String {
        set (key) {
            placeholder = NSLocalizedString(key, comment: "")
        }
        get {
            return placeholder!
        }
    }
}
