//
//  Notification_Name+Custom.swift
//  PhotoMapSwiftAWS
//
//  Created by Roman Andreyenko on 5/30/17.
//  Copyright © 2017 iTechArt. All rights reserved.
//

import Foundation

extension Notification.Name {
    public static let photoCreated = NSNotification.Name("PhotoCreatedNotification")
    public static let photoCancelled = NSNotification.Name("PhotoCancelledNotification")
    public static let categoriesFilterChanged = NSNotification.Name("CategoriesFilterChangedNotification")
}
