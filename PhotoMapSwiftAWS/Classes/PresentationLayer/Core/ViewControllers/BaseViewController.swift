//
//  BaseViewController.swift
//  PhotoMapSwiftAWS
//
//  Created by Roman Andreyenko on 4/26/17.
//  Copyright © 2017 iTechArt. All rights reserved.
//

import UIKit
import Swinject

/**
 Base implementation of view controller
 */
class BaseViewController: UIViewController {

    private var component: BaseComponent!
    
    // MARK: - init & deinit
    
    public static func newInstance<T: UIViewController>(of viewControllerType: T.Type) -> T {
        let storyboard = UIStoryboard(name: viewControllerType.className, bundle: nil)
        return storyboard.instantiateInitialViewController() as! T
    }
    
    override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?) {
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
        
        setupSwinjectDependencies()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        setupSwinjectDependencies()
    }

    // MARK: - UIViewController lifecycle
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

    // MARK: - Swinject
    
    public func setupSwinjectMembers() -> BaseComponent? {
        fatalError("This method should be overriden in child classes")
    }
    
    public func resolveDependencies(withResolver r: Resolver) {
        fatalError("This method should be overriden in child classes")
    }
    
    private func setupSwinjectDependencies() {
        component = setupSwinjectMembers()
        if component != nil {
            resolveDependencies(withResolver: component.getResolver())
        }
    }
    
    // MARK: - UI logic methods
    
    public func presentAlert(withMessage message: String) {
        self.presentAlert(withTitle: NSLocalizedString("Warning", comment: ""), andMessage: message)
    }
    
    public func presentAlert(withTitle title: String, andMessage message: String) {
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let defaultAction = UIAlertAction(title: NSLocalizedString("OK", comment:""), style: .default)
        alertController.addAction(defaultAction)
        self.present(alertController, animated: true)
    }
    
    public func presentSettingsAlert(withMessage message: String) {
        let alertController = UIAlertController(title: NSLocalizedString("Warning", comment: ""), message:message, preferredStyle: .alert)
        let defaultAction = UIAlertAction(title: NSLocalizedString("OK", comment:""), style: .default)
        alertController.addAction(defaultAction)
        let settingsAction = UIAlertAction(title: NSLocalizedString("Settings", comment:""), style: .default, handler: { action in
            UIApplication.shared.open(URL(string: UIApplicationOpenSettingsURLString)!, options: [:])
        })
        alertController.addAction(settingsAction)
        self.present(alertController, animated: true)
    }
    
    public func presentHorizontally(viewController: UIViewController, animated: Bool, completion: (() -> Swift.Void)? = nil) {
        let transition = CATransition()
        transition.duration = 0.5
        transition.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseInEaseOut)
        transition.type = kCATransitionPush
        transition.subtype = kCATransitionFromRight
        self.view.window?.layer.add(transition, forKey: nil)
        self.present(viewController, animated: animated, completion: completion)
    }
    
    public func dismissHorizontallyViewController(animated: Bool, completion: (() -> Swift.Void)? = nil) {
        let transition = CATransition()
        transition.duration = 0.5
        transition.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseInEaseOut)
        transition.type = kCATransitionPush
        transition.subtype = kCATransitionFromLeft
        self.view.window?.layer.add(transition, forKey: nil)
        self.dismiss(animated: animated, completion: completion)
    }
}
