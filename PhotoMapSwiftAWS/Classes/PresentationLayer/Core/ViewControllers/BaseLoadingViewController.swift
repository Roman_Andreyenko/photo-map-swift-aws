//
//  BaseLoadingViewController.swift
//  PhotoMapSwiftAWS
//
//  Created by Roman Andreyenko on 7/20/17.
//  Copyright © 2017 iTechArt. All rights reserved.
//

import UIKit

class BaseLoadingViewController: BaseViewController, BaseLoadingView {

    @IBOutlet private weak var activityIndicatorView: UIActivityIndicatorView!
    
    var activityIndicator: UIActivityIndicatorView! {
        return activityIndicatorView
    }
    
    func showActivityIndicator() {
        view.isUserInteractionEnabled = false
        activityIndicatorView.isHidden = false
    }
    
    func hideActivityIndicator() {
        view.isUserInteractionEnabled = true
        activityIndicatorView.isHidden = true
    }
    
    func show(error: Error) {
        presentAlert(withMessage: error.localizedDescription)
    }
    
    func showSettingsAlert() {
        if presentedViewController == nil {
            presentSettingsAlert(withMessage: NSLocalizedString("NoInternetConnection", comment: ""))
        }
    }
}
