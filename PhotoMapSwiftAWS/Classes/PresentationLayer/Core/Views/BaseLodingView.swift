//
//  BaseLodingView.swift
//  PhotoMapSwiftAWS
//
//  Created by Roman Andreyenko on 5/2/17.
//  Copyright © 2017 iTechArt. All rights reserved.
//

import Foundation

/**
 Base Load View (MVP) interface that represents a View that will be used to load data
 */
protocol BaseLoadingView: class {
    
    /**
     Shows activity indicator
     */
    func showActivityIndicator()
    
    /**
     Hides activity indicator
     */
    func hideActivityIndicator()
    
    /**
     Shows error message
     
     - parameters:
        - error: reason of failure
     */
    func show(error: Error)
    
    /**
     Shows settings alert
     */
    func showSettingsAlert()
}
