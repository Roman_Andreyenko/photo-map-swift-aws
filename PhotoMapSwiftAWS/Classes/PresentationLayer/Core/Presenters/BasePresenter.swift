//
//  BasePresenter.swift
//  PhotoMapSwiftAWS
//
//  Created by Roman Andreyenko on 5/2/17.
//  Copyright © 2017 iTechArt. All rights reserved.
//

import Foundation

/**
 Base Presenter used for MVP Pattern
 */
protocol BasePresenter {
    
    /**
     Method that controls lifecycle of the view.
     It should be called in the view's (ViewController) viewWillAppear(_:) method.
     */
    func willAppear()
    
    /**
     Method that controls lifecycle of the view.
     It should be called in the view's (ViewController) viewWillDisappear(_:) method.
     */
    func willDisappear()
    
    /**
     Method that controls lifecycle of the view. 
     It should be called in the view's (ViewController) deinit() method.
     */
    func destroy()
}
