//
//  ApplicationAssembly.swift
//  PhotoMapSwiftAWS
//
//  Created by Roman Andreyenko on 4/26/17.
//  Copyright © 2017 iTechArt. All rights reserved.
//

import Swinject

/**
 Swinject assembly which provides dependency bindings for ApplicationComponent
 */
class ApplicationAssembly: Assembly {

    private unowned var appDelegate: AppDelegate
    
    init(appDelegate: AppDelegate) {
        self.appDelegate = appDelegate
    }
    
    func assemble(container: Container) {
        assembleAppDelegate(container: container)
        assembleSessionManager(container: container)
        assembleNotificationCenter(container: container)
        assembleDateManager(container: container)
        assemblePhotoCategoryFilterManager(container: container)
    }
    
    private func assembleAppDelegate(container: Container) {
        container.register(AppDelegate.self) { r in
            return self.appDelegate
            }
            .inObjectScope(.container)
    }
    
    private func assembleSessionManager(container: Container) {
        container.autoregister(SessionManager.self, initializer: SessionManagerImpl.init)
            .inObjectScope(.container)
    }
    
    private func assembleNotificationCenter(container: Container) {
        container.register(NotificationCenter.self) { r in
            return NotificationCenter.default
            }
            .inObjectScope(.container)
    }
    
    private func assembleDateManager(container: Container) {
        container.register(DateManager.self) { r in
            return DateManagerImpl()
            }
            .inObjectScope(.container)
    }
    
    private func assemblePhotoCategoryFilterManager(container: Container) {
        container.autoregister(PhotoCategoryFilterManager.self, initializer: PhotoCategoryFilterManagerImpl.init)
            .inObjectScope(.container)
    }
}
