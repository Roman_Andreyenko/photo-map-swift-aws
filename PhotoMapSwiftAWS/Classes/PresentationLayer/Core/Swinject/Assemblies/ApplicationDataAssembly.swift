//
//  ApplicationDataAssembly.swift
//  PhotoMapSwiftAWS
//
//  Created by Roman Andreyenko on 5/2/17.
//  Copyright © 2017 iTechArt. All rights reserved.
//

import Swinject

/**
 Swinject assembly which provides dependency bindings for ApplicationComponent
 */
class ApplicationDataAssembly: Assembly {

    func assemble(container: Container) {
        assembleSessionModelDataMapper(container: container)
        assemblePhotoModelDataMapper(container: container)
    }
    
    private func assembleSessionModelDataMapper(container: Container) {
        container.register(SessionModelDataMapper.self) { r in
            return SessionModelDataMapperImpl()
        }
    }
    
    private func assemblePhotoModelDataMapper(container: Container) {
        container.register(PhotoModelDataMapper.self) { r in
            return PhotoModelDataMapperImpl()
        }
    }
}
