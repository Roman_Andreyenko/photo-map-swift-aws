//
//  InteractorsAssembly.swift
//  PhotoMapSwiftAWS
//
//  Created by Roman Andreyenko on 5/2/17.
//  Copyright © 2017 iTechArt. All rights reserved.
//

import Swinject
import RxCocoa
import RxSwift

/**
 Swinject assembly which provides dependency bindings for ApplicationComponent
 */
class InteractorsAssembly: Assembly {

    func assemble(container: Container) {
        assembleSubscriptionScheduler(container: container)
        assembleObservationScheduler(container: container)
        assembleSessionInteractor(container: container)
        assemblePhotoInteractor(container: container)
    }
 
    private func assembleSubscriptionScheduler(container: Container) {
        container.register(SchedulerType.self) { r in
            return ConcurrentDispatchQueueScheduler(qos: .background)
            }
            .inObjectScope(.container)
    }
    
    private func assembleObservationScheduler(container: Container) {
        container.register(SerialDispatchQueueScheduler.self) { r in
            return MainScheduler.instance
            }
            .inObjectScope(.container)
    }
    
    private func assembleSessionInteractor(container: Container) {
        container.autoregister(SessionInteractor.self, initializer: SessionInteractorImpl.init)
    }

    private func assemblePhotoInteractor(container: Container) {
        container.autoregister(PhotoInteractor.self, initializer: PhotoInteractorImpl.init)
    }
}
