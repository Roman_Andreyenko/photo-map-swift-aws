//
//  BaseComponent.swift
//  PhotoMapSwiftAWS
//
//  Created by Roman Andreyenko on 4/26/17.
//  Copyright © 2017 iTechArt. All rights reserved.
//

import Swinject

/**
 Swinject base implementation of component
 */
class BaseComponent {

    var assembler: Assembler! = nil
    
    public func getResolver() -> Resolver {
        return assembler.resolver
    }
}
