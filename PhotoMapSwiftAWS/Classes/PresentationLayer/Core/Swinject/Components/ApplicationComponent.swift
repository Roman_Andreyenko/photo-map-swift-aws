//
//  ApplicationComponent.swift
//  PhotoMapSwiftAWS
//
//  Created by Roman Andreyenko on 4/26/17.
//  Copyright © 2017 iTechArt. All rights reserved.
//

import Swinject

/**
 Swinject component for AppDelegate
 */
class ApplicationComponent: BaseComponent {
    
    init(applicationAssembly: ApplicationAssembly, applicationDataAssembly: ApplicationDataAssembly, interactorsAssembly: InteractorsAssembly, sessionDataAssembly: SessionDataAssembly, photoDataAssembly: PhotoDataAssembly, cognitoAssembly: AWSCognitoAssembly, dynamoDBAssembly: AWSDynamoDBAssembly, s3Assembly: AWSS3Assembly) {
        super.init()
        self.assembler = try! Assembler(assemblies: [applicationAssembly, applicationDataAssembly, interactorsAssembly, sessionDataAssembly, photoDataAssembly, cognitoAssembly, dynamoDBAssembly, s3Assembly])
    }
}
