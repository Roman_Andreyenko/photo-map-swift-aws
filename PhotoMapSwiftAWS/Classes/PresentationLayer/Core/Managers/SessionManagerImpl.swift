//
//  SessionManagerImpl.swift
//  PhotoMapSwiftAWS
//
//  Created by Roman Andreyenko on 5/4/17.
//  Copyright © 2017 iTechArt. All rights reserved.
//

import RxCocoa
import RxSwift

/**
 Class implementation of SessionManager
 */
class SessionManagerImpl: SessionManager {

    //Inject
    private var sessionInteractor: SessionInteractor
    //Inject
    private var mapper: SessionModelDataMapper
    private weak var callback: SessionCallback?
    private var user: UserModel?
    
    init(sessionInteractor: SessionInteractor, mapper: SessionModelDataMapper) {
        self.sessionInteractor = sessionInteractor
        self.mapper = mapper
    }
    
    func getCurrentUser() -> UserModel? {
        return user
    }
    
    func getSession(with callback: SessionCallback) {
        self.callback = callback
        let sessionObserver = AnyObserver<User?> { [unowned self] event in
            switch event {
            case .next(let user):
                if let user = user {
                    self.user = self.mapper.transform(user: user)
                    self.callback?.isUserAuthorized()
                } else {
                    self.user = nil
                    self.callback?.isUserAnonymous()
                }
            case .error, .completed:
                return
            }
        }
        sessionInteractor.getCurrentUser(observer: sessionObserver)
    }
    
    func onCleanUp() {
        sessionInteractor.onCleanUp()
    }
}
