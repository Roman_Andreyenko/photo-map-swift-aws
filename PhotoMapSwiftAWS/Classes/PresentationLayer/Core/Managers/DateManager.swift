//
//  DateManager.swift
//  PhotoMapSwiftAWS
//
//  Created by Roman Andreyenko on 5/31/17.
//  Copyright © 2017 iTechArt. All rights reserved.
//

import Foundation

/**
 Interface which provides date formatters
 */
protocol DateManager {
    
    /**
     Property of date formatter with format "MM_dd_yyyy"
     
     - returns:
        date formatter
     */
    var MM_dd_yyyy: DateFormatter! { get }
    
    /**
     Property of date formatter with format "MMMM_d_suffix_comma_yyyy_hyphen_h_mm_a"
     
     - returns:
        date formatter
     */
    var MMMM_d_suffix_comma_yyyy_hyphen_h_mm_a: DateFormatter! { get }
    
    /**
     Property of date formatter with format "MMMM_d_suffix_comma_yyyy_at_h_mm_a"
     
     - returns:
        date formatter
     */
    var MMMM_d_suffix_comma_yyyy_at_h_mm_a: DateFormatter! { get }
    
    /**
     Property of date formatter with format "MMMM_yyyy"
     
     - returns:
        date formatter
     */
    var MMMM_yyyy: DateFormatter! { get }
    
    /**
     Gets text suffix for ordinal number of day.
     
     - parameters:
        - for: source date
     
     - returns:
        text suffix for ordinal number of day
     */
    static func daySuffix(for: Date) -> String
}
