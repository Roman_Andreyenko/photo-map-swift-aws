//
//  SessionCallback.swift
//  PhotoMapSwiftAWS
//
//  Created by Roman Andreyenko on 5/4/17.
//  Copyright © 2017 iTechArt. All rights reserved.
//

import Foundation

/**
 Callback interface which notifies client about session changes
 */
protocol SessionCallback: class {
    
    /**
     Callback denotes that current user is authorized
     */
    func isUserAuthorized()
    
    /**
     Callback denotes that current user is anonymous
     */
    func isUserAnonymous()
}
