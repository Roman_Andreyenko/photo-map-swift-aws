//
//  DateManagerImpl.swift
//  PhotoMapSwiftAWS
//
//  Created by Roman Andreyenko on 5/31/17.
//  Copyright © 2017 iTechArt. All rights reserved.
//

import Foundation

/**
 Class implementation of DateManager
 */
class DateManagerImpl: DateManager {

    var MM_dd_yyyy: DateFormatter!
    var MMMM_d_suffix_comma_yyyy_hyphen_h_mm_a: DateFormatter!
    var MMMM_d_suffix_comma_yyyy_at_h_mm_a: DateFormatter!
    var MMMM_yyyy: DateFormatter!
    
    public static func daySuffix(for date: Date) -> String {
        let dayOfMonth = Calendar.current.component(.day, from: date)
        switch (dayOfMonth) {
        case 1, 21, 31:
            return "st"
        case 2, 22:
            return "nd"
        case 3, 23:
            return "rd"
        default:
            return "th"
        }
    }
    
    init() {
        self.initializeMM_dd_yyyyDateFormatter()
        self.initializeMMMM_d_suffix_comma_yyyy_hyphen_h_mm_aDateFormatter()
        self.initializeMMMM_d_suffix_comma_yyyy_at_h_mm_aDateFormatter()
        self.initializeMMMM_yyyyDateFormatter()
    }
    
    private func initializeMM_dd_yyyyDateFormatter() {
        self.MM_dd_yyyy = DateFormatter()
        self.MM_dd_yyyy.dateFormat = "MM-dd-yyyy"
    }
    
    private func initializeMMMM_d_suffix_comma_yyyy_hyphen_h_mm_aDateFormatter() {
        self.MMMM_d_suffix_comma_yyyy_hyphen_h_mm_a = DateFormatter()
        self.MMMM_d_suffix_comma_yyyy_hyphen_h_mm_a.dateFormat = "MMMM d'%@', yyyy - h:mm a"
        self.MMMM_d_suffix_comma_yyyy_hyphen_h_mm_a.amSymbol = "am"
        self.MMMM_d_suffix_comma_yyyy_hyphen_h_mm_a.pmSymbol = "pm"
    }
    
    private func initializeMMMM_d_suffix_comma_yyyy_at_h_mm_aDateFormatter() {
        self.MMMM_d_suffix_comma_yyyy_at_h_mm_a = DateFormatter()
        self.MMMM_d_suffix_comma_yyyy_at_h_mm_a.dateFormat = "MMMM d'%@', yyyy 'at' h:mm a"
        self.MMMM_d_suffix_comma_yyyy_at_h_mm_a.amSymbol = "am"
        self.MMMM_d_suffix_comma_yyyy_at_h_mm_a.pmSymbol = "pm"
    }
    
    private func initializeMMMM_yyyyDateFormatter() {
        self.MMMM_yyyy = DateFormatter()
        self.MMMM_yyyy.dateFormat = "MMMM yyyy"
    }
    
}
