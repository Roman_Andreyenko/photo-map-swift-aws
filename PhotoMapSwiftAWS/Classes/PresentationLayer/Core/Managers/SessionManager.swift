//
//  SessionManager.swift
//  PhotoMapSwiftAWS
//
//  Created by Roman Andreyenko on 5/4/17.
//  Copyright © 2017 iTechArt. All rights reserved.
//

import Foundation

/**
 Interface of manager which coordinates user session
 */
protocol SessionManager {
    
    /**
     Checks current user's session
     
     - parameters:
     
        - callback: session callback
     */
    func getSession(with callback: SessionCallback)
    
    /**
     Cleans manager's staff
     */
    func onCleanUp()
    
    /**
     Gets current user
     
     - returns:
        current user model if user was logged in, otherwise - nil
     */
    func getCurrentUser() -> UserModel?
}
