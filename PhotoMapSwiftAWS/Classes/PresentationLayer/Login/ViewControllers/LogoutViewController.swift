//
//  LogoutViewController.swift
//  PhotoMapSwiftAWS
//
//  Created by Roman Andreyenko on 5/4/17.
//  Copyright © 2017 iTechArt. All rights reserved.
//

import UIKit
import Swinject
import SwinjectAutoregistration

/**
 Class implementation of view controller log out user from the app
 */
class LogoutViewController: BaseLoadingViewController, LogoutView {

    @IBOutlet private weak var logOutView: UIButton!
    
    //Inject
    private var logoutPresenter: LogoutPresenter!
    
    // MARK: - init & deinit
    
    deinit {
        logoutPresenter.destroy()
    }
    
    // MARK: - UIViewController lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        logoutPresenter.willAppear()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        logoutPresenter.willDisappear()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - Swinject
    
    override func setupSwinjectMembers() -> BaseComponent? {
        return LogoutComponent(applicationComponent: AppDelegate.sharedDelegate().getAppComponent(), logoutAssembly: LogoutAssembly(view: self))
    }
    
    override func resolveDependencies(withResolver r: Resolver) {
        logoutPresenter = r ~> LogoutPresenter.self
    }

    // MARK: - LogoutView
    
    func openWelcomeScreen() {
        AppDelegate.sharedDelegate().setupRootViewController(animated: true)
    }
    
    func enableLogout() {
        logOutView.isEnabled = true
    }
    
    func disableLogout() {
        logOutView.isEnabled = false
    }
    
    // MARK: - UI logic methods
    
    @IBAction func onLogoutClicked(_ sender: UIButton) {
        logoutPresenter.logout()
    }
}
