//
//  LoginViewController.swift
//  PhotoMapSwiftAWS
//
//  Created by Roman Andreyenko on 4/28/17.
//  Copyright © 2017 iTechArt. All rights reserved.
//

import UIKit
import Swinject
import SwinjectAutoregistration

/**
 Class implementation of view controller to login user
 */
class LoginViewController: BaseLoadingViewController, LoginView, UITextFieldDelegate {

    @IBOutlet private weak var logInView: UITextField!
    @IBOutlet private weak var passwordView: UITextField!
    @IBOutlet private weak var logInValidationView: UILabel!
    @IBOutlet private weak var passwordValidationView: UILabel!
    @IBOutlet private weak var logInActionView: UIButton!
    
    //Inject
    private var loginPresenter: LoginPresenter!
    
    // MARK: - init & deinit
    
    deinit {
        loginPresenter.destroy()
    }
    
    // MARK: - UIViewController lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()

        setUpViews()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        loginPresenter.willAppear()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        loginPresenter.willDisappear()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    // MARK: - Swinject
    
    override func setupSwinjectMembers() -> BaseComponent? {
        return LoginComponent(applicationComponent: AppDelegate.sharedDelegate().getAppComponent(), loginAssembly: LoginAssembly(view: self))
    }
    
    override func resolveDependencies(withResolver r: Resolver) {
        loginPresenter = r ~> LoginPresenter.self
    }
    
    // MARK: - LoginView
    
    func openMainScreen() {
        AppDelegate.sharedDelegate().setupRootViewController(animated: true)
    }
    
    func showUsername(validationMessage message: String?) {
        show(validationMessage: message, forView: logInValidationView)
    }

    func showPassword(validationMessage message: String?) {
        show(validationMessage: message, forView: passwordValidationView)
    }
    
    func enableLogin() {
        logInActionView.isEnabled = true
    }
    
    func disableLogin() {
        logInActionView.isEnabled = false
    }
    
    // MARK: - UI logic methods
    
    private func setUpViews() {
        logInView.delegate = self
        passwordView.delegate = self
        disableLogin()
    }
    
    @IBAction func logInClicked(_ sender: UIButton) {
        loginPresenter.login()
    }
    
    private func show(validationMessage message: String?, forView view: UILabel) {
        view.text = message
        view.isHidden = message == nil
    }
    
    // MARK: - UITextFieldDelegate
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let text = textField.text as NSString?
        if let text = text {
            let replacedText = text.replacingCharacters(in: range, with: string)
            if textField == logInView {
                loginPresenter.validate(username: replacedText)
            } else if textField == passwordView {
                loginPresenter.validate(password: replacedText)
            }
        }
        
        return true
    }
}
