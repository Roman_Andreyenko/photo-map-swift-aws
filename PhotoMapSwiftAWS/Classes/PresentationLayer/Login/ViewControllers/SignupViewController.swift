//
//  SignupViewController.swift
//  PhotoMapSwiftAWS
//
//  Created by Roman Andreyenko on 5/3/17.
//  Copyright © 2017 iTechArt. All rights reserved.
//

import UIKit
import Swinject
import SwinjectAutoregistration

/**
 Class implementation of view controller to sign up new user
 */
class SignupViewController: BaseLoadingViewController, SignupView, UITextFieldDelegate {

    @IBOutlet private weak var usernameView: UITextField!
    @IBOutlet private weak var passwordView: UITextField!
    @IBOutlet private weak var passwordConfirmationView: UITextField!
    @IBOutlet private weak var usernameValidationView: UILabel!
    @IBOutlet private weak var passwordValidationView: UILabel!
    @IBOutlet private weak var passwordConfirmationValidationView: UILabel!
    @IBOutlet private weak var signUpView: UIButton!
    
    //Inject
    private var signupPresenter: SignupPresenter!
    
    // MARK: - init & deinit
    
    deinit {
        signupPresenter.destroy()
    }
    
    // MARK: - UIViewController lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()

        setUpViews()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        signupPresenter.willAppear()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        signupPresenter.willDisappear()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    // MARK: - Swinject
    
    override func setupSwinjectMembers() -> BaseComponent? {
        return SignupComponent(applicationComponent: AppDelegate.sharedDelegate().getAppComponent(),
                               signupAssembly: SignupAssembly(view: self))
    }
    
    override func resolveDependencies(withResolver r: Resolver) {
        signupPresenter = r ~> SignupPresenter.self
    }
    
    // MARK: - SignupView
    
    func openMainScreen() {
        AppDelegate.sharedDelegate().setupRootViewController(animated: true)
    }

    func showUsername(validationMessage message: String?) {
        show(validationMessage: message, forView: usernameValidationView)
    }

    func showPassword(validationMessage message: String?) {
        show(validationMessage: message, forView: passwordValidationView)
    }
    
    func showPasswordConfirmation(validationMessage message: String?) {
        show(validationMessage: message, forView: passwordConfirmationValidationView)
    }
    
    func enableSignup() {
        signUpView.isEnabled = true
    }
    
    func disableSignup() {
        signUpView.isEnabled = false
    }
    
    // MARK: - UI logic methods
    
    private func setUpViews() {
        usernameView.delegate = self
        passwordView.delegate = self
        passwordConfirmationView.delegate = self
        disableSignup()
    }
    
    @IBAction func onSignupClicked(_ sender: UIButton) {
        signupPresenter.signup()
    }
    
    private func show(validationMessage message: String?, forView view: UILabel) {
        view.text = message
        view.isHidden = message == nil
    }
    
    // MARK: - UITextFieldDelegate
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let text = textField.text as NSString?
        if let text = text {
            let replacedText = text.replacingCharacters(in: range, with: string)
            if textField == usernameView {
                signupPresenter.validate(username: replacedText)
            } else if textField == passwordView {
                signupPresenter.validate(password: replacedText, passwordConfirmation: passwordConfirmationView.text!)
            } else if textField == passwordConfirmationView {
                signupPresenter.validate(password: passwordView.text!, passwordConfirmation: replacedText)
            }
        }
        
        return true
    }
}
