//
//  LogoutView.swift
//  PhotoMapSwiftAWS
//
//  Created by Roman Andreyenko on 5/4/17.
//  Copyright © 2017 iTechArt. All rights reserved.
//

import Foundation

/**
 View interface(MVP) for LogoutViewController
 */
protocol LogoutView: BaseLoadingView {
    
    /**
     Opens welcome screen
     */
    func openWelcomeScreen()
    
    /**
     Enabled logout functionality
     */
    func enableLogout()
    
    /**
     Disabled logout functionality
     */
    func disableLogout()
}
