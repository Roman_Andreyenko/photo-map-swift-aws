//
//  LoginView.swift
//  PhotoMapSwiftAWS
//
//  Created by Roman Andreyenko on 5/2/17.
//  Copyright © 2017 iTechArt. All rights reserved.
//

import Foundation

/**
 View interface(MVP) for LoginViewController
 */
protocol LoginView: BaseLoadingView {
    
    /**
     Opens main screen of the app
     */
    func openMainScreen()
    
    /**
     Shows validation message for username field
     
     - parameters:
        - validationMessage: description of validation result
     */
    func showUsername(validationMessage message: String?)

    /**
     Shows validation message for password field
     
     - parameters:
        - validationMessage: description of validation result
     */
    func showPassword(validationMessage message: String?)
    
    /**
     Enabled login functionality
     */
    func enableLogin()

    /**
     Disabled login functionality
     */
    func disableLogin()
}
