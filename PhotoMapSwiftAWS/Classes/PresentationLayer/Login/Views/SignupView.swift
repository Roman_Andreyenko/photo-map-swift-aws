//
//  SignupView.swift
//  PhotoMapSwiftAWS
//
//  Created by Roman Andreyenko on 5/3/17.
//  Copyright © 2017 iTechArt. All rights reserved.
//

import Foundation

/**
 View interface(MVP) for SignupViewController
 */
protocol SignupView: BaseLoadingView {
    
    /**
     Opens main screen of the app
     */
    func openMainScreen()
    
    /**
     Shows validation message for username field
     
     - parameters:
        - validationMessage: description of validation result
     */
    func showUsername(validationMessage message: String?)
    
    /**
     Shows validation message for password field
     
     - parameters:
        - validationMessage: description of validation result
     */
    func showPassword(validationMessage message: String?)

    /**
     Shows validation message for password confirmation field
     
     - parameters:
        - validationMessage: description of validation result
     */
    func showPasswordConfirmation(validationMessage message: String?)
    
    /**
     Enabled signup functionality
     */
    func enableSignup()
    
    /**
     Disabled signup functionality
     */
    func disableSignup()
}
