//
//  LogoutPresenterImpl.swift
//  PhotoMapSwiftAWS
//
//  Created by Roman Andreyenko on 5/4/17.
//  Copyright © 2017 iTechArt. All rights reserved.
//

import RxCocoa
import RxSwift
import ReachabilitySwift

/**
 Class implementation of LogoutPresenter
 */
class LogoutPresenterImpl: LogoutPresenter {
    
    //Inject
    private unowned let view: LogoutView
    //Inject
    private let interactor: SessionInteractor
    //Inject
    private let notificationCenter: NotificationCenter
    
    init(view: LogoutView, interactor: SessionInteractor, notificationCenter: NotificationCenter) {
        self.view = view
        self.interactor = interactor
        self.notificationCenter = notificationCenter
    }
    
    func willAppear() {
        setupLogoutAvailability(reachability: AppDelegate.sharedDelegate().reachability)
        notificationCenter.addObserver(self, selector: #selector(LogoutPresenterImpl.reachabilityChanged(note:)), name: ReachabilityChangedNotification, object: AppDelegate.sharedDelegate().reachability)
    }

    func willDisappear() {
        notificationCenter.removeObserver(self, name: ReachabilityChangedNotification, object: AppDelegate.sharedDelegate().reachability)
    }

    func destroy() {
        interactor.onCleanUp()
    }
    
    func logout() {
        let sessionObserver = AnyObserver<Void> { [unowned self] event in
            switch event {
            case .next(let value):
                self.view.hideActivityIndicator()
                self.didUserLogout(value)
            case .error(let error):
                self.view.hideActivityIndicator()
                self.failedUserLogout(error: error)
            case .completed:
                return
            }
        }
        view.showActivityIndicator()
        interactor.signout(observer: sessionObserver)
    }

    func didUserLogout() {
        view.openWelcomeScreen()
    }

    func failedUserLogout(error: Error) {
        view.show(error: error)
    }
    
    private func setupLogoutAvailability(reachability: Reachability) {
        if reachability.isReachable {
            view.enableLogout()
        } else {
            view.disableLogout()
        }
    }
    
    @objc private func reachabilityChanged(note: Notification) {
        let reachability = note.object as! Reachability
        setupLogoutAvailability(reachability: reachability)
        if !reachability.isReachable {
            view.showSettingsAlert()
        }
    }
}
