//
//  LoginPresenter.swift
//  PhotoMapSwiftAWS
//
//  Created by Roman Andreyenko on 4/28/17.
//  Copyright © 2017 iTechArt. All rights reserved.
//

import Foundation

/**
 Presenter interface(MVP) for LoginViewController
 */
protocol LoginPresenter: BasePresenter {
    
    /**
     View should call this method to login the user
     */
    func login()
    
    /**
     View should call this method to validate username
     
     - parameters:
        - username: name of user
     */
    func validate(username: String)
    
    /**
     View should call this method to validate password
     
     - parameters:
        - password: password of user
     */
    func validate(password: String)
    
    /**
     Callback that notifies that user is logined
     
     - parameters:
        - userSessionModel: session of the logined user
     */
    func didUserLogin(userSessionModel: UserSessionModel)

    /**
     Callback that notifies that user is failed to login
     
     - parameters:
        - error: reason of failure
     */
    func failedUserLogin(error: Error)
}
