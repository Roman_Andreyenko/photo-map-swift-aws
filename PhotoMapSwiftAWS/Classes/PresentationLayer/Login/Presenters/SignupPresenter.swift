//
//  SignupPresenter.swift
//  PhotoMapSwiftAWS
//
//  Created by Roman Andreyenko on 5/3/17.
//  Copyright © 2017 iTechArt. All rights reserved.
//

import Foundation

/**
 Presenter interface(MVP) for SignupViewController
 */
protocol SignupPresenter: BasePresenter {
    
    /**
     View should call this method to signup the user
     */
    func signup()
    
    /**
     View should call this method to validate username
     
     - parameters:
        - username: name of user
     */
    func validate(username: String)
    
    /**
     View should call this method to validate password and password confirmation
     
     - parameters:
        - password: password of user
        - passwordConfirmation: password confirmation
     */
    func validate(password: String, passwordConfirmation: String)
    
    /**
     Callback that notifies that user is signed up
     
     - parameters:
        - userSessionModel: new created user's session
     */
    func didUserSignup(userSessionModel: UserSessionModel)
    
    /**
     Callback that notifies that user is failed to signup
     
     - parameters:
        - error: reason of failure
     */
    func failedUserSignup(error: Error)
}
