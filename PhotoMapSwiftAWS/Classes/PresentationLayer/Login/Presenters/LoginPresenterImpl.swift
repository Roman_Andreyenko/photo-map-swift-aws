//
//  LoginPresenterImpl.swift
//  PhotoMapSwiftAWS
//
//  Created by Roman Andreyenko on 5/2/17.
//  Copyright © 2017 iTechArt. All rights reserved.
//

import RxCocoa
import RxSwift
import ReachabilitySwift

/**
 Class implementation of LoginPresenter
 */
class LoginPresenterImpl: LoginPresenter {

    //Inject
    private unowned let view: LoginView
    //Inject
    private let interactor: SessionInteractor
    //Inject
    private let dataMapper: SessionModelDataMapper
    //Inject
    private let notificationCenter: NotificationCenter
    
    private var username: String?
    private var password: String?

    private let usernameRegex: NSRegularExpression
    private let passwordRegex: NSRegularExpression
    
    init(view: LoginView, interactor: SessionInteractor, dataMapper: SessionModelDataMapper, notificationCenter: NotificationCenter) {
        self.view = view
        self.interactor = interactor
        self.dataMapper = dataMapper
        self.notificationCenter = notificationCenter
        self.usernameRegex = try! NSRegularExpression(pattern: "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}")
        self.passwordRegex = try! NSRegularExpression(pattern: "^.{8,20}$")
    }
    
    func login() {
        guard let username = username, let password = password else {
            return
        }
        let sessionObserver = AnyObserver<UserSession> { [unowned self] event in
            switch event {
            case .next(let value):
                self.view.hideActivityIndicator()
                self.didUserLogin(userSessionModel: self.dataMapper.transform(userSession: value))
            case .error(let error):
                self.view.hideActivityIndicator()
                self.failedUserLogin(error: error)
            case .completed:
                return
            }
        }
        view.showActivityIndicator()
        interactor.login(username: username, password: password, observer: sessionObserver)
    }
    
    func validate(username: String) {
        let isUsernameValid = self.usernameRegex.numberOfMatches(in: username, options: [], range: NSRange(location: 0, length: username.characters.count)) != 0
        self.username = isUsernameValid ? username : nil
        let validationMessage = isUsernameValid ? nil : NSLocalizedString("ValidEmail", comment: "")
        view.showUsername(validationMessage: validationMessage)
        setupLoginAvailability(reachability: AppDelegate.sharedDelegate().reachability)
    }
    
    func validate(password: String) {
        let isPasswordValid = self.passwordRegex.numberOfMatches(in: password, options: [], range: NSRange(location: 0, length: password.characters.count)) != 0
        self.password = isPasswordValid ? password : nil
        let validationMessage = isPasswordValid ? nil : NSLocalizedString("ValidPasswordLength", comment: "")
        view.showPassword(validationMessage: validationMessage)
        setupLoginAvailability(reachability: AppDelegate.sharedDelegate().reachability)
    }
    
    func didUserLogin(userSessionModel: UserSessionModel) {
        view.openMainScreen()
    }
    
    func failedUserLogin(error: Error) {
        view.show(error: error)
    }
    
    func willAppear() {
        notificationCenter.addObserver(self, selector: #selector(LoginPresenterImpl.reachabilityChanged(note:)), name: ReachabilityChangedNotification, object: AppDelegate.sharedDelegate().reachability)
    }
    
    func willDisappear() {
        notificationCenter.removeObserver(self, name: ReachabilityChangedNotification, object: AppDelegate.sharedDelegate().reachability)
    }
    
    func destroy() {
        interactor.onCleanUp()
    }
    
    private func setupLoginAvailability(reachability: Reachability) {
        if username != nil
            && password != nil
            && reachability.isReachable {
            view.enableLogin()
        } else {
            view.disableLogin()
        }
    }
    
    @objc private func reachabilityChanged(note: Notification) {
        let reachability = note.object as! Reachability
        setupLoginAvailability(reachability: reachability)
        if !reachability.isReachable {
            view.showSettingsAlert()
        }
    }
}
