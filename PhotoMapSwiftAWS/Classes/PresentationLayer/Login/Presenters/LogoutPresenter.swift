//
//  LogoutPresenter.swift
//  PhotoMapSwiftAWS
//
//  Created by Roman Andreyenko on 5/4/17.
//  Copyright © 2017 iTechArt. All rights reserved.
//

import Foundation

/**
 Presenter interface(MVP) for LoginViewController
 */
protocol LogoutPresenter: BasePresenter {
    
    /**
     View should call this method to logout the user
     */
    func logout()
    
    /**
     Callback that notifies that user is logged out
     */
    func didUserLogout()
    
    /**
     Callback that notifies that user is failed to logout
     
     - parameters:
        - error: reason of failure
     */
    func failedUserLogout(error: Error)
}
