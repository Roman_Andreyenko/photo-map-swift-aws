//
//  SignupPresenterImpl.swift
//  PhotoMapSwiftAWS
//
//  Created by Roman Andreyenko on 5/3/17.
//  Copyright © 2017 iTechArt. All rights reserved.
//

import RxCocoa
import RxSwift
import ReachabilitySwift

/**
 Class implementation of SignupPresenter
 */
class SignupPresenterImpl: SignupPresenter {

    //Inject
    private unowned let view: SignupView
    //Inject
    private let interactor: SessionInteractor
    //Inject
    private let dataMapper: SessionModelDataMapper
    //Inject
    private let notificationCenter: NotificationCenter
    
    private var username: String?
    private var password: String?
    private var passwordConfirmation: String?
    
    private let usernameRegex: NSRegularExpression
    private let passwordRegex: NSRegularExpression
    
    init(view: SignupView, interactor: SessionInteractor, dataMapper: SessionModelDataMapper, notificationCenter: NotificationCenter) {
        self.view = view
        self.interactor = interactor
        self.dataMapper = dataMapper
        self.notificationCenter = notificationCenter
        self.usernameRegex = try! NSRegularExpression(pattern: "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}")
        self.passwordRegex = try! NSRegularExpression(pattern: "^.{8,20}$")
    }
    
    func willAppear() {
        notificationCenter.addObserver(self, selector: #selector(SignupPresenterImpl.reachabilityChanged(note:)), name: ReachabilityChangedNotification, object: AppDelegate.sharedDelegate().reachability)
    }
    
    func willDisappear() {
        notificationCenter.removeObserver(self, name: ReachabilityChangedNotification, object: AppDelegate.sharedDelegate().reachability)
    }
    
    func destroy() {
        interactor.onCleanUp()
    }
    
    func signup() {
        guard let username = username, let password = password else {
            return
        }
        let sessionObserver = AnyObserver<UserSession> { [unowned self] event in
            switch event {
            case .next(let value):
                self.view.hideActivityIndicator()
                self.didUserSignup(userSessionModel: self.dataMapper.transform(userSession: value))
            case .error(let error):
                self.view.hideActivityIndicator()
                self.failedUserSignup(error: error)
            case .completed:
                return
            }
        }
        view.showActivityIndicator()
        interactor.signup(username: username, password: password, observer: sessionObserver)
    }

    func validate(username: String) {
        let isUsernameValid = self.usernameRegex.numberOfMatches(in: username, options: [], range: NSRange(location: 0, length: username.characters.count)) != 0
        self.username = isUsernameValid ? username : nil
        let validationMessage = isUsernameValid ? nil : NSLocalizedString("ValidEmail", comment: "")
        view.showUsername(validationMessage: validationMessage)
        setupSignupAvailability(reachability: AppDelegate.sharedDelegate().reachability)
    }
    
    func validate(password: String, passwordConfirmation: String) {
        validateInternal(password: password, passwordConfirmation: passwordConfirmation)
        validateInternal(passwordConfirmation: passwordConfirmation, password: password)
    }
    
    func didUserSignup(userSessionModel: UserSessionModel) {
        view.openMainScreen()
    }
    
    func failedUserSignup(error: Error) {
        view.show(error: error)
    }
    
    private func setupSignupAvailability(reachability: Reachability) {
        if username != nil
            && password != nil
            && passwordConfirmation != nil
            && reachability.isReachable {
            view.enableSignup()
        } else {
            view.disableSignup()
        }
    }
    
    private func validateInternal(password: String, passwordConfirmation: String) {
        let isPasswordValid = self.passwordRegex.numberOfMatches(in: password, options: [], range: NSRange(location: 0, length: password.characters.count)) != 0
        var validationMessage: String? = nil
        if isPasswordValid {
            self.password = password
            validationMessage = password == passwordConfirmation ? nil : NSLocalizedString("ValidPassword", comment: "")
        } else {
            validationMessage = NSLocalizedString("ValidPasswordLength", comment: "")
        }
        view.showPassword(validationMessage: validationMessage)
        setupSignupAvailability(reachability: AppDelegate.sharedDelegate().reachability)
    }
    
    private func validateInternal(passwordConfirmation: String, password: String) {
        let isPasswordConfirmationValid = passwordConfirmation == password
        self.passwordConfirmation = isPasswordConfirmationValid ? passwordConfirmation : nil
        let validationMessage = isPasswordConfirmationValid ? nil : NSLocalizedString("ValidConfirmPassword", comment: "")
        view.showPasswordConfirmation(validationMessage: validationMessage)
        setupSignupAvailability(reachability: AppDelegate.sharedDelegate().reachability)
    }
    
    @objc private func reachabilityChanged(note: Notification) {
        let reachability = note.object as! Reachability
        setupSignupAvailability(reachability: reachability)
        if !reachability.isReachable {
            view.showSettingsAlert()
        }
    }
}
