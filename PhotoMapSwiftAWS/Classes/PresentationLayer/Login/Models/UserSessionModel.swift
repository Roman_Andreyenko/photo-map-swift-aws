//
//  UserSessionModel.swift
//  PhotoMapSwiftAWS
//
//  Created by Roman Andreyenko on 5/2/17.
//  Copyright © 2017 iTechArt. All rights reserved.
//

import Foundation

/**
 Class representation of a user session model
 */
class UserSessionModel {
    
    let accessToken: String
    
    init(accessToken: String) {
        self.accessToken = accessToken
    }
}
