//
//  UserModel.swift
//  PhotoMapSwiftAWS
//
//  Created by Roman Andreyenko on 5/2/17.
//  Copyright © 2017 iTechArt. All rights reserved.
//

import Foundation

/**
 Class representation of a user model
 */
class UserModel {

    let username: String
    let userId: String
    
    init(username: String, userId: String) {
        self.username = username
        self.userId = userId
    }
}
