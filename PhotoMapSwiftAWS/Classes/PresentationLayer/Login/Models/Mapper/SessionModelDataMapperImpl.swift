//
//  SessionModelDataMapperImpl.swift
//  PhotoMapSwiftAWS
//
//  Created by Roman Andreyenko on 5/2/17.
//  Copyright © 2017 iTechArt. All rights reserved.
//

import Foundation

/**
 Class implementation of SessionModelDataMapper
 */
class SessionModelDataMapperImpl: SessionModelDataMapper {

    func transform(userModel: UserModel) -> User {
        return User(username: userModel.username, userId: userModel.userId)
    }
    
    func transform(user: User) -> UserModel {
        return UserModel(username: user.username, userId: user.userId)
    }
    
    func transform(userSessionModel: UserSessionModel) -> UserSession {
        return UserSession(accessToken: userSessionModel.accessToken)
    }
    
    func transform(userSession: UserSession) -> UserSessionModel {
        return UserSessionModel(accessToken: userSession.accessToken)
    }
}
