//
//  SessionModelDataMapper.swift
//  PhotoMapSwiftAWS
//
//  Created by Roman Andreyenko on 5/2/17.
//  Copyright © 2017 iTechArt. All rights reserved.
//

import Foundation

/**
 Contract to map Session related data from presentation layer to domain layer and vice versa.
 */
protocol SessionModelDataMapper {
    
    /**
     Converts UserModel to a domain layer User
     
     - parameters:
        - userModel: the model to convert
     
     - returns:
        the converted User
     */
    func transform(userModel: UserModel) -> User
    
    /**
     Converts User to a presentation layer UserModel
     
     - parameters:
        - user: the model to convert
     
     - returns:
        the converted UserModel
     */
    func transform(user: User) -> UserModel
    
    /**
     Converts UserSessionModel to a domain layer UserSession
     
     - parameters:
        - userSessionModel: the model to convert
     
     - returns:
        the converted UserSession
     */
    func transform(userSessionModel: UserSessionModel) -> UserSession
    
    /**
     Converts UserSession to a presentation layer UserSessionModel
     
     - parameters:
        - userSession: the model to convert
     
     - returns:
        the converted UserSessionModel
     */
    func transform(userSession: UserSession) -> UserSessionModel
}
