//
//  LogoutAssembly.swift
//  PhotoMapSwiftAWS
//
//  Created by Roman Andreyenko on 5/4/17.
//  Copyright © 2017 iTechArt. All rights reserved.
//

import Swinject

/**
 Swinject assembly which provides dependency bindings for LogoutComponent
 */
class LogoutAssembly: Assembly {
    
    private unowned let view: LogoutView
    
    init(view: LogoutView) {
        self.view = view
    }
    
    func assemble(container: Container) {
        assembleLogoutView(container: container)
        assemleLogoutPresenter(container: container)
    }
    
    private func assembleLogoutView(container: Container) {
        container.register(LogoutView.self) { r in
            return self.view
        }
    }
    
    private func assemleLogoutPresenter(container: Container) {
        container.autoregister(LogoutPresenter.self, initializer: LogoutPresenterImpl.init)
    }
}
