//
//  LoginAssembly.swift
//  PhotoMapSwiftAWS
//
//  Created by Roman Andreyenko on 5/2/17.
//  Copyright © 2017 iTechArt. All rights reserved.
//

import Swinject

/**
 Swinject assembly which provides dependency bindings for LoginComponent
 */
class LoginAssembly: Assembly {

    private unowned let view: LoginView
    
    init(view: LoginView) {
        self.view = view
    }
    
    func assemble(container: Container) {
        assembleLoginView(container: container)
        assemleLoginPresenter(container: container)
    }
    
    private func assembleLoginView(container: Container) {
        container.register(LoginView.self) { r in
            return self.view
        }
    }
    
    private func assemleLoginPresenter(container: Container) {
        container.autoregister(LoginPresenter.self, initializer: LoginPresenterImpl.init)
    }
}
