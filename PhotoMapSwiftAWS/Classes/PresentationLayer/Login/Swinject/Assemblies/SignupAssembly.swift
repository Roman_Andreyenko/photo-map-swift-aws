//
//  SignupAssembly.swift
//  PhotoMapSwiftAWS
//
//  Created by Roman Andreyenko on 5/3/17.
//  Copyright © 2017 iTechArt. All rights reserved.
//

import Swinject

/**
 Swinject assembly which provides dependency bindings for SignupComponent
 */
class SignupAssembly: Assembly {

    private unowned let view: SignupView
    
    init(view: SignupView) {
        self.view = view
    }
    
    func assemble(container: Container) {
        assembleSignupView(container: container)
        assemleSignupPresenter(container: container)
    }
    
    private func assembleSignupView(container: Container) {
        container.register(SignupView.self) { r in
            return self.view
        }
    }
    
    private func assemleSignupPresenter(container: Container) {
        container.autoregister(SignupPresenter.self, initializer: SignupPresenterImpl.init)
    }
}
