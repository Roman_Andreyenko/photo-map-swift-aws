//
//  LoginComponent.swift
//  PhotoMapSwiftAWS
//
//  Created by Roman Andreyenko on 5/2/17.
//  Copyright © 2017 iTechArt. All rights reserved.
//

import Swinject

/**
 Swinject component for LoginViewController
 */
class LoginComponent: BaseComponent {

    init(applicationComponent: ApplicationComponent, loginAssembly: LoginAssembly) {
        super.init()
        self.assembler = try! Assembler(assemblies: [loginAssembly], parentAssembler: applicationComponent.assembler)
    }
}
