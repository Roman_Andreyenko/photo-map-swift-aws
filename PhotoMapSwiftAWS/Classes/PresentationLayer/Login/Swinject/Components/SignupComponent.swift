//
//  SignupComponent.swift
//  PhotoMapSwiftAWS
//
//  Created by Roman Andreyenko on 5/3/17.
//  Copyright © 2017 iTechArt. All rights reserved.
//

import Swinject

/**
 Swinject component for SignupViewController
 */
class SignupComponent: BaseComponent {

    init(applicationComponent: ApplicationComponent, signupAssembly: SignupAssembly) {
        super.init()
        self.assembler = try! Assembler(assemblies: [signupAssembly], parentAssembler: applicationComponent.assembler)
    }
}
