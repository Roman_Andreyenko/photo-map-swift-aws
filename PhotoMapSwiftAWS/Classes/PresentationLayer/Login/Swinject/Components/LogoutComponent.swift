//
//  LogoutComponent.swift
//  PhotoMapSwiftAWS
//
//  Created by Roman Andreyenko on 5/4/17.
//  Copyright © 2017 iTechArt. All rights reserved.
//

import Swinject

/**
 Swinject component for LogoutViewController
 */
class LogoutComponent: BaseComponent {

    init(applicationComponent: ApplicationComponent, logoutAssembly: LogoutAssembly) {
        super.init()
        self.assembler = try! Assembler(assemblies: [logoutAssembly], parentAssembler: applicationComponent.assembler)
    }
}
