//
//  SessionRepository.swift
//  PhotoMapSwiftAWS
//
//  Created by Roman Andreyenko on 4/28/17.
//  Copyright © 2017 iTechArt. All rights reserved.
//

import RxCocoa
import RxSwift

/**
 Contract pertaining to business logic around session data
 */
protocol SessionRepository {
    
    /**
     Logins existing user to the app
     
     - parameters:
        - username: unique name of user
        - password: user password
     - returns:
        observable for UserSession
     */
    func login(username: String, password: String) -> Observable<UserSession>
    
    /**
     Registers new user to the app
     
     - parameters:
        - username: unique name of user
        - password: user password
     - returns:
        observable for User
     */
    func signup(username: String, password: String) -> Observable<User>
    
    /**
     Signouts existing user from the app
     
     - returns:
        observable
     */
    func signout() -> Observable<Void>
    
    /**
     Gets current user related data
     
     - returns:
        observable for User
     */
    func getCurrentUser() -> Observable<User?>
}
