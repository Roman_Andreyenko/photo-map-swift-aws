//
//  SessionInteractorImpl.swift
//  PhotoMapSwiftAWS
//
//  Created by Roman Andreyenko on 5/2/17.
//  Copyright © 2017 iTechArt. All rights reserved.
//

import RxCocoa
import RxSwift

/**
 Class implementation of SessionInteractor
 */
class SessionInteractorImpl: SessionInteractor {

    private let repository: SessionRepository
    private let subscriptionScheduler: SchedulerType
    private let observationScheduler: SerialDispatchQueueScheduler
    private var disposeBag: DisposeBag!
    
    init(repository: SessionRepository, subscriptionScheduler: SchedulerType, observationScheduler: SerialDispatchQueueScheduler) {
        self.repository = repository
        self.subscriptionScheduler = subscriptionScheduler
        self.observationScheduler = observationScheduler
        initialize()
    }
    
    func initialize() {
        disposeBag = DisposeBag()
    }
    
    func login(username: String, password: String, observer: AnyObserver<UserSession>) {
        repository.login(username: username, password: password)
            .subscribeOn(subscriptionScheduler)
            .observeOn(observationScheduler)
            .subscribe(observer)
            .disposed(by: disposeBag)
    }
    
    func signup(username: String, password: String, observer: AnyObserver<UserSession>) {
        repository.signup(username: username, password: password)
            .subscribeOn(subscriptionScheduler)
            .flatMapLatest { [unowned self] user in
                return self.repository.login(username: username, password: password)
            }
            .observeOn(observationScheduler)
            .subscribe(observer)
            .disposed(by: disposeBag)
    }
    
    func signout(observer: AnyObserver<Void>) {
        repository.signout()
            .subscribeOn(subscriptionScheduler)
            .observeOn(observationScheduler)
            .subscribe(observer)
            .disposed(by: disposeBag)
    }
    
    func getCurrentUser(observer: AnyObserver<User?>) {
        repository.getCurrentUser()
            .subscribeOn(subscriptionScheduler)
            .observeOn(observationScheduler)
            .subscribe(observer)
            .disposed(by: disposeBag)
    }
    
    func getSubscription() -> DisposeBag {
        return disposeBag
    }
    
    func onCleanUp() {
        disposeBag = nil
    }
}
