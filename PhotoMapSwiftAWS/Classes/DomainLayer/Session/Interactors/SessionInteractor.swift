//
//  SessionInteractor.swift
//  PhotoMapSwiftAWS
//
//  Created by Roman Andreyenko on 4/28/17.
//  Copyright © 2017 iTechArt. All rights reserved.
//

import RxCocoa
import RxSwift

/**
 Interactor interface that provides Session data from data layer to presentation layer.
 */
protocol SessionInteractor: BaseInteractor {
    
    /**
     Logins existing user to the app
     
     - parameters:
        - username: unique name of user
        - password: user password
        - observer: listens for responses from request
     */
    func login(username: String, password: String, observer: AnyObserver<UserSession>)

    /**
     Registers new user to the app
     
     - parameters:
        - username: unique name of user
        - password: user password
        - observer: listens for responses from request
     */
    func signup(username: String, password: String, observer: AnyObserver<UserSession>)
    
    /**
     Signouts existing user from the app
     
     - parameters:
        - observer: listens for responses from request
     */
    func signout(observer: AnyObserver<Void>)
    
    /**
     Gets current user related data
     
     - parameters:
        - observer: listens for responses from request
     */
    func getCurrentUser(observer: AnyObserver<User?>)
}
