//
//  User.swift
//  PhotoMapSwiftAWS
//
//  Created by Roman Andreyenko on 4/28/17.
//  Copyright © 2017 iTechArt. All rights reserved.
//

import Foundation

/**
 Class representation of a User
 */
class User {
    
    let username: String
    let userId: String
    
    init(username: String, userId: String) {
        self.username = username
        self.userId = userId
    }
}
