//
//  UserSession.swift
//  PhotoMapSwiftAWS
//
//  Created by Roman Andreyenko on 4/28/17.
//  Copyright © 2017 iTechArt. All rights reserved.
//

import Foundation

/**
 Class representation of a UserSession
 */
class UserSession {
    
    let accessToken: String
    
    init(accessToken: String) {
        self.accessToken = accessToken
    }
}
