//
//  HashtagIndex.swift
//  PhotoMapSwiftAWS
//
//  Created by Roman Andreyenko on 5/19/17.
//  Copyright © 2017 iTechArt. All rights reserved.
//

import Foundation

/**
 Class representation of a HashtagIndex
 */
class HashtagIndex: NSObject {
    
    var hashtagName: String
    var photoId: String
    
    init(hashtagName: String, photoId: String) {
        self.hashtagName = hashtagName
        self.photoId = photoId
    }
    
    public class func parseTags(inText textWithTags: String) -> [String] {
        
        let regex = try! NSRegularExpression(pattern: "#(\\w+)")
        let matches = regex.matches(in: textWithTags, options: [], range: NSRange(location: 0, length: textWithTags.characters.count))
        var hashtags = Set<String>(minimumCapacity: matches.count)
        for match in matches {
            let matchRange = match.rangeAt(1)
            let wordRange = textWithTags.index(textWithTags.startIndex, offsetBy: matchRange.location)..<textWithTags.index(textWithTags.startIndex, offsetBy: matchRange.location + matchRange.length)
            let word = textWithTags.substring(with: wordRange)
            hashtags.insert(word)
        }
        
        return Array(hashtags)
    }
}
