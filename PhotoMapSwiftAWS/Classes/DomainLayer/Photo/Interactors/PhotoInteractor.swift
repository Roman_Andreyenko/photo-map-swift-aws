//
//  PhotoInteractor.swift
//  PhotoMapSwiftAWS
//
//  Created by Roman Andreyenko on 5/19/17.
//  Copyright © 2017 iTechArt. All rights reserved.
//

import RxCocoa
import RxSwift

/**
 Interactor interface that provides Photo data from data layer to presentation layer.
 */
protocol PhotoInteractor: BaseInteractor {
    
    /**
     Fetches all user photos filtered by search text
     
     - parameters:
        - forUserId: user id
        - withSearchText: search text which may contain tags to filter photos
        - observer: listens for responses from request
     */
    func fetchPhotos(forUserId: String, withSearchText: String, observer: AnyObserver<[Photo]>)

    /**
     Creates new user photo
     
     - parameters:
        - withPictureData: data that represents full quality picture
        - withThumbnailPictureData: data that represents minimal quality picture
        - withOwnerId: user id
        - withCategory: category of the photo
        - withDescriptionText: text that describes photo
        - withLatitude: latitude of the photo geographical location
        - withLongitude: latitude of the photo geographical location
        - withCreatedTime: creation time of the photo
        - observer: listens for responses from request
     */
    func createPhoto(withPictureData: Data, withThumbnailPictureData: Data, withOwnerId: String, withCategory: Photo.Category, withDescriptionText: String, withLatitude: Double, withLongitude: Double, withCreatedTime: Date, observer: AnyObserver<Photo>)
    
    /**
     Downloads full quality picture of photo
     
     - parameters:
        - of: photo model of picture
        - observer: listens for responses from request
     */
    func downloadPicture(of: PhotoModel, observer: AnyObserver<PhotoPicture>)

    /**
     Downloads minimal quality picture of photo
     
     - parameters:
        - of: photo model of picture
        - observer: listens for responses from request
     */
    func downloadThumbnailPicture(of: PhotoModel, observer: AnyObserver<PhotoPicture>)
    
    /**
     Cancels downloading of full quality picture of photo
     
     - parameters:
        - of: photo model of picture
     */
    func cancelPictureDownloading(of: PhotoModel)

    /**
     Cancels downloading of minimal quality picture of photo
     
     - parameters:
        - of: photo model of picture
     */
    func cancelThumbnailPictureDownloading(of: PhotoModel)
}
