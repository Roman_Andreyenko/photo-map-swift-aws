//
//  PhotoInteractorImpl.swift
//  PhotoMapSwiftAWS
//
//  Created by Roman Andreyenko on 5/19/17.
//  Copyright © 2017 iTechArt. All rights reserved.
//

import RxCocoa
import RxSwift

/**
 Class implementation of PhotoInteractor
 */
class PhotoInteractorImpl: PhotoInteractor {

    private let repository: PhotoRepository
    private let subscriptionScheduler: SchedulerType
    private let observationScheduler: SerialDispatchQueueScheduler
    private var disposeBag: DisposeBag!
    private lazy var pictureSubscriptions = Dictionary<String, Disposable>()
    private lazy var pictureThumbnailSubscriptions = Dictionary<String, Disposable>()
    
    init(repository: PhotoRepository, subscriptionScheduler: SchedulerType, observationScheduler: SerialDispatchQueueScheduler) {
        self.repository = repository
        self.subscriptionScheduler = subscriptionScheduler
        self.observationScheduler = observationScheduler
        initialize()
    }
    
    func fetchPhotos(forUserId userId: String, withSearchText searchText: String, observer: AnyObserver<[Photo]>) {
        let tags = HashtagIndex.parseTags(inText: searchText)
        var fetchPhotosObservable = repository.getPhotos(forUserId: userId)
        if !tags.isEmpty {
            fetchPhotosObservable = repository.getHashtagIndexes(withHashtagNames: tags)
                .flatMapLatest { [unowned self] hashtagIndexes -> Observable<[Photo]> in
                    return self.repository.getPhotos(forUserId: userId, withHashtagIndexes: hashtagIndexes)
            }
        }
        
        fetchPhotosObservable.subscribeOn(subscriptionScheduler)
            .observeOn(observationScheduler)
            .subscribe(observer)
            .disposed(by: disposeBag)
    }
    
    func createPhoto(withPictureData pictureData: Data,
                     withThumbnailPictureData thumbnailPictureData: Data,
                     withOwnerId ownerId: String,
                     withCategory category: Photo.Category,
                     withDescriptionText descriptionText: String,
                     withLatitude latitude: Double,
                     withLongitude longitude: Double,
                     withCreatedTime createdTime: Date,
                     observer: AnyObserver<Photo>) {
        var photo: Photo! = nil
        var createPhotoObservable = repository.createPhoto(withOwnerId: ownerId, withCategory: category, withDescriptionText: descriptionText, withLatitude: latitude, withLongitude: longitude, withCreatedTime: createdTime)
            .flatMapLatest { [unowned self] createdPhoto -> Observable<String> in
                photo = createdPhoto
                return self.repository.uploadPhoto(withThumbnailPictureData: thumbnailPictureData, withName: photo.photoId)
            }
            .flatMapLatest { [unowned self] pictureName -> Observable<String> in
                return self.repository.uploadPhoto(withPictureData: pictureData, withName: photo.photoId)
            }.map { pictureThumbnailName -> Photo in
                return photo
        }
        let tags = HashtagIndex.parseTags(inText: descriptionText)
        if !tags.isEmpty {
            createPhotoObservable = createPhotoObservable
                .flatMapLatest { [unowned self] createdPhoto -> Observable<HashtagIndex> in
                    var photoIndexesObservable = Observable<HashtagIndex>.empty()
                    for tag in tags {
                        photoIndexesObservable = photoIndexesObservable.concat(self.repository.createHashtagIndex(forPhoto: photo, withHashtagName: tag))
                    }
                    return photoIndexesObservable
                }
                .toArray()
                .map { hashtagIndexes -> Photo in
                    return photo
            }
        }
        
        createPhotoObservable.subscribeOn(subscriptionScheduler)
            .observeOn(observationScheduler)
            .subscribe(observer)
            .disposed(by: disposeBag)
    }
    
    func downloadPicture(of photoModel: PhotoModel, observer: AnyObserver<PhotoPicture>) {
        pictureSubscriptions[photoModel.photoId] = repository.downloadPhoto(withName: photoModel.photoId)
            .subscribeOn(subscriptionScheduler)
            .observeOn(observationScheduler)
            .subscribe(observer)
    }
    
    func downloadThumbnailPicture(of photoModel: PhotoModel, observer: AnyObserver<PhotoPicture>) {
        pictureThumbnailSubscriptions[photoModel.photoId] = repository.downloadThumbnailPhoto(withName: photoModel.photoId)
            .subscribeOn(subscriptionScheduler)
            .observeOn(observationScheduler)
            .subscribe(observer)
    }
    
    func cancelPictureDownloading(of photoModel: PhotoModel) {
        let subscription = pictureSubscriptions.removeValue(forKey: photoModel.photoId)
        if let subscription = subscription {
            subscription.dispose()
        }
    }
    
    func cancelThumbnailPictureDownloading(of photoModel: PhotoModel) {
        let subscription = pictureThumbnailSubscriptions.removeValue(forKey: photoModel.photoId)
        if let subscription = subscription {
            subscription.dispose()
        }
    }
    
    func initialize() {
        disposeBag = DisposeBag()
    }
    
    func getSubscription() -> DisposeBag {
        return disposeBag
    }
    
    func onCleanUp() {
        disposeBag = nil
        dispose(pictureSubscriptions: pictureSubscriptions)
        dispose(pictureSubscriptions: pictureThumbnailSubscriptions)
    }
    
    private func dispose(pictureSubscriptions: Dictionary<String, Disposable>) {
        for value in pictureSubscriptions.values {
            value.dispose()
        }
    }
}
