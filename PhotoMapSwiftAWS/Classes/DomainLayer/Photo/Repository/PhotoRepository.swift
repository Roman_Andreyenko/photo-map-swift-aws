//
//  PhotoRepository.swift
//  PhotoMapSwiftAWS
//
//  Created by Roman Andreyenko on 5/19/17.
//  Copyright © 2017 iTechArt. All rights reserved.
//

import RxCocoa
import RxSwift

/**
 Contract pertaining to business logic around photo data
 */
protocol PhotoRepository {
    
    /**
     Gets hashtag index list for certain hastags
     
    - parameters:
        - withHashtagNames: list of hashtag's names
     
    - returns:
        observable for hashtag index list
     */
    func getHashtagIndexes(withHashtagNames: [String]) -> Observable<[HashtagIndex]>
    
    /**
     Gets photo list for provided user
     
     - parameters:
        - forUserId: user's id
     
     - returns:
        observable for photo list
     */
    func getPhotos(forUserId: String) -> Observable<[Photo]>
    
    /**
     Gets photo list for provided user
     
     - parameters:
        - forUserId: user's id
        - withHashtagIndexes: hashtag index entity list
     
     - returns:
        observable for photo list
     */
    func getPhotos(forUserId: String, withHashtagIndexes: [HashtagIndex]) -> Observable<[Photo]>
    
    /**
     Creates hashtag index for certain photo
     
     - parameters:
        - forPhoto: photo entity, which hashtag index will be created for
        - withHashtagName: hashtag name
     
     - returns:
        observable for new created hashtag index
     */
    func createHashtagIndex(forPhoto: Photo, withHashtagName: String) -> Observable<HashtagIndex>
    
    /**
     Creates photo
     
     - parameters:
        - withOwnerId: user's id of photo owner
        - withCategory: category of photo (possible values: default, nature, friends)
        - withDescriptionText: photo description text
        - withLatitude: geographical latitude of photo location
        - withLongitude: geographical longitude of photo location
        - withCreatedTime: date of photo creation
     
     - returns:
        observable for new created photo
     */
    func createPhoto(withOwnerId: String, withCategory: Photo.Category, withDescriptionText: String, withLatitude: Double, withLongitude: Double, withCreatedTime: Date) -> Observable<Photo>
    
    /**
     Uploads photo picture to data store (Cloud or Database)
     
     - parameters:
        - withPictureData: binary picture representation
        - withName: picture name
     
     - returns:
        observable for uploaded picture
     */
    func uploadPhoto(withPictureData: Data, withName: String) -> Observable<String>
    
    /**
     Uploads photo thumbnail picture to data store (Cloud or Database)
     
     - parameters:
        - withThumbnailPictureData: binary picture representation
        - withName: picture name
     
     - returns:
        observable for uploaded thumbnail picture
     */
    func uploadPhoto(withThumbnailPictureData: Data, withName: String) -> Observable<String>
    
    /**
     Downloads photo picture from data store (Cloud or Database)
     
     - parameters:
        - withName: picture name
     
     - returns:
        observable for downloaded PhotoPicture
     */
    func downloadPhoto(withName: String) -> Observable<PhotoPicture>
    
    /**
     Downloads photo thumbnail picture from data store (Cloud or Database)
     
     - parameters:
        - withName: picture name
     
     - returns:
        observable for downloaded PhotoPicture
     */
    func downloadThumbnailPhoto(withName: String) -> Observable<PhotoPicture>
}
