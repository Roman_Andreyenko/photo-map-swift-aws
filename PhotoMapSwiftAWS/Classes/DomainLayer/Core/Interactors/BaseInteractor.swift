//
//  BaseInteractor.swift
//  PhotoMapSwiftAWS
//
//  Created by Roman Andreyenko on 5/2/17.
//  Copyright © 2017 iTechArt. All rights reserved.
//

import RxCocoa
import RxSwift

/**
 Base protocol for interactors.
 */
protocol BaseInteractor {
    
    /**
     Initialize interactor.
     */
    func initialize()
    
    /**
     Gets composite subscription
     
     - returns:
        dispose bag with all subscriptions
     */
    func getSubscription() -> DisposeBag
    
    /**
     Cleans up any data processing. i.e Unsubscribe all subscribers.
     */
    func onCleanUp()
}
