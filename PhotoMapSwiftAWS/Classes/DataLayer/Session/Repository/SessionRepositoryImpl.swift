//
//  SessionRepositoryImpl.swift
//  PhotoMapSwiftAWS
//
//  Created by Roman Andreyenko on 4/28/17.
//  Copyright © 2017 iTechArt. All rights reserved.
//

import RxCocoa
import RxSwift

/**
 Class implementation of SessionRepository
 */
class SessionRepositoryImpl: SessionRepository {

    //Inject
    private var dataSource: SessionDataStore
    //Inject
    private var dataMapper: SessionDataMapper
    
    init(dataSource: SessionDataStore, dataMapper: SessionDataMapper) {
        self.dataSource = dataSource
        self.dataMapper = dataMapper
    }
    
    func login(username: String, password: String) -> Observable<UserSession> {
        return dataSource.login(username: username, password: password).map { [weak self] userSessionEntity in
            return self!.dataMapper.transform(userSessionEntity: userSessionEntity)
        }
    }
    
    func signup(username: String, password: String) -> Observable<User> {
        return dataSource.signup(username: username, password: password).map { [weak self] userEntity in
            return self!.dataMapper.transform(userEntity: userEntity)
        }
    }
    
    func signout() -> Observable<Void> {
        return dataSource.signout()
    }
    
    func getCurrentUser() -> Observable<User?> {
        return dataSource.getCurrentUser().map { [weak self] userEntity in
            return userEntity == nil ? nil : self!.dataMapper.transform(userEntity: userEntity!)
        }
    }
    
}
