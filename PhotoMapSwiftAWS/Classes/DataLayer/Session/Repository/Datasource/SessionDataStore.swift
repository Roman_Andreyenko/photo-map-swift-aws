//
//  LoginDataStore.swift
//  PhotoMapSwiftAWS
//
//  Created by Roman Andreyenko on 4/28/17.
//  Copyright © 2017 iTechArt. All rights reserved.
//

import RxCocoa
import RxSwift

/**
 Contract that provides session entity related items from data layer.
 */
protocol SessionDataStore {
    
    /**
     Logins user from data store (Cloud or Database)
     
     - parameters:
        - username: unique name of user
        - password: user password
     
     - returns:
        observable for UserSessionEntity
     */
    func login(username: String, password: String) -> Observable<UserSessionEntity>

    /**
     Registers user to data store (Cloud or Database)
     
     - parameters:
        - username: unique name of user
        - password: user password
     
     - returns:
        observable for UserEntity
     */
    func signup(username: String, password: String) -> Observable<UserEntity>
    
    /**
     Signouts user from data store (Cloud or Database)
     
     - returns:
        observable
     */
    func signout() -> Observable<Void>
    
    /**
     Gets current authenticared user from data store (Cloud or Database)
     
     - returns:
        observable for UserEntity
     */
    func getCurrentUser() -> Observable<UserEntity?>
}
