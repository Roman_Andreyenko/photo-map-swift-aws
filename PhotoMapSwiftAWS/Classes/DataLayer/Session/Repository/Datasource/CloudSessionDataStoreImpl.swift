//
//  CloudLoginDataStoreImpl.swift
//  PhotoMapSwiftAWS
//
//  Created by Roman Andreyenko on 4/28/17.
//  Copyright © 2017 iTechArt. All rights reserved.
//

import RxCocoa
import RxSwift
import AWSCognitoIdentityProvider

/**
 Class implementation of SessionDataStore
 Handles Photo data coming from Cloud.
 */
class CloudSessionDataStoreImpl: SessionDataStore {

    private var userPool: AWSCognitoIdentityUserPool
    private var credentialsProvider: AWSCognitoCredentialsProvider
    
    init(userPool: AWSCognitoIdentityUserPool, credentialsProvider: AWSCognitoCredentialsProvider) {
        self.userPool = userPool
        self.credentialsProvider = credentialsProvider
    }
    
    func login(username: String, password: String) -> Observable<UserSessionEntity> {
        return Observable<UserSessionEntity>.create { [unowned self] observer in
            self.userPool.getUser()
                .getSession(username, password: password, validationData: nil)
                .continueWith(block: { task in
                    if let error = task.error {
                        observer.onError(AWSUtils.correctAWS(error: error))
                    } else {
                        let userSession = UserSessionEntity(accessToken: task.result!.accessToken!.tokenString)
                        observer.onNext(userSession)
                        observer.onCompleted()
                    }
                    return nil
                })
            return Disposables.create()
        }
    }
    
    func signup(username: String, password: String) -> Observable<UserEntity> {
        return Observable<UserEntity>.create { [unowned self] observer in
            self.userPool
                .signUp(username, password: password, userAttributes: nil, validationData: nil)
                .continueWith(block: { [unowned self] task in
                    if let error = task.error {
                        observer.onError(AWSUtils.correctAWS(error: error))
                    } else {
                        let user = UserEntity(username: task.result!.user.username!, userId: self.credentialsProvider.identityId!)
                        observer.onNext(user)
                        observer.onCompleted()
                    }
                    return nil
                })
            return Disposables.create()
        }
    }
    
    func signout() -> Observable<Void> {
        return Observable<Void>.create { [unowned self] observer in
            self.userPool.currentUser()?.signOut()
            observer.onNext()
            observer.onCompleted()
            return Disposables.create()
        }
    }
    
    func getCurrentUser() -> Observable<UserEntity?> {
        return Observable<UserEntity?>.create { [unowned self] observer in
            if let currentUser = self.userPool.currentUser() {
                if currentUser.isSignedIn {
                    let user = UserEntity(username: currentUser.username!, userId: self.credentialsProvider.identityId!)
                    observer.onNext(user)
                } else {
                    observer.onNext(nil)
                }
            } else {
                observer.onNext(nil)
            }
            observer.onCompleted()
            return Disposables.create()
        }
    }
}
