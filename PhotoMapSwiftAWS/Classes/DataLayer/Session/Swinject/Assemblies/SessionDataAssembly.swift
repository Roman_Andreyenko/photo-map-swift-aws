//
//  SessionDataAssembly.swift
//  PhotoMapSwiftAWS
//
//  Created by Roman Andreyenko on 5/2/17.
//  Copyright © 2017 iTechArt. All rights reserved.
//

import Swinject

/**
 Swinject assembly which provides dependency bindings for session related components
 */
class SessionDataAssembly: Assembly {

    func assemble(container: Container) {
        assembleSessionRepository(container: container)
        assembleSessionDataStore(container: container)
        assembleSessionDataMapper(container: container)
    }
    
    private func assembleSessionRepository(container: Container) {
        container.autoregister(SessionRepository.self, initializer: SessionRepositoryImpl.init)
    }
    
    private func assembleSessionDataStore(container: Container) {
        container.autoregister(SessionDataStore.self, initializer: CloudSessionDataStoreImpl.init)
    }
    
    private func assembleSessionDataMapper(container: Container) {
        container.register(SessionDataMapper.self) { r in
            return SessionDataMapperImpl()
        }
    }
}
