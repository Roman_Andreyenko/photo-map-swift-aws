//
//  SessionDataMapperImpl.swift
//  PhotoMapSwiftAWS
//
//  Created by Roman Andreyenko on 4/28/17.
//  Copyright © 2017 iTechArt. All rights reserved.
//

import Foundation

/**
 Class implementation of SessionDataMapper
 */
class SessionDataMapperImpl: SessionDataMapper {

    func transform(userEntity: UserEntity) -> User {
        return User(username: userEntity.username, userId: userEntity.userId)
    }
    
    func transform(user: User) -> UserEntity {
        return UserEntity(username: user.username, userId: user.userId)
    }

    func transform(userSessionEntity: UserSessionEntity) -> UserSession {
        return UserSession(accessToken: userSessionEntity.accessToken)
    }

    func transform(userSession: UserSession) -> UserSessionEntity {
        return UserSessionEntity(accessToken: userSession.accessToken)
    }
}
