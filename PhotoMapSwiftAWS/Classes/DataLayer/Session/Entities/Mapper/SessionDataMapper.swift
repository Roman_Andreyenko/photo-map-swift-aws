//
//  SessionDataMapper.swift
//  PhotoMapSwiftAWS
//
//  Created by Roman Andreyenko on 4/28/17.
//  Copyright © 2017 iTechArt. All rights reserved.
//

import Foundation

/**
 Contract to map Session related data from domain layer to data layer and vice versa.
 */
protocol SessionDataMapper {
    
    /**
     Converts UserEntity to a domain layer User
     
     - parameters:
        - userEntity: the model to convert
     
     - returns:
        the converted User
     */
    func transform(userEntity: UserEntity) -> User
    
    /**
     Converts User to a data layer UserEntity
     
     - parameters:
        - user: the model to convert
     
     - returns:
        the converted UserEntity
     */
    func transform(user: User) -> UserEntity
    
    /**
     Converts UserSessionEntity to a domain layer UserSession
     
     - parameters:
        - userSessionEntity: the model to convert
     
     - returns:
        the converted UserSession
     */
    func transform(userSessionEntity: UserSessionEntity) -> UserSession
    
    /**
     Converts UserSession to a data layer UserSessionEntity
     
     - parameters:
        - userSession: the model to convert
     
     - returns:
        the converted UserSessionEntity
     */
    func transform(userSession: UserSession) -> UserSessionEntity
}
