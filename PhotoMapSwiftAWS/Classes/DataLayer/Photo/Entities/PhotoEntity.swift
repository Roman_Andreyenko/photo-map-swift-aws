//
//  PhotoEntity.swift
//  PhotoMapSwiftAWS
//
//  Created by Roman Andreyenko on 5/10/17.
//  Copyright © 2017 iTechArt. All rights reserved.
//

import AWSDynamoDB

/**
 Class representation of a PhotoEntity
 */
class PhotoEntity: AWSDynamoDBObjectModel, AWSDynamoDBModeling {
    
    var photoId: String!
    var ownerId: String!
    var createdTime: NSNumber!
    var category: String!
    var descriptionText: String?
    var latitude: NSNumber!
    var longitude: NSNumber!
    
    override init() {
        super.init()
    }
    
    init(photoId: String, ownerId: String, createdTime: NSNumber, category: String, descriptionText: String?, latitude: NSNumber, longitude: NSNumber) {
        super.init()
        
        self.photoId = photoId
        self.ownerId = ownerId
        self.createdTime = createdTime
        self.category = category
        self.descriptionText = (descriptionText != nil && descriptionText!.isEmpty) ? nil : descriptionText
        self.latitude = latitude
        self.longitude = longitude
    }
    
    override init(dictionary dictionaryValue: [AnyHashable : Any]!, error: ()) throws {
        try super.init(dictionary: dictionaryValue, error: error)
    }
    
    required init!(coder: NSCoder!) {
        super.init(coder: coder)
    }
    
    static func dynamoDBTableName() -> String {
        return PhotoEntity.className
    }
    
    static func hashKeyAttribute() -> String {
        return #keyPath(PhotoEntity.ownerId)
    }
    
    static func rangeKeyAttribute() -> String {
        return #keyPath(PhotoEntity.createdTime)
    }
    
}
