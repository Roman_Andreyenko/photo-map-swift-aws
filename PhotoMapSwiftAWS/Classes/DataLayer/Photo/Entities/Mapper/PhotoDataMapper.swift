//
//  PhotoDataMapper.swift
//  PhotoMapSwiftAWS
//
//  Created by Roman Andreyenko on 5/19/17.
//  Copyright © 2017 iTechArt. All rights reserved.
//

import Foundation

/**
 Contract to map Photo related data from domain layer to data layer and vice versa.
 */
protocol PhotoDataMapper {
    
    /**
     Converts Photo to a data layer PhotoEntity
     
     - parameters:
        - photo: the model to convert
     
     - returns:
        the converted PhotoEntity
     */
    func transform(photo: Photo) -> PhotoEntity
    
    /**
     Converts PhotoEntity to a domain layer Photo
     
     - parameters:
        - photoEntity: the model to convert
     
     - returns:
        the converted Photo
     */
    func transform(photoEntity: PhotoEntity) -> Photo
    
    /**
     Converts PhotoPicture to a data layer PhotoPictureEntity
     
     - parameters:
        - photoPicture: the model to convert
     
     - returns:
        the converted PhotoPictureEntity
     */
    func transform(photoPicture: PhotoPicture) -> PhotoPictureEntity
    
    /**
     Converts PhotoPictureEntity to a domain layer PhotoPicture
     
     - parameters:
        - photoPictureEntity: the model to convert
     
     - returns:
        the converted PhotoPicture
     */
    func transform(photoPictureEntity: PhotoPictureEntity) -> PhotoPicture
    
    /**
     Converts Photo list to a data layer PhotoEntity list
     
     - parameters:
        - photoList: the model list to convert
     
     - returns:
        the converted PhotoEntity list
     */
    func transform(photoList: [Photo]) -> [PhotoEntity]
    
    /**
     Converts PhotoEntity list to a domain layer Photo list
     
     - parameters:
        - photoEntityList: the model to convert
     
     - returns:
        the converted Photo list
     */
    func transform(photoEntityList: [PhotoEntity]) -> [Photo]
    
    /**
     Converts HashtagIndex to a data layer HashtagIndexEntity
     
     - parameters:
        - hashtagIndex: the model to convert
     
     - returns:
        the converted HashtagIndexEntity
     */
    func transform(hashtagIndex: HashtagIndex) -> HashtagIndexEntity
    
    /**
     Converts HashtagIndexEntity to a domain layer HashtagIndex
     
     - parameters:
        - hashtagIndexEntity: the model to convert
     
     - returns:
        the converted HashtagIndex
     */
    func transform(hashtagIndexEntity: HashtagIndexEntity) -> HashtagIndex
    
    /**
     Converts HashtagIndex list to a data layer HashtagIndexEntity list
     
     - parameters:
        - hashtagIndex: the model list to convert
     
     - returns:
        the converted HashtagIndexEntity list
     */
    func transform(hashtagIndexList: [HashtagIndex]) -> [HashtagIndexEntity]
    
    /**
     Converts HashtagIndexEntity list to a domain layer HashtagIndex list
     
     - parameters:
        - hashtagIndexEntityList: the model list to convert
     
     - returns:
        the converted HashtagIndex list
     */
    func transform(hashtagIndexEntityList: [HashtagIndexEntity]) -> [HashtagIndex]
}
