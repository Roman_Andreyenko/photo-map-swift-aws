//
//  PhotoDataMapperImpl.swift
//  PhotoMapSwiftAWS
//
//  Created by Roman Andreyenko on 5/19/17.
//  Copyright © 2017 iTechArt. All rights reserved.
//

import Foundation

/**
 Class implementation of PhotoDataMapper
 */
class PhotoDataMapperImpl: PhotoDataMapper {

    func transform(photo: Photo) -> PhotoEntity {
        return PhotoEntity(photoId: photo.photoId, ownerId: photo.ownerId, createdTime: NSNumber(value: photo.createdTime.timeIntervalSince1970), category: photo.category.rawValue, descriptionText: photo.descriptionText, latitude: NSNumber(value: photo.latitude) , longitude: NSNumber(value: photo.longitude))
    }
    
    func transform(photoEntity: PhotoEntity) -> Photo {
        return Photo(photoId: photoEntity.photoId,
                     ownerId: photoEntity.ownerId,
                     createdTime: Date(timeIntervalSince1970: photoEntity.createdTime.doubleValue), category: Photo.Category(rawValue: photoEntity.category)!, descriptionText: photoEntity.descriptionText ?? "", latitude: photoEntity.latitude.doubleValue, longitude: photoEntity.longitude.doubleValue)
    }
    
    func transform(photoPicture: PhotoPicture) -> PhotoPictureEntity {
        return PhotoPictureEntity(photoId: photoPicture.photoId,
                                  type: PhotoPictureEntity.PictureType(rawValue: photoPicture.type.rawValue)!,
                                  data: photoPicture.data)
    }
    
    func transform(photoPictureEntity: PhotoPictureEntity) -> PhotoPicture {
        return PhotoPicture(photoId: photoPictureEntity.photoId,
                            type: PhotoPicture.PictureType(rawValue: photoPictureEntity.type.rawValue)!,
                            data: photoPictureEntity.data)
    }
    
    func transform(photoList: [Photo]) -> [PhotoEntity] {
        var photoEntityList = [PhotoEntity]()
        photoEntityList.reserveCapacity(photoList.count)
        for item in photoList {
            photoEntityList.append(transform(photo: item))
        }
        return photoEntityList
    }
    
    func transform(photoEntityList: [PhotoEntity]) -> [Photo] {
        var photoList = [Photo]()
        photoList.reserveCapacity(photoEntityList.count)
        for item in photoEntityList {
            photoList.append(transform(photoEntity: item))
        }
        return photoList
    }
    
    func transform(hashtagIndex: HashtagIndex) -> HashtagIndexEntity {
        let hashtagIndexEntity = HashtagIndexEntity(hashtagName: hashtagIndex.hashtagName, photoId: hashtagIndex.photoId)
        return hashtagIndexEntity
    }
    
    func transform(hashtagIndexEntity: HashtagIndexEntity) -> HashtagIndex {
        return HashtagIndex(hashtagName: hashtagIndexEntity.hashtagName, photoId: hashtagIndexEntity.photoId)
    }
    
    func transform(hashtagIndexList: [HashtagIndex]) -> [HashtagIndexEntity] {
        var hashtagIndexEntityList = [HashtagIndexEntity]()
        hashtagIndexEntityList.reserveCapacity(hashtagIndexList.count)
        for item in hashtagIndexList {
            hashtagIndexEntityList.append(transform(hashtagIndex: item))
        }
        return hashtagIndexEntityList
    }
    
    func transform(hashtagIndexEntityList: [HashtagIndexEntity]) -> [HashtagIndex] {
        var hashtagIndexList = [HashtagIndex]()
        hashtagIndexList.reserveCapacity(hashtagIndexEntityList.count)
        for item in hashtagIndexEntityList {
            hashtagIndexList.append(transform(hashtagIndexEntity: item))
        }
        return hashtagIndexList
    }
    
}
