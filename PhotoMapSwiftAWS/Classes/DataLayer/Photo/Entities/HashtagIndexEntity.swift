//
//  HashtagIndexEntity.swift
//  PhotoMapSwiftAWS
//
//  Created by Roman Andreyenko on 5/11/17.
//  Copyright © 2017 iTechArt. All rights reserved.
//

import AWSDynamoDB

/**
 Class representation of a HashtagIndexEntity
 */
class HashtagIndexEntity: AWSDynamoDBObjectModel, AWSDynamoDBModeling {

    var hashtagName: String!
    var photoId: String!
    
    override init() {
        super.init()
    }
    
    override init(dictionary dictionaryValue: [AnyHashable : Any]!, error: ()) throws {
        try super.init(dictionary: dictionaryValue, error: error)
    }
    
    init(hashtagName: String, photoId: String) {
        super.init()
        
        self.hashtagName = hashtagName
        self.photoId = photoId
    }
    
    required init!(coder: NSCoder!) {
        super.init()
    }
    
    static func dynamoDBTableName() -> String {
        return HashtagIndexEntity.className
    }
    
    static func hashKeyAttribute() -> String {
        return #keyPath(HashtagIndexEntity.hashtagName)
    }
    
    static func rangeKeyAttribute() -> String {
        return #keyPath(HashtagIndexEntity.photoId)
    }
}
