//
//  PhotoDataAssembly.swift
//  PhotoMapSwiftAWS
//
//  Created by Roman Andreyenko on 5/29/17.
//  Copyright © 2017 iTechArt. All rights reserved.
//

import Swinject

/**
 Swinject assembly which provides dependency bindings for photo related components
 */
class PhotoDataAssembly: Assembly {

    func assemble(container: Container) {
        assemblePhotoRepository(container: container)
        assemblePhotoDataStore(container: container)
        assemblePhotoDataMapper(container: container)
    }
    
    private func assemblePhotoRepository(container: Container) {
        container.autoregister(PhotoRepository.self, initializer: PhotoRepositoryImpl.init)
    }
    
    private func assemblePhotoDataStore(container: Container) {
        container.autoregister(PhotoDataStore.self, initializer: CloudPhotoDataStoreImpl.init)
    }
    
    private func assemblePhotoDataMapper(container: Container) {
        container.register(PhotoDataMapper.self) { r in
            return PhotoDataMapperImpl()
        }
    }
    
}
