//
//  PhotoDataStore.swift
//  PhotoMapSwiftAWS
//
//  Created by Roman Andreyenko on 5/11/17.
//  Copyright © 2017 iTechArt. All rights reserved.
//

import RxCocoa
import RxSwift

/**
 Contract that provides photo entity related items from data layer.
 */
protocol PhotoDataStore {
    
    /**
     Gets hashtag index entity list for certain hastags from data store (Cloud or Database)
     
     - parameters:
        - withHashtagNames: list of hashtag's names
     
     - returns:
        observable for hashtag index entity list
     */
    func getHashtagIndexes(withHashtagNames: [String]) -> Observable<[HashtagIndexEntity]>
    
    /**
     Gets photo entity list for provided user from data store (Cloud or Database)
     
     - parameters:
        - forUserId: user's id
     
     - returns:
        observable for photo entity list
     */
    func getPhotos(forUserId: String) -> Observable<[PhotoEntity]>
    
    /**
     Gets photo entity list for provided user from data store (Cloud or Database)
     
     - parameters:
        - forUserId: user's id
        - withHashtagIndexes: hashtag index entity list
     
     - returns:
        observable for photo entity list
     */
    func getPhotos(forUserId: String, withHashtagIndexes: [HashtagIndexEntity]) -> Observable<[PhotoEntity]>
    
    /**
     Creates hashtag index entity for certain photo in data store (Cloud or Database)
     
     - parameters:
        - forPhoto: photo entity, which hashtag index will be created for
        - withHashtagName: hashtag name
     
     - returns:
        observable for new created hashtag index entity
     */
    func createHashtagIndex(forPhoto: PhotoEntity, withHashtagName: String) -> Observable<HashtagIndexEntity>
    
    /**
     Creates photo entity in data store (Cloud or Database)
     
     - parameters:
        - withOwnerId: user's id of photo owner
        - withCategory: category of photo (possible values: default, nature, friends)
        - withDescriptionText: photo description text
        - withLatitude: geographical latitude of photo location
        - withLongitude: geographical longitude of photo location
        - withCreatedTime: date of photo creation
     
     - returns:
        observable for new created photo entity
     */
    func createPhoto(withOwnerId: String, withCategory: String, withDescriptionText: String, withLatitude: Double, withLongitude: Double, withCreatedTime: Date) -> Observable<PhotoEntity>
    
    /**
     Uploads photo picture to data store (Cloud or Database)
     
     - parameters:
        - withPictureData: binary picture representation
        - withName: picture name
     
     - returns:
        observable for uploaded picture
     */
    func uploadPhoto(withPictureData: Data, withName: String) -> Observable<String>
    
    /**
     Uploads photo thumbnail picture to data store (Cloud or Database)
     
     - parameters:
        - withThumbnailPictureData: binary picture representation
        - withName: picture name
     
     - returns:
        observable for uploaded thumbnail picture
     */
    func uploadPhoto(withThumbnailPictureData: Data, withName: String) -> Observable<String>
    
    /**
     Downloads photo picture from data store (Cloud or Database)
     
     - parameters:
        - withName: picture name
     
     - returns:
        observable for downloaded PhotoPictureEntity
     */
    func downloadPhoto(withName: String) -> Observable<PhotoPictureEntity>
    
    /**
     Downloads photo thumbnail picture from data store (Cloud or Database)
     
     - parameters:
        - withName: picture name
     
     - returns:
        observable for downloaded PhotoPictureEntity
     */
    func downloadThumbnailPhoto(withName: String) -> Observable<PhotoPictureEntity>
}
