//
//  CloudPhotoDataStoreImpl.swift
//  PhotoMapSwiftAWS
//
//  Created by Roman Andreyenko on 5/11/17.
//  Copyright © 2017 iTechArt. All rights reserved.
//

import RxCocoa
import RxSwift
import AWSDynamoDB
import AWSS3

/**
 Class implementation of PhotoDataStore
 Handles Photo data coming from Cloud.
 */
class CloudPhotoDataStoreImpl: PhotoDataStore {

    private let objectMapper: AWSDynamoDBObjectMapper
    private let transferUtility: AWSS3TransferUtility
    
    init(objectMapper: AWSDynamoDBObjectMapper, transferUtility: AWSS3TransferUtility) {
        self.objectMapper = objectMapper
        self.transferUtility = transferUtility
    }
    
    func getHashtagIndexes(withHashtagNames hashtagNames: [String]) -> Observable<[HashtagIndexEntity]> {
        return Observable<[HashtagIndexEntity]>.create { [unowned self] observer in
            let filterCondition = self.buildFilterCondition(withHashtagNames: hashtagNames)
            let scanExpression = AWSDynamoDBScanExpression()
            if !filterCondition.isEmpty {
                scanExpression.filterExpression = filterCondition.condition
                scanExpression.expressionAttributeValues = filterCondition.values
            }
            self.objectMapper
                .scan(HashtagIndexEntity.self, expression: scanExpression)
                .continueWith(block: { task in
                    if let error = task.error {
                        observer.onError(AWSUtils.correctAWS(error: error))
                    } else {
                        let hashtagIndexes = task.result!.items as! [HashtagIndexEntity]
                        observer.onNext(hashtagIndexes)
                        observer.onCompleted()
                    }
                    return nil
                })
            
            return Disposables.create()
        }
    }
    
    private func buildFilterCondition(withHashtagNames hashtagNames: [String]) -> AWSExpression {
        let baseHashtagVariableName = #keyPath(HashtagIndexEntity.hashtagName)
        var filterCondition = AWSExpression()
        for index in 0..<hashtagNames.count {
            let hashtagVariableName = baseHashtagVariableName + String(index)
            if index == 0 {
                filterCondition = AWSExpression.comparisonExpression(withAttribute: #keyPath(HashtagIndexEntity.hashtagName), withComparisonOperator: .EQ, withValue: (name: hashtagVariableName, value: hashtagNames[index]))
            } else {
                filterCondition = filterCondition.add(binaryCondition: .OR, withExpression: AWSExpression.comparisonExpression(withAttribute: #keyPath(HashtagIndexEntity.hashtagName), withComparisonOperator: .EQ, withValue: (name: hashtagVariableName, value: hashtagNames[index])))
            }
        }
        
        return filterCondition
    }
    
    func getPhotos(forUserId userId: String) -> Observable<[PhotoEntity]> {
        return Observable<[PhotoEntity]>.create { [unowned self] observer in
            let keyCondition = self.buildKeyCondition(withUserId: userId)
            let queryExpression = AWSDynamoDBQueryExpression()
            if !keyCondition.isEmpty {
                queryExpression.keyConditionExpression = keyCondition.condition
                queryExpression.expressionAttributeValues = keyCondition.values
            }
            self.objectMapper
                .query(PhotoEntity.self, expression: queryExpression)
                .continueWith(block: { task in
                    if let error = task.error {
                        observer.onError(AWSUtils.correctAWS(error: error))
                    } else {
                        let photos = task.result!.items as! [PhotoEntity]
                        observer.onNext(photos)
                        observer.onCompleted()
                    }
                    return nil
                })
            return Disposables.create()
        }
    }
    
    private func buildKeyCondition(withUserId userId: String) -> AWSExpression {
        return AWSExpression.comparisonExpression(withAttribute: #keyPath(PhotoEntity.ownerId), withComparisonOperator: .EQ, withValue: (name: #keyPath(PhotoEntity.ownerId), value: userId))
    }
    
    func getPhotos(forUserId userId: String, withHashtagIndexes hashtagIndexes: [HashtagIndexEntity]) -> Observable<[PhotoEntity]> {
        return Observable<[PhotoEntity]>.create { [unowned self] observer in
            let keyCondition = self.buildKeyCondition(withUserId: userId)
            let filterCondition = self.buildFilterCondition(withHashtagIndexes: hashtagIndexes)
            let queryExpression = AWSDynamoDBQueryExpression()
            if !keyCondition.isEmpty {
                queryExpression.keyConditionExpression = keyCondition.condition
            }
            if !filterCondition.isEmpty {
                queryExpression.filterExpression = filterCondition.condition
            }
            let attributeValues = keyCondition.values + filterCondition.values
            if !attributeValues.isEmpty {
                queryExpression.expressionAttributeValues = attributeValues
            }
            self.objectMapper
                .query(PhotoEntity.self, expression: queryExpression)
                .continueWith(block: { task in
                    if let error = task.error {
                        observer.onError(AWSUtils.correctAWS(error: error))
                    } else {
                        let photos = task.result!.items as! [PhotoEntity]
                        observer.onNext(photos)
                        observer.onCompleted()
                    }
                    return nil
                })
            return Disposables.create()
        }
    }

    private func buildFilterCondition(withHashtagIndexes hashtagIndexes: [HashtagIndexEntity]) -> AWSExpression {
        let basePhotoIdVariableName = #keyPath(PhotoEntity.photoId)
        var filterCondition = AWSExpression()
        for index in 0..<hashtagIndexes.count {
            let photoIdVariableName = basePhotoIdVariableName + String(index)
            if index == 0 {
                filterCondition = AWSExpression.comparisonExpression(withAttribute: #keyPath(PhotoEntity.photoId), withComparisonOperator: .EQ, withValue: (name: photoIdVariableName, value: hashtagIndexes[index].photoId!))
            } else {
                filterCondition = filterCondition.add(binaryCondition: .OR, withExpression: AWSExpression.comparisonExpression(withAttribute: #keyPath(PhotoEntity.photoId), withComparisonOperator: .EQ, withValue: (name: photoIdVariableName, value: hashtagIndexes[index].photoId!)))
            }
        }
        
        return filterCondition
    }
    
    func createHashtagIndex(forPhoto photo: PhotoEntity, withHashtagName hashtagName: String) -> Observable<HashtagIndexEntity> {
        return Observable<HashtagIndexEntity>.create { [unowned self] observer in
            let hashtagIndex = HashtagIndexEntity(hashtagName: hashtagName, photoId: photo.photoId)
            self.objectMapper
                .save(hashtagIndex)
                .continueWith(block: { task in
                    if let error = task.error {
                        observer.onError(AWSUtils.correctAWS(error: error))
                    } else {
                        observer.onNext(hashtagIndex)
                        observer.onCompleted()
                    }
                    return nil
                })
            return Disposables.create()
        }
    }

    func createPhoto(withOwnerId ownerId: String,
                     withCategory category: String,
                     withDescriptionText descriptionText: String,
                     withLatitude latitude: Double,
                     withLongitude longitude: Double,
                     withCreatedTime createdTime: Date) -> Observable<PhotoEntity> {
        return Observable<PhotoEntity>.create { [unowned self] observer in
            let photo = PhotoEntity(photoId: UUID().uuidString,
                                    ownerId: ownerId,
                                    createdTime: NSNumber(value: createdTime.timeIntervalSince1970),
                                    category: category,
                                    descriptionText: descriptionText,
                                    latitude: NSNumber(value: latitude),
                                    longitude: NSNumber(value: longitude))
            self.objectMapper
                .save(photo)
                .continueWith(block: { task in
                    if let error = task.error {
                        observer.onError(AWSUtils.correctAWS(error: error))
                    } else {
                        observer.onNext(photo)
                        observer.onCompleted()
                    }
                    return nil
                })
            return Disposables.create()
        }
    }
    
    func uploadPhoto(withPictureData pictureData: Data, withName name: String) -> Observable<String> {
        return uploadPhoto(withPictureData: pictureData, withPath: AWSConstants.S3.PhotoPictureBasePath + name)
    }
    
    func uploadPhoto(withThumbnailPictureData thumbnailPictureData: Data, withName name: String) -> Observable<String> {
        return uploadPhoto(withPictureData: thumbnailPictureData, withPath: AWSConstants.S3.PhotoThumbnailPictureBasePath + name)
    }
    
    private func uploadPhoto(withPictureData pictureData: Data, withPath path: String) -> Observable<String> {
        return Observable<String>.create { [unowned self] observer in
            self.transferUtility
                .uploadData(pictureData, bucket: AWSConstants.S3.BucketName, key: path, contentType: AWSConstants.S3.PictureContentType, expression: nil, completionHandler: { uploadTask, error in
                    if let error = error {
                        observer.onError(AWSUtils.correctAWS(error: error))
                    } else {
                        observer.onNext(uploadTask.key)
                        observer.onCompleted()
                    }
                })
            
            return Disposables.create()
        }
    }
    
    func downloadPhoto(withName name: String) -> Observable<PhotoPictureEntity> {
        return downloadPhoto(withName: name, withPath: AWSConstants.S3.PhotoPictureBasePath + name, qualityType: .fullQuality)
    }
    
    func downloadThumbnailPhoto(withName name: String) -> Observable<PhotoPictureEntity> {
        return downloadPhoto(withName: name, withPath: AWSConstants.S3.PhotoThumbnailPictureBasePath + name, qualityType: .thumbnailQuality)
    }
    
    private func downloadPhoto(withName name: String, withPath path: String, qualityType: PhotoPictureEntity.PictureType) -> Observable<PhotoPictureEntity> {
        return Observable<PhotoPictureEntity>.create { [unowned self] observer in
            self.transferUtility
                .downloadData(fromBucket: AWSConstants.S3.BucketName, key: path, expression: nil, completionHandler: { downloadTask, url, data, error in
                    if let error = error {
                        observer.onError(AWSUtils.correctAWS(error: error))
                    } else {
                        observer.onNext(PhotoPictureEntity(photoId: name, type: qualityType, data: data!))
                        observer.onCompleted()
                    }
                })
            
            return Disposables.create()
        }
    }
}
