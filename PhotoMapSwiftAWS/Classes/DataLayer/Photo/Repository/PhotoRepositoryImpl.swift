//
//  PhotoRepositoryImpl.swift
//  PhotoMapSwiftAWS
//
//  Created by Roman Andreyenko on 5/19/17.
//  Copyright © 2017 iTechArt. All rights reserved.
//

import RxCocoa
import RxSwift

/**
 Class implementation of PhotoRepository
 */
class PhotoRepositoryImpl: PhotoRepository {

    //Inject
    private var dataSource: PhotoDataStore
    //Inject
    private var dataMapper: PhotoDataMapper
    
    init(dataSource: PhotoDataStore, dataMapper: PhotoDataMapper) {
        self.dataSource = dataSource
        self.dataMapper = dataMapper
    }
    
    func getHashtagIndexes(withHashtagNames hashtagNames: [String]) -> Observable<[HashtagIndex]> {
        return dataSource.getHashtagIndexes(withHashtagNames: hashtagNames).map { [unowned self] hashtagIndexes in
            return self.dataMapper.transform(hashtagIndexEntityList: hashtagIndexes)
        }
    }
    
    func getPhotos(forUserId userId: String) -> Observable<[Photo]> {
        return dataSource.getPhotos(forUserId: userId).map { [unowned self] photos in
            return self.dataMapper.transform(photoEntityList: photos)
        }
    }
    
    func getPhotos(forUserId userId: String, withHashtagIndexes hashtagIndexes: [HashtagIndex]) -> Observable<[Photo]> {
        return dataSource.getPhotos(forUserId: userId, withHashtagIndexes: dataMapper.transform(hashtagIndexList: hashtagIndexes)).map { [unowned self] photos in
            return self.dataMapper.transform(photoEntityList: photos)
        }
    }
    
    func createHashtagIndex(forPhoto photo: Photo, withHashtagName hashtagName: String) -> Observable<HashtagIndex> {
        return dataSource.createHashtagIndex(forPhoto: dataMapper.transform(photo: photo), withHashtagName: hashtagName).map { [unowned self] hashtagIndex in
            return self.dataMapper.transform(hashtagIndexEntity: hashtagIndex)
        }
    }
    
    func createPhoto(withOwnerId ownerId: String,
                     withCategory category: Photo.Category,
                     withDescriptionText descriptionText: String,
                     withLatitude latitude: Double,
                     withLongitude longitude: Double,
                     withCreatedTime createdTime: Date) -> Observable<Photo> {
        return dataSource.createPhoto(withOwnerId: ownerId, withCategory: category.rawValue, withDescriptionText: descriptionText, withLatitude: latitude, withLongitude: longitude, withCreatedTime: createdTime).map { [unowned self] photo in
            return self.dataMapper.transform(photoEntity: photo)
        }
    }
    
    func uploadPhoto(withPictureData pictureData: Data, withName name: String) -> Observable<String> {
        return dataSource.uploadPhoto(withPictureData: pictureData, withName: name)
    }
    
    func uploadPhoto(withThumbnailPictureData thumbnailPictureData: Data, withName name: String) -> Observable<String> {
        return dataSource.uploadPhoto(withThumbnailPictureData: thumbnailPictureData, withName: name)
    }
    
    func downloadPhoto(withName name: String) -> Observable<PhotoPicture> {
        return dataSource.downloadPhoto(withName: name).map { [unowned self] photoPictureEntity in
            return self.dataMapper.transform(photoPictureEntity: photoPictureEntity)
        }
    }
    
    func downloadThumbnailPhoto(withName name: String) -> Observable<PhotoPicture> {
        return dataSource.downloadThumbnailPhoto(withName: name).map { [unowned self] photoPictureEntity in
            return self.dataMapper.transform(photoPictureEntity: photoPictureEntity)
        }
    }
}
