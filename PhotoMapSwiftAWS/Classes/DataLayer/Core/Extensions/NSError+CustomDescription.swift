//
//  NSError+CustomDescription.swift
//  PhotoMapSwiftAWS
//
//  Created by Roman Andreyenko on 5/3/17.
//  Copyright © 2017 iTechArt. All rights reserved.
//

import Foundation

extension NSError {
    
    public func copy(withReplacedLocalizedDescription localizedDescription: String) -> NSError {
        var userInfo = [AnyHashable : Any](minimumCapacity: self.userInfo.count)
        for (key, value) in self.userInfo {
            userInfo[key] = value
        }
        userInfo[NSLocalizedDescriptionKey] = localizedDescription
        return NSError(domain: domain, code: code, userInfo: userInfo)
    }
}
