//
//  AWSDynamoDBAssembly.swift
//  PhotoMapSwiftAWS
//
//  Created by Roman Andreyenko on 5/29/17.
//  Copyright © 2017 iTechArt. All rights reserved.
//

import Swinject
import AWSDynamoDB

/**
 Swinject assembly which provides dependency bindings for ApplicationComponent
 */
class AWSDynamoDBAssembly: Assembly {

    func assemble(container: Container) {
        assembleAWSDynamoDBObjectMapper(container: container)
    }
    
    private func assembleAWSDynamoDBObjectMapper(container: Container) {
        container.register(AWSDynamoDBObjectMapper.self) { r in
            return AWSDynamoDBObjectMapper.default()
            }
            .inObjectScope(.container)
    }
    
}
