//
//  AWSCognitoAssembly.swift
//  PhotoMapSwiftAWS
//
//  Created by Roman Andreyenko on 5/2/17.
//  Copyright © 2017 iTechArt. All rights reserved.
//

import Swinject
import AWSCognito
import AWSCognitoIdentityProvider

/**
 Swinject assembly which provides dependency bindings for ApplicationComponent
 */
class AWSCognitoAssembly: Assembly {

    func assemble(container: Container) {
        assembleAWSServiceManager(container: container)
        assembleAWSCredentialsProvider(container: container)
        assembleAWSServiceConfiguration(container: container)
        assembleAWSCognitoIdentityUserPoolConfiguration(container: container)
        assembleAWSCognitoIdentityUserPool(container: container)
    }
    
    private func assembleAWSServiceManager(container: Container) {
        container.register(AWSServiceManager.self) { r in
            return AWSServiceManager.default()
            }
            .inObjectScope(.container)
    }
    
    private func assembleAWSCredentialsProvider(container: Container) {
        container.register(AWSCognitoCredentialsProvider.self) { r in
            return AWSCognitoCredentialsProvider(regionType: AWSConstants.Cognito.Region, identityPoolId: AWSConstants.Cognito.IdentityPoolId)
            }
            .inObjectScope(.container)
    }
    
    private func assembleAWSServiceConfiguration(container: Container) {
        container.register(AWSServiceConfiguration.self) { r in
            let serviceConfiguration = AWSServiceConfiguration(region: AWSConstants.Cognito.Region, credentialsProvider: r.resolve(AWSCognitoCredentialsProvider.self))
            container.resolve(AWSServiceManager.self)!.defaultServiceConfiguration = serviceConfiguration
            return serviceConfiguration!
            }
            .inObjectScope(.container)
    }
    
    private func assembleAWSCognitoIdentityUserPoolConfiguration(container: Container) {
        container.register(AWSCognitoIdentityUserPoolConfiguration.self) { r in
            return AWSCognitoIdentityUserPoolConfiguration(clientId: AWSConstants.Cognito.ClientId, clientSecret: AWSConstants.Cognito.ClientSecret, poolId: AWSConstants.Cognito.PoolId)
            }
            .inObjectScope(.container)
    }
    
    private func assembleAWSCognitoIdentityUserPool(container: Container) {
        container.register(AWSCognitoIdentityUserPool.self) { r in
            AWSCognitoIdentityUserPool.register(with: container.resolve(AWSServiceConfiguration.self), userPoolConfiguration: container.resolve(AWSCognitoIdentityUserPoolConfiguration.self)!, forKey: AWSConstants.Cognito.PoolKey)
            return AWSCognitoIdentityUserPool(forKey: AWSConstants.Cognito.PoolKey)
            }
            .inObjectScope(.container)
    }
}
