//
//  AWSS3Assembly.swift
//  PhotoMapSwiftAWS
//
//  Created by Roman Andreyenko on 6/2/17.
//  Copyright © 2017 iTechArt. All rights reserved.
//

import Swinject
import AWSS3

/**
 Swinject assembly which provides dependency bindings for ApplicationComponent
 */
class AWSS3Assembly: Assembly {

    func assemble(container: Container) {
        assembleAWSS3TransferManager(container: container)
    }
    
    private func assembleAWSS3TransferManager(container: Container) {
        container.register(AWSS3TransferUtility.self) { r in
            return AWSS3TransferUtility.default()
            }
            .inObjectScope(.container)
    }
    
}
