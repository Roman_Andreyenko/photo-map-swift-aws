//
//  Expression.swift
//  PhotoMapSwiftAWS
//
//  Created by Roman Andreyenko on 5/18/17.
//  Copyright © 2017 iTechArt. All rights reserved.
//

import Foundation

/**
 Amazon Web Services helper class to build condition expression for DynamoDB queries 
 */
class AWSExpression: NSObject {
    
    /**
     Enum representation of binary operator in DynamoDB queries
     */
    enum BinaryOperator: String {
        case OR = " OR "
        case AND = " AND "
    }
    /**
     Enum representation of unary operator in DynamoDB queries
     */
    enum UnaryOperator: String {
        case NOT = "NOT "
        case BRACKETS
    }
    
    /**
     Enum representation of comparison operator in DynamoDB queries
     */
    enum ComparisonOperator: String {
        case EQ = " = "
        case NQ = " <> "
        case LS = " < "
        case GT = " > "
        case LE = " <= "
        case GE = " >= "
    }
    
    var condition: String = ""
    var values = [String : Any]()
    
    class func comparisonExpression(withAttribute attribute: String, withComparisonOperator comporator: ComparisonOperator, withValue value: (name: String, value: Any)) -> AWSExpression {
        let expression = AWSExpression()
        if !attribute.isEmpty
            && !value.name.isEmpty {
            let valueName = ":" + value.name
            expression.condition = attribute + comporator.rawValue + valueName
            expression.values[valueName] = value.value
        }
        return expression
    }
    
    var isEmpty: Bool {
        return condition.isEmpty
    }
    
    func add(binaryCondition condition: BinaryOperator, withExpression expression: AWSExpression) -> AWSExpression {
        if !isEmpty {
            self.condition += condition.rawValue + expression.condition
            self.values += expression.values
        }
        
        return self
    }
    
    func add(unaryCondition condition: UnaryOperator) -> AWSExpression {
        switch condition {
        case .NOT:
            self.condition = condition.rawValue + self.condition
        case .BRACKETS:
            self.condition = "(" + self.condition + ")"
        }
        return self
    }
}
