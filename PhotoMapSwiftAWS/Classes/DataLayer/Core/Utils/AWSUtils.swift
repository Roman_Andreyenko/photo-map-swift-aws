//
//  AWSUtils.swift
//  PhotoMapSwiftAWS
//
//  Created by Roman Andreyenko on 5/3/17.
//  Copyright © 2017 iTechArt. All rights reserved.
//

import Foundation

/**
 Contains all Amazon Web Services utilities
 */
class AWSUtils {

    public class func correctAWS(error: Error) -> Error {
        let e = error as NSError
        if e.userInfo[AWSConstants.MessageKey] != nil {
            let message = e.userInfo[AWSConstants.MessageKey] as! String
            return e.copy(withReplacedLocalizedDescription: message)
        } else {
            return e
        }
    }
}
