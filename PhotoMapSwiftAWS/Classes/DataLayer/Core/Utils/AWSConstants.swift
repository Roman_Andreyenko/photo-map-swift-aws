//
//  AWSConstants.swift
//  PhotoMapSwiftAWS
//
//  Created by Roman Andreyenko on 5/3/17.
//  Copyright © 2017 iTechArt. All rights reserved.
//

import Foundation
import AWSCognitoIdentityProvider

/**
 Contains all Amazon Web Services settings
 */
struct AWSConstants {
    
    // AWS NSError contains localized description in userInfo's dictionary under this key
    static let MessageKey = "message"
    // AWS UserAttributeType 'email'
    static let UserAttributeTypeEmailKey = "email"
    
    /**
     Contains Amazon Web Services Cognito settings
     */
    struct Cognito {
        
        static let Region = AWSRegionType.USWest2
        static let IdentityPoolId = "us-west-2:a990aa00-4357-4da3-a6a2-a1c6991bd0bf"
        static let ClientId = "44t77is63gv6lqfd3gkov3p525"
        static let ClientSecret = "11kdlts7bts7foogeb2rfvh233c8e9irhhtgtt4uis2tqfe3vd3c"
        static let PoolId = "us-west-2_UvEdl4txD"
        static let PoolKey = "PhotoMapUserPool"
    }
    
    /**
     Contains Amazon Web Services S3 settings
     */
    struct S3 {
        
        static let BucketName = "com.photomap"
        static let PictureContentType = "image/png"
        
        static let PhotoPictureBasePath = "PhotoPicture/"
        static let PhotoThumbnailPictureBasePath = "PhotoThumbnailPicture/"
    }
}
